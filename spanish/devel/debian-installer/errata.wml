#use wml::debian::template title="Erratas del instalador de Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="190364034624c840c0b9e316daa41653f0a86db2" maintainer="Laura Arjona Reina"

<h1>Erratas en «<humanversion />»</h1>

<p>
Esta es una lista de problemas conocidos en la versión «<humanversion />»
del instalador de Debian. Si usted no ve aquí listado su problema,
por favor envíenos un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">informe de instalación</a>
describiéndolo.
</p>

<dl class="gloss">
     <dt>Tema usado en el instalador</dt>
     <dd>Aún no hay arte gráfico para <q>bookworm</q>, 
	y el instalador aún usa el tema de <q>bullseye</q>.
     <br />
     <b>Estado:</b> Corregido en Bookworm Alfa 2, aunque los entornos de escritorio
     puede que aún usen el tema de <q>bullseye</q>.</dd>

     <dt>Firmware necesario para algunas tarjetas de sonido</dt>
     <dd>Parece que hay algunas tarjetas de sonido que requieren cargar 
     firmware para poder emitir sonido
     (<a href="https://bugs.debian.org/992699">#992699</a>).
     <br />
     <b>Estado:</b> Corregido en Bookworm Alfa 1.</dd>

     <dt>El LVM cifrado puede fallar en sistemas con poca memoria</dt>
     <dd>Es posible que en sistemas con poca memoria (por ej. 1 GB) aparezca un fallo al configurar LVM cifrado: cryptsetup puede disparar una finalización del proceso por falta de memoria mientras formatea la partición LUKS
     (<a href="https://bugs.debian.org/1028250">#1028250</a>,
     <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">proyecto original cryptsetup</a>).
     </dd>
</dl>
