#use wml::debian::template title="Instalador de Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="a22870164df5007ae4f4e356dfe54983be0f1e9e" maintainer="Laura Arjona Reina"

<h1>Noticias</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Noticias anteriores</a>
</p>

<h1>Instalar con el instalador de Debian</h1>


<p>
<if-stable-release release="bullseye">
<strong>Para obtener los medios oficiales de instalación de Debian <current_release_bullseye> así
como información sobre éstos</strong>, consulte
<a href="$(HOME)/releases/bullseye/debian-installer">la página de bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>Para obtener los medios oficiales de instalación de Debian  <current_release_bookworm> así
como información sobre éstos</strong>, consulte
<a href="$(HOME)/releases/bookworm/debian-installer">la página de bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Todas las imágenes enlazadas más abajo son para la versión del Instalador de Debian
que se está desarrollando para la próxima versión de Debian e instalarán Debian
<q>en pruebas</q> (<q><current_testing_name></q>) por omisión.
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>Para instalar Debian <em>en pruebas</em></strong> le recomendamos que
utilice las <strong>versiones diarias</strong> del instalador.
Disponemos de las siguientes imágenes de las versiones diarias:
</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

Le recomendamos que utilice la versión <strong><humanversion /></strong> del instalador
<strong>para instalar Debian <em>en pruebas</em></strong> después de revisar la 
<a href="errata">fe de erratas</a>. Disponemos de las siguientes imágenes de
<humanversion />:

</p>

<h2>Publicación oficial</h2>

<div class="line">
<div class="item col50">
<strong>Imágenes de CD «netinst» para instalación por red</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>Imágenes de CD «netinst» para instalación por red (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Otras imágenes (arranque de red, memoria USB, etc.)</strong>
<other-images />
</div>
</div>

<p>
De manera alternativa puede usar una <b>instantánea</b> de Debian <em>en pruebas</em></b>.
Las construcciones semanales generan conjuntos completos de imágenes mientras que las construcciones diarias generan solo algunas imágenes.
</p>

<div class="warning">

<p>
Estas instantáneas instalan Debian <em>en pruebas</em>, pero el instalador está basado en Debian
inestable.
</p>

</div>

<h2>Imágenes generadas semanalmente</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>

<h2>Imágenes generadas diariamente</h2>

<div class="line">
<div class="item col50">
<strong>Imágenes de CD «netinst» para instalación por red</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Imágenes de CD «netinst» para instalación por red (a través de <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Otras imágenes (arranque por red, memoria USB, etc.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>Notas</strong>
</p>
<ul>
#	<li>Antes de descargar las imágenes que se generan a diario, le sugerimos que compruebe los
#	<a href="https://wiki.debian.org/DebianInstaller/Today">problemas conocidos</a>.</li>
	<li>Una arquitectura se puede omitir (temporalmente) de las imágenes diarias 
	si éstas no están disponibles (de forma confiable).</li>
	<li>Para las imágenes de instalación, están disponibles ficheros de verificación
	(<tt>SHA512SUMS</tt>, <tt>SHA256SUMS</tt> y otros) en el mismo
	 directorio que las imágenes.</li>
	<li>Se recomienda el uso de jigdo para obtener las imágenes de CD y DVD.</li>
	<li>Sólo están disponibles un número limitado de imágenes de DVD como 
	archivos ISO para descarga directa. La mayoría de usuarios no necesitan todo el software 
	disponible en todos los discos, así que para ahorrar espacio en los servidores y réplicas, 
	los conjuntos completos sólo están disponibles a través de
	jigdo.</li>
</ul>

<p>
<strong>Después de usar el instalador de Debian</strong>, por favor, envíe un 
<a href="https://d-i.debian.org/manual/es.amd64/ch05s04.html#submit-bug">informe
de instalación</a>, incluso si pudo realizar la instalación sin ningún problema.
</p>

<h1>Documentación</h1>

<p>
<strong>Si sólo quiere leer un único documento</strong> antes de instalar, lea nuestro
<a href="https://d-i.debian.org/manual/es.amd64/apa.html">Cómo
instalar</a>, una guía rápida del proceso de instalación. También
encontrará útiles documentos como:
</p>

<ul>
<li>Guía de instalación:
#    <a href="$(HOME)/releases/stable/installmanual">versión para la publicación estable</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">versión de desarrollo (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">última versión (Git)</a>
<br />
con instrucciones de instalación detalladas (tenga en cuenta que la traducción
al español puede no estar actualizada)</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">preguntas frecuentes sobre el instalador
de Debian</a> y <a href="$(HOME)/CD/faq/">preguntas frecuentes sobre los CD de Debian</a> <br /> 
respuestas a preguntas comunes</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki del instalador de
Debian</a><br /> documentación mantenida por la comunidad</li>
</ul>

<h1>Cómo contactar con nosotros</h1>

<p>
La <a href="https://lists.debian.org/debian-boot/">lista de correo
debian-boot</a> es el foro principal de discusión y trabajo sobre el instalador
de Debian.
</p>

<p>
También tenemos un canal de IRC, #debian-boot, en <tt>irc.debian.org</tt>.
Este canal se usa principalmente para desarrollo, y en ocasiones también para
dar soporte. Si no recibe respuesta, pruebe en su lugar la lista de correo.
</p>
