#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.4</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la cuarta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apt-cacher-ng "Hace que la llamada al servidor al lanzar trabajos de mantenimiento sea segura [CVE-2020-5202]; permite compresión .zst para archivos tar; incrementa el tamaño del área de memoria utilizada para descomprimir las líneas durante la lectura del fichero de configuración">
<correction backuppc "Pasa el nombre de usuario a start-stop-daemon al recargar, evitando fallos de recarga">
<correction base-files "Actualizado para esta versión">
<correction brltty "Reduce gravedad de mensaje de log para evitar la generación de demasiados mensajes cuando se utiliza con versiones nuevas de Orca">
<correction checkstyle "Corrige problema de inyección de entidad externa XML [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Actualiza lista de réplicas incluida">
<correction clamav "Nueva versión del proyecto original [CVE-2020-3123]">
<correction corosync "totemsrp: reduce MTU para evitar la generación de paquetes con exceso de longitud">
<correction corosync-qdevice "Corrige arranque del servicio">
<correction csync2 "Falla la orden HELLO cuando se requiere SSL">
<correction cups "Corrige desbordamiento de memoria dinámica («heap») [CVE-2020-3898] y <q>la función `ippReadIO` puede leer fuera de límites un campo extensión («under-read an extension field»)</q> [CVE-2019-8842]">
<correction dav4tbsync "Nueva versión del proyecto original, recuperando compatibilidad con versiones más recientes de Thunderbird">
<correction debian-edu-config "Añade ficheros de políticas para Firefox ESR y para Thunderbird con el objetivo de corregir la configuración de TLS/SSL">
<correction debian-installer "Actualizado para la ABI del núcleo 4.19.0-9">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-security-support "Nueva versión «estable» del proyecto original; actualizado el estado de varios paquetes; usa <q>runuser</q> en lugar de <q>su</q>">
<correction distro-info-data "Añade Ubuntu 20.10 y fecha estimada de fin de soporte para stretch">
<correction dojo "Corrige uso incorrecto de expresiones regulares [CVE-2019-10785]">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction dtv-scan-tables "Nuevo snapshot del proyecto original; añade todos los multiplexores actuales del DVB-T2 alemán y el satélite Eutelsat-5-West-A">
<correction eas4tbsync "Nueva versión del proyecto original, recuperando compatibilidad con versiones más recientes de Thunderbird">
<correction edk2 "Correcciones de seguridad [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Corrige migraciones de stretch a buster que incluyen Tomcat 8">
<correction fex "Corrige un potencial problema de seguridad en fexsrv">
<correction filezilla "Corrige vulnerabilidad de ruta de búsqueda no confiable («untrusted search path vulnerability») [CVE-2019-5429]">
<correction frr "Corrige capacidad extendida de siguiente salto («extended next hop capability»)">
<correction fuse "Elimina órdenes udevadm obsoletas de scripts post-install; no borra fuse.conf explícitamente al eliminar («purge»)">
<correction fuse3 "Elimina órdenes udevadm obsoletas de scripts post-install; no borra fuse.conf explícitamente al eliminar («purge»); corrige filtración de contenido de la memoria en fuse_session_new()">
<correction golang-github-prometheus-common "Extiende validez de certificados de prueba">
<correction gosa "Sustituye (un)serialize por json_encode/json_decode para mitigar inyección de objetos PHP [CVE-2019-14466]">
<correction hbci4java "Soporta directiva de la Unión Europea sobre servicios de pago (PSD2)">
<correction hibiscus "Soporta directiva de la Unión Europea sobre servicios de pago (PSD2)">
<correction iputils "Corrige un problema por el cual ping termina, incorrectamente, con un código de error cuando todavía quedan direcciones no probadas en la lista devuelta por la función de biblioteca getaddrinfo()">
<correction ircd-hybrid "Usa dhparam.pem para evitar caída en el arranque">
<correction jekyll "Permite el uso de ruby-i18n 0.x y 1.x">
<correction jsp-api "Corrige migraciones de stretch a buster que incluyen Tomcat 8">
<correction lemonldap-ng "Evita accesos no deseados a puntos de acceso («endpoints») de administración [CVE-2019-19791]; corrige la extensión («plugin») GrantSession que no podía prohibir el acceso («logon») cuando se usaba autenticación de doble factor; corrige redirecciones arbitrarias con OIDC si no se usaba redirect_uri">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libreoffice "Corrige transiciones de transparencias OpenGL">
<correction libssh "Corrige posible problema de denegación de servicio al manejar claves AES-CTR con OpenSSL [CVE-2020-1730]">
<correction libvncserver "Corrige desbordamiento de memoria dinámica («heap») [CVE-2019-15690]">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizada la ABI del núcleo a la 4.19.0-9">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction lwip "Corrige desbordamiento de memoria [CVE-2020-8597]">
<correction lxc-templates "Nueva versión «estable» del proyecto original; gestiona idiomas que solo están codificados en UTF-8">
<correction manila "Corrige comprobación de los permisos de acceso, que faltaba [CVE-2020-9543]">
<correction megatools "Añade soporte para el nuevo formato de los enlaces mega.nz">
<correction mew "Corrige comprobación de la validez de certificados SSL de servidor">
<correction mew-beta "Corrige comprobación de la validez de certificados SSL de servidor">
<correction mkvtoolnix "Recompilado para ajustar dependencia con libmatroska6v5">
<correction ncbi-blast+ "Inhabilita soporte de SSE4.2">
<correction node-anymatch "Elimina dependencias innecesarias">
<correction node-dot "Evita ejecución de código después de contaminación de prototipo [CVE-2020-8141]">
<correction node-dot-prop "Corrige contaminación de prototipo [CVE-2020-8116]">
<correction node-knockout "Corrige la codificación para evitar la evaluación («escaping») con versiones antiguas de Internet Explorer [CVE-2019-14862]">
<correction node-mongodb "Rechaza _bsontypes inválidos [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Corrige contaminación de prototipo [CVE-2020-7608]">
<correction npm "Corrige acceso a rutas arbitrarias [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "Nueva versión «estable» del proyecto original">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión «estable» del proyecto original">
<correction nvidia-settings-legacy-340xx "Nueva versión del proyecto original">
<correction oar "Revierte el comportamiento de la función perl Storable::dclone al que tenía en stretch, corrigiendo problemas de profundidad de recursión">
<correction opam "Prefiere mccs a aspcud">
<correction openvswitch "Corrige fallo de vswitchd cuando se añade un puerto y el controlador está caído">
<correction orocos-kdl "Corrige conversión de cadena de caracteres con Python 3">
<correction owfs "Elimina paquetes de Python 3 rotos">
<correction pango1.0 "Corrige caída en pango_fc_font_key_get_variations() cuando la clave es nula">
<correction pgcli "Añade dependencia con python3-pkg-resources, que faltaba">
<correction php-horde-data "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8518]">
<correction php-horde-form "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8866]">
<correction php-horde-trean "Corrige vulnerabilidad de ejecución de código remoto por usuarios autenticados [CVE-2020-8865]">
<correction postfix "Nueva versión «estable» del proyecto original; corrige panic con configuración Postfix multi-Milter durante MAIL FROM; corrige cambio en d/init.d running de forma que vuelva a funcionar con instancias múltiples">
<correction proftpd-dfsg "Corrige problema de acceso a memoria en código de «keyboard-interactive» en mod_sftp; procesa correctamente mensajes DEBUG, IGNORE, DISCONNECT y UNIMPLEMENTED en modo «keyboard-interactive»">
<correction puma "Corrige problema de denegación de servicio [CVE-2019-16770]">
<correction purple-discord "Corrige caídas en ssl_nss_read">
<correction python-oslo.utils "Corrige fuga de información sensible a través de los logs de mistral [CVE-2019-3866]">
<correction rails "Corrige posible ejecución de scripts entre sitios («cross-site scripting») mediante métodos de ayuda Javascript para codificar caracteres y evitar su evaluación («escape helper») [CVE-2020-5267]">
<correction rake "Corrige vulnerabilidad de inyección de órdenes [CVE-2020-8130]">
<correction raspi3-firmware "Corrige discordancia de nombres de los dtb en z50-raspi-firmware; corrige arranque en Raspberry Pi familias 1 y 0">
<correction resource-agents "Corrige <q>ethmonitor no lista interfaces que no tienen dirección IP asignada</q>; elimina parche xen-toolstack, que ya no es necesario; corrige uso no estándar en agente ZFS">
<correction rootskel "Inhabilita soporte de múltiples consolas si se usa preselección de respuestas («preseeding»)">
<correction ruby-i18n "Corrige generación de gemspec">
<correction rubygems-integration "Evita avisos de depreciación cuando los usuarios instalan una versión más reciente de Rubygems mediante <q>gem update --system</q>">
<correction schleuder "Mejora el parche para tratar errores de codificación introducido en la versión anterior; cambia la codificación por omisión a UTF-8; permite que x-add-key procese correos con claves adjuntas codificadas con caracteres imprimibles («quoted-printable»); corrige x-attach-listkey con correos creados por Thunderbird que incluyen cabeceras protegidas">
<correction scilab "Corrige la carga de bibliotecas con OpenJDK 11.0.7">
<correction serverspec-runner "Soporta Ruby 2.5">
<correction softflowd "Corrige la agregación de flujos, que está rota y podría dar lugar al desbordamiento de la tabla de flujos y a un consumo de CPU del 100%">
<correction speech-dispatcher "Corrige latencia por omisión de pulseaudio, que provoca salida <q>chirriante</q> («scratchy»)">
<correction spl-linux "Corrige abrazo mortal">
<correction sssd "Corrige bucle en sssd_be cuando la conexión a LDAP es intermitente">
<correction systemd "Al autorizar con PolicyKit, resuelve de nuevo la retrollamada («callback») y los datos de usuario en lugar de cachearlos [CVE-2020-1712]; instala 60-block.rules en udev-udeb y en initramfs-tools">
<correction taglib "Corrige problemas de corrupción de ficheros OGG">
<correction tbsync "Nueva versión del proyecto original, recuperando compatibilidad con versiones más recientes de Thunderbird">
<correction timeshift "Corrige uso de directorio temporal predecible [CVE-2020-10174]">
<correction tinyproxy "Solo da valor a PIDDIR si PIDFILE es una cadena de caracteres con longitud mayor que cero">
<correction tzdata "Nueva versión «estable» del proyecto original">
<correction uim "Elimina del registro módulos que no están instalados, corrigiendo una regresión en la actualización anterior del paquete">
<correction user-mode-linux "Corrige error de compilación con núcleos «estables» actuales">
<correction vite "Corrige caída cuando hay más de 32 elementos">
<correction waagent "Nueva versión del proyecto original; soporta instalación conjuntamente con cloud-init">
<correction websocket-api "Corrige migraciones de stretch a buster que incluyen Tomcat 8">
<correction wpa "No intenta detectar discordancia de la PSK durante la regeneración («rekeying») de la PTK; comprueba el soporte de FT al seleccionar suites FT; corrige problema de aleatorización de la MAC con algunas tarjetas">
<correction xdg-utils "xdg-open: corrige comprobación y tratamiento por parte de pcmanfm de directorios cuyo nombre contiene espacios; xdg-screensaver: sanea el nombre de la ventana antes de enviárselo a D-Bus; xdg-mime: crea el directorio de configuración si no existe">
<correction xtrlock "Corrige el bloqueo de (algunos) dispositivos multitáctiles mientras está cerrado [CVE-2016-10894]">
<correction zfs-linux "Corrige potenciales problemas de abrazo mortal">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction getlive "Roto debido a cambios en Hotmail">
<correction gplaycli "Roto por cambios en la API de Google">
<correction kerneloops "El servicio del proyecto original ya no está disponible">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] Roto en arquitecturas no amd64">
<correction libmicrodns "Problemas de seguridad">
<correction libperlspeak-perl "Problemas de seguridad; sin desarrollo activo">
<correction quotecolors "Incompatible con versiones más recientes de Thunderbird">
<correction torbirdy "Incompatible con versiones más recientes de Thunderbird">
<correction ugene "No libre; no compila">
<correction yahoo2mbox "Roto desde hace varios años">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


