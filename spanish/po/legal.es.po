# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Templates webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-08-09 12:57+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Información sobre licencias"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "Índice RLD"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DSLD"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "Preguntas frecuentes DSLD"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Archivo de Debian-legal"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, Versión %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Fecha de publicación"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Licencia"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Versión"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Resumen"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Justificación"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Discusión"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Resumen original"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Se puede encontrar el resumen original de <summary-author/> en los <a href="
"\"<summary-url/>\">archivos de la lista</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "<summary-author> preparó este resumen."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Texto de la licencia (traducido)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Texto de la licencia"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "libre"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "no-libre"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "no redistribuible"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Libre"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "No-libre"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "No redistribuible"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Consulte la página con la <a href=\"./\">información de licencias</a> para "
"tener una visión general de los resúmenes de licencias de Debian (RLD)."
