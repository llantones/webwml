#use wml::debian::template title="Ayuda técnica" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">


<ul class="toc">
  <li><a href="#irc">IRC (ayuda en tiempo real)</a></li>
  <li><a href="#mail_lists">Listas de correo</a></li>
  <li><a href="#usenet">Grupos de noticias Usenet</a></li>
  <li><a href="#forums">Foros de usuarios de Debian</a></li>
  <li><a href="#maintainers">Cómo contactar con los o las responsables de paquetes</a></li>
  <li><a href="#bts">Sistema de seguimiento de fallos</a></li>
  <li><a href="#release">Problemas conocidos</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> La ayuda técnica de Debian es ofrecida por un grupo de personas voluntarias. Si esta ayuda dinamizada por la comunidad no cubre sus necesidades y no encuentra la respuesta en nuestra <a href="doc/">documentación</a>, puede contratar a un <a href="consultants/">consultor o consultora</a> para que responda a sus preguntas o para que mantenga o añada funcionalidades adicionales a su sistema Debian.</p>
</aside>

<h2><a id="irc">IRC (ayuda en tiempo real)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) es una buena forma de hablar con personas de todo el mundo en tiempo real. Es un sistema de chat basado en texto. En IRC puede entrar a salas de conversación (llamadas canales) o hablar directamente con personas individuales por medio de mensajes privados.
</p>

<p>
Puede encontrar canales dedicados a Debian en <a href="https://www.oftc.net/">OFTC</a>. Nuestra <a href="https://wiki.debian.org/IRC">Wiki</a> incluye un listado completo de canales de Debian. También puede usar un <a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">motor de búsquedas</a> para localizar canales relacionados con Debian.
</p>

<h3>Clientes IRC</h3>

<p>
Para conectarse a la red IRC puede utililizar el <a href="https://www.oftc.net/WebChat/">WebChat</a> de OFTC en el navegador web de su elección o instalar un cliente en su computadora. Hay muchos clientes disponibles, algunos con interfaz gráfica y algunos para consola. Varios clientes IRC populares están empaquetados para Debian, como, por ejemplo:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (modo texto)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (modo texto)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat</a> (GTK)</li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul> 

<p>
La Wiki de Debian ofrece una lista más completa de <a href="https://wiki.debian.org/IrcClients">clientes IRC</a> que están disponibles como paquetes Debian.
</p>

<h3>Conexión a la red</h3>

<p>
Una vez que tenga el cliente instalado, necesitará decirle que se conecte
al servidor. En la mayoría de los clientes lo podrá hacer con:
</p>

<pre>
/server irc.debian.org
</pre>

<p>El nombre de máquina irc.debian.org es un alias de irc.oftc.net. En cambio, en algunos clientes (como irssi) necesitará escribir lo siguiente:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Únase a un canal</h3>

<p>Una vez conectado, únase al canal <code>#debian</code> (en inglés) mediante esta orden:</p>

<pre>
/join #debian
</pre>

<p>O al canal <code>#debian-es</code> (en castellano) escribiendo:</p>

<pre>
/join #debian-es
</pre>

<p>Nota: los clientes con interfaz gráfica como HexChat o Konversation a menudo tienen un botón
o una entrada de menú para conectarse a servidores y unirse a canales.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Lea nuestras FAQ de IRC</a></button></p>

<h2><a id="mail_lists">Listas de correo</a></h2>

<p>
Más de mil <a href="intro/people.en.html#devcont">desarrolladores</a> en activo repartidos por todo el mundo trabajan en Debian en su tiempo libre (y en sus propias zonas horarias). Por eso nos comunicamos, principalmente, a través de correo electrónico. De manera similar, la mayoría de las conversaciones entre desarrolladores y usuarios de Debian tienen lugar en diferentes <a href="MailingLists/">listas de correo</a>:
</p>

# Nota a los traductores:
# Tal vez quieras adaptar el siguiente parágrafo, indicando qué lista
# está disponible para los usuarios en tu idioma en vez de en inglés.

<ul>
  <li>Para obtener ayuda técnica en español, escriba a la lista de correo <a href="https://lists.debian.org/debian-user/">debian-user-spanish</a>.</li>
  <li>Para obtener ayuda técnica en otros idiomas, revise el <a href="https://lists.debian.org/users.html">índice</a> de otras listas de correo para usuarios.</li>
</ul>

<p>
Puede navegar por nuestros <a href="https://lists.debian.org/">archivos de listas de correo</a> o <a href="https://lists.debian.org/search.html">hacer búsquedas</a> en ellos sin necesidad de suscribirse.
</p>

<p>
Por supuesto, hay muchas otras listas de correo, que están dedicadas a algún aspecto del vasto ecosistema Linux no específico de Debian. Use su motor de búsqueda favorito para encontrar la lista más adecuada a su propósito.
</p>

<h2><a id="usenet">Grupos de noticias Usenet</a></h2>

<p>
Puede leer muchas de nuestras <a href="#mail_lists">listas de correo</a> como grupos de noticias, en la jerarquía <kbd>linux.debian.*</kbd>.
</p>

<h2><a id="forums">Foros de usuarios de Debian</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="https://exdebian.org/forum">Foro de usuarios de Debian en español</a> 

<p>
<a href="https://forums.debian.net">Foros de usuarios de Debian</a> es un portal web
en el que miles de usuarios y usuarias discuten cuestiones relacionadas con Debian, hacen preguntas,
y se ayudan entre ellos respondiéndolas. Puede leer todos los tableros sin
necesidad de registrarse. Si quiere participar en la discusión y publicar
sus propios mensajes, por favor, regístrese.
</p>

<h2><a id="maintainers">Cómo contactar con los o las responsables de paquetes</a></h2>

<p>
Básicamente, hay dos maneras de ponerse en contacto con el responsable de un paquete Debian:
</p>

<ul>
  <li>Si desea informar de un fallo, simplemente envíe un <a href="Bugs/Reporting">informe de fallo</a>; el mantenedor recibe automáticamente una copia del mismo.</li>
  <li>Si simplemente quiere enviar un correo electrónico al responsable, use los alias de correo electrónico definidos para cada paquete:<br>
      &lt;<em>nombre_de_paquete</em>&gt;@packages.debian.org</li>
</ul>


<h2><a id="bts">Sistema de seguimiento de fallos</a></h2>

<p>
La distribución Debian tiene su propio <a href="Bugs/">sistema de seguimiento</a> con fallos remitidos por usuarios y desarrolladores. Cada fallo tiene un número único y se guarda registro de él hasta que se marca como solucionado. Hay dos maneras de informar de un fallo:
</p>

<ul>
  <li>La recomendada es utilizar el paquete Debian <em>reportbug</em>.</li>
  <li>Alternativamente, puede enviar un correo electrónico como se describe en esta <a href="Bugs/Reporting">página</a>.</li>
</ul>

<h2><a id="release">Problemas conocidos</a></h2>

<p>Las limitaciones y problemas graves de la distribución «estable» actual
(si los hay) se detallan en <a href="releases/stable/">la página de información sobre la distribución</a>.</p>

<p>Preste atención especial a las <a href="releases/stable/releasenotes">notas de
publicación</a> y a las <a href="releases/stable/errata">erratas</a>.</p>
