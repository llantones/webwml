#use wml::debian::translation-check translation="022e55922f28ad2d0ab4d75ed4d28a027bafddfa" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В BIND, реализации DNS-сервера, было обнаружено несколько
уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

    <p>Грег Кюхле обнаружил, что специально сформированная входящая IXFR-передача
    может вызывать ошибку утверждения в службе named, что приводит к отказу
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

    <p>Сива Какарла обнаружил, что работа named может быть завершена аварийно, если запись
    DNAME помещается в раздел ANSWER, когда изменение DNAME оказывается
    окончательным ответом на клиентский запрос.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

    <p>Было обнаружено, что реализация SPNEGO, используемая в BIND, уязвима
    к переполнению буфера. Данное обновление переключается на использование
    реализации SPNEGO из библиотек Kerberos.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:9.11.5.P4+dfsg-5.1+deb10u5.</p>

<p>Рекомендуется обновить пакеты bind9.</p>

<p>С подробным статусом поддержки безопасности bind9 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4909.data"
