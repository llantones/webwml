#use wml::debian::template title="Debians världsomspännande spegelplatser" BARETITLE=true
#use wml::debian::translation-check translation="d83882f2dd9b20c81d0cb83b32c028d9765578c7"

<p>Detta är en <strong>fullständig</strong> lista över Debianspeglar.
För varje plats listas de olika typerna av tillgängligt material
tillsammans med åtkomstmetoden för varje typ.</p>

<p>Följande saker speglas:</p>

<dl>
<dt><strong>Paket</strong></dt>
	<dd>Debians paketpool.</dd>
<dt><strong>CD-avbildningar</strong></dt>
	<dd>Officiella Debian-CD-avbildningar. Se
	<url "https://www.debian.org/CD/"> för detaljer.</dd>
<dt><strong>Gamla utgåvor</strong></dt>
	<dd>Arkivet över gamla Debianutgåvor.
	<br />
	Några av de gamla utgåvorna innehåller också det så kallade
        debian-non-US-arkivet, med sektioner för Debianpaket som inte kan
	distribueras i USA på grund av mjukvarupatent eller användning av kryptering.
	Uppdateringar i debian-non-US avrböts i och med Debian 3.1.</dd>
</dl>

<p>Följande åtkomstmetoder är möjliga:</p>

<dl>
<dt><strong>HTTP</strong></dt>
	<dd>Vanlig webbåtkomst, men kan användas för att hämta ner filer.</dd>
<dt><strong>rsync</strong></dt>
	<dd>Ett effektivt sätt att spegla.</dd>
</dl>

<p>Den officiella kopian av följande lista kan alltid hittas på:
<url "https://www.debian.org/mirror/list-full">.
<br />
Allt annat du vill veta om Debians speglar:
<url "https://www.debian.org/mirror/">.
</p>

<hr style="height:1">

<p>Hoppa direkt till ett land i listan:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"
