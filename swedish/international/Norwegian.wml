#use wml::debian::template title="Debians norska hörna"
#use wml::debian::translation-check translation="7ff1e58fbad65b9683e41dcd4c9d35fe7c22ecef"

<p>På denna sida hittar du information för norska Debiananvändare.
Om du har, eller känner till, information du tycker hör hit, ta kontakt
med någon av de norska <a href="#translators">översättarna</a>.</p>

<h2>Sändlistor</h2>

<p>Debian har för tillfället inga officiella sändlistor på norska.
Om det finns intresse för det, kan vi starta en eller flera sådana listor
för norska användare, utvecklare och/eller översättare.</p>

<h2>Länkar</h2>

<p>Några länkar som kan vara av intresse för norska Debiananvändare:
</p>

  <ul>
   <!--
      Organisationen Linux Norge verkar inte existera längre,
      Domänen linux.no fungera, men den är helt utdaterad.
   -->
    <!-- <li><a href="http://www.linux.no/">Linux Norge</a><br>
   <em>"En frivilligorganisation som skall sprida information om
   operativsystemet Linux i Norge."</em>
   Erbjuder bland annat det
    </li> -->

   <li><a href="https://nuug.no/">Norwegian UNIX User Group</a>. Besök deras
   Wikisida om <a href="https://wiki.nuug.no/likesinnede-oversikt">likasinnade
   organisationer</a> som listar de flesta/alla aktiva Linux användargrupper
   i Norge.
   </li>

  </ul>


<h2>Norska bidragslämnare till Debianprojektet</h2>

<p>För tillfället finns följande norska Debianutvecklare:
</p>

    <ul>
      <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
      <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
      <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
      <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
      <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
      <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
      <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
      <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
      <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
      <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
    </ul>

<p>
Listan uppdateras sällan, så för àjourförd information bör du se
<a href="https://db.debian.org/">Debian Developers Database</a> &ndash; välj
&rdquo;Norway&rdquo; som &rdquo;country&rdquo;.</p>

    <h3>Tidigare Norska Debianutvecklare:</h3>

    <ul>
      <li>Morten Hustvei</li>
      <li>Ole J. Tetlie</li>
      <li>Peter Krefting</li>
      <li>Tom Cato Amundsen</li>
      <li>Tore Anderson</li>
      <li>Tor Slettnes</li>
     </ul>

    <a id="translators"></a>
    <h3>Översättare:</h3>

    <p>Debians webbsidor översätts för närvarande till norska av:</p>


    <ul>
      <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
    </ul>

<p>
Om du är intresserad bör du börja med att läsa
<a href="$(DEVEL)/website/translating">informationen för översättare</a>.
</p>

<p>
Debians webbsidor har tidigare översatts av:
</p>

  <ul>
      <li>Cato Auestad</li>
      <li>Stig Sandbeck Mathisen</li>
      <li>Tollef Fog Heen</li>
      <li>Tor Slettnes</li>
  </ul>

<p>
Om du har lust att hjälpa till, eller om du känner någon som på ett eller
annat sätt är involverad i Debianprojektet, kontakta någon av oss.
</p>
