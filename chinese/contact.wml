#use wml::debian::template title="如何联系我们" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

# Translator: Anthony Wong <ypwong@gmail.com>, 1999
# Translator: Chuan-kai Lin <cklin@debian.org>, 2000
# Translator: Franklin <franklin@goodhorse.idv.tw>, 2002/11/15
# Translator: yangfl <mmyangfl@gmail.com>, 2017
# Checked by foka, 2002/11/24
# Checked by cctsai, 2004/11/05

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">一般信息</a>
  <li><a href="#installuse">Debian 的安裝與使用</a>
  <li><a href="#press">宣传和新闻</a>
  <li><a href="#events">活動和研討會</a>
  <li><a href="#helping">協助 Debian</a>
  <li><a href="#packageproblems">報告 Debian 套件中的錯誤</a>
  <li><a href="#development">Debian 的開發</a>
  <li><a href="#infrastructure">Debian 基础设施的問題</a>
  <li><a href="#harassment">骚扰问题</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 我們希望您最初來聯繫的時候先使用<strong>英文</strong>，因为英文是我们大多数工作人员使用的语言。如果您不會英文，請到我们的<a href="https://lists.debian.org/users.html#debian-user">使用者通信論壇</a>之一寻求帮助，这组通信論壇支持许多种不同的语言。</p>
</aside>

<p>
Debian 是一個很大的組織，有很多方法可以與我們的成员和其他 Debian 用户取得聯繫。這個網頁會总结一些常用的方法，而這些絕不是全部。其他的聯繫方法請參考網頁的其他部份。
</p>

<p>
请注意，下面列出的大多数邮件地址都是公开的邮件列表，讨论会被公开存档。请在发送任何消息前阅读<a href="$(HOME)/MailingLists/disclaimer">免责声明</a>。
</p>

<h2 id="generalinfo">一般信息</h2>

<p>
大部分关于 Debian 的信息都可以在我們的 <a href="$(HOME)">https://www.debian.org/</a> 網站上找到，所以在與我們聯繫前請先瀏覽及<a href="$(SEARCH)">搜尋</a>我們的網站。
</p>

<p>
關於 Debian 計劃的問題可以发送到 <email debian-project@lists.debian.org> 通信論壇上。請不要在上面询问关于 Linux 的一般性問題。请继续阅读本页面以了解关于其他邮件列表的信息。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">阅读我们的 FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Debian 的安裝與使用</h2>

<p>
如果您很確定安装介质和我们的网站上的[CN:文档:][HKTW:文件:]不能解決您的問題，\
我們有一個相當活躍的通信論壇，也許上面的 Debian 使用者與開發者可以回答您的問題。\
您可以在上面提出和下列主题相关的一切问题：
</p>

<ul>
  <li>安裝</li>
  <li>[CN:配置:][HKTW:設定:]</li>
  <li>硬體<tw支援></li>
  <li>系統管理</li>
  <li>使用 Debian</li>
</ul>

<p>
请<a href="https://lists.debian.org/debian-user/">订阅</a>此邮件列表，并将您的\
问题发送到 <email debian-user@lists.debian.org>。
</p>

<p>
除此以外，我們也有專為各種不同語言的用戶而設的通信論壇。詳情請見<a href="https://lists.debian.org/users.html#debian-user">國際通信論壇</a>的訂閱信息。
中文用戶可以訂閱 [CN:<em>debian-chinese-gb</em>（簡體中文）或
<em>debian-chinese-big5</em>（繁體中文）:]
[HKTW:<em>debian-chinese-big5</em>（繁體中文）或
<em>debian-chinese-gb</em>（簡體中文）:]通信論壇。
</p>

<p>
您无需订阅，即可浏览我们的<a href="https://lists.debian.org/">邮件列表存档</a>或<a href="https://lists.debian.org/search.html">搜索</a>存档。
</p>

<p>
此外，您还可以像瀏覽新聞群組一样瀏覽我們的通信論壇。
</p>

<p>
如果您在我們的安裝系統中發現錯誤，請將相關信息发送到 <email debian-boot@lists.debian.org>。您也可以向 <a href="https://bugs.debian.org/debian-installer">debian-installer</a> 伪软件包提出<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">錯誤報告</a>。
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">宣传和新闻</h2>

<p>
<a href="https://wiki.debian.org/Teams/Publicity">Debian 宣传团队</a>管理 Debian 官方渠道的所有新闻和通告，包括部分邮件列表、博客、微新闻网站和官方社交媒体频道。 
</p>

<p>
如果您在撰写关于 Debian 的内容，并且需要帮助或者信息，请联系<a href="mailto:press@debian.org">宣传团队</a>。如果您想把一些新闻放到我们的新闻网页上，也请发送邮件到这个地址。
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">活動和研討會</h2>

<p>
请将<a href="$(HOME)/events/">研讨会</a>、展览会或其他活动的邀请函发送到 <email events@debian.org>。
</p>

<p>
歐洲地區索取傳單，海報與活動參與等要求應送到 <email debian-events-eu@lists.debian.org>  邮件列表。
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">协助 Debian</h2>

<p>
有多种方式可以支持 Debian 计划并向本发行版作出贡献。如果您愿意提供帮助，请先阅读我们的<a href="devel/join/">如何参与 Debian</a> 页面。
</p>

<p>如果您願意維護一個 Debian 映射站台，請參考 <a href="mirror/">Debian 映射站台</a>。
新的映射站台可填写<a href="mirror/submit">这个表单</a>注冊。現有的映射站台的問題
可以報告到 <email mirrors@debian.org>。
</p>

<p>如果您打算販賣 Debian 光碟，請參考 <a href="CD/vendors/info">CD 販賣信息</a>。
要加入 CD 販賣者清單，请使用<a href="CD/vendors/adding-form">这个表单</a>。
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">報告 Debian 套件中的錯誤</h2>

<p>如果您想要向 Debian 套件提交錯誤报告，我們有一套錯誤追蹤系統讓您可以報告
您遇到的問題。請阅读<a href="Bugs/Reporting">提交錯誤報告的步骤</a>。
</p>

<p>如果您只是單純想要跟套件維護者聯絡，您可以使用为每个软件包设立的特殊的
邮件地址别名。任何寄到 &lt;<var>软件包名</var>&gt;@packages.debian.org 的郵件
會自動被[CN:转发:][HKTW:轉寄:]到該套件維護者的信箱。
</p>

<p>如果您想要秘密地通知开发者 Debian 中存在的一个安全问题，请发送邮件到 <email security@debian.org>。
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Debian 的开发</h2>

<p>如果您有開發上的問題，有一些
<a href="https://lists.debian.org/devel.html">Debian 開發通信論壇</a>可以讓您跟開發
人員接觸與交流。
</p>

<p>
也有一个一般的開發人員通信論壇：<email debian-devel@lists.debian.org>。
请点击此<a href="https://lists.debian.org/debian-devel/">链接</a>进行订阅。
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Debian 基础设施的問題</h2>

<p>要回報關於 Debian 服務上的問題，您可以使用適當的
<a href="Bugs/pseudo-packages">伪套件</a>來<a href="Bugs/Reporting">報告錯誤</a>。
</p>

<p>您也可以寄信到以下的電子郵件地址：</p>

<define-tag btsurl>软件包：<a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>网站编辑</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>網頁中文翻譯協調者</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>通信論壇管理員和存檔維護者</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>錯誤追蹤系統管理員</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">骚扰问题</h2>

<p>Debian 是一個重視彼此尊重的[CN:社区:][HKTW:社群:]。
如果您是不当行爲的受害者，如果计划的某位成员伤害了您，或者您感觉遇到了骚扰，
请联系[CN:社区:][HKTW:社群:]团队：<email community@debian.org>。
</p>
