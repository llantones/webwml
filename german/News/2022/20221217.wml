#use wml::debian::translation-check translation="f878955e279294523a604a4e9bbd3d93dd0f3cc7" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11: 11.6 veröffentlicht</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die sechste Aktualisierung seiner Stable-Distribution 
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese 
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung 
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen 
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt. 
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete 
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf 
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele 
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind 
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen 
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction awstats "Seitenübergreifendes Skripting unterbunden [CVE-2022-46391]">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung 11.6 aktualisiert">
<correction binfmt-support "binfmt-support.service nach systemd-binfmt.service ausführen">
<correction clickhouse "Lesezugriff außerhalb der Grenzen abgestellt [CVE-2021-42387 CVE-2021-42388], Probleme mit Pufferüberlauf behoben [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "CRI-Plugin: goroutine-Leck während Ausführung behoben [CVE-2022-23471]">
<correction core-async-clojure "Kompilierungsfehlschläge im Testzustand behoben">
<correction dcfldd "SHA1-Ausgabe auf Big-Endian-Architekturen behoben">
<correction debian-installer "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 5.10.0-20 angehoben">
<correction debian-installer-netboot-images "Neubau gegen proposed-updates">
<correction debmirror "non-free-firmware zur Liste der Standard-Sektionen hinzugefügt">
<correction distro-info-data "Ubuntu 23.04, Lunar Lobster, hinzugefügt; Enddaten von Debian ELTS eingetragen; Veröffentlichungsdatum von Debian 8 (Jessie) korrigiert">
<correction dojo "Prototype-Pollution behoben [CVE-2021-23450]">
<correction dovecot-fts-xapian "Abhängigkeit von genau der Dovecot-ABI, die während der Kompilierung benutzt wurde, erzeugt">
<correction efitools "Unregelmäßige Kompilierungsfehlschläge wegen fehlerhafter Abhängigkeit im Makefile behoben">
<correction evolution "Google-Kontakte-Adressbücher auf CalDAV umgestellt, weil das Google-Kontakte-API abgestellt wurde">
<correction evolution-data-server "Google-Kontakte-Adressbücher auf CalDAV umgestellt, weil das Google-Kontakte-API abgestellt wurde; Kompatibilität mit OAuth-Änderungen in Gmail hergestellt">
<correction evolution-ews "Abruf von Benutzerzertifikaten, die zu Kontakten gehören, überarbeitet">
<correction g810-led "Gerätezugriff mit uaccess kontrollieren, statt alles für alle Welt schreibbar zu machen [CVE-2022-46338]">
<correction glibc "Rückschritt in wmemchr und wcslen auf CPUs, die über AVX2, aber nicht über BMI2 verfügen (wie Intel Haswell), behoben">
<correction golang-github-go-chef-chef "Gelegentliche Testfehlschläge behoben">
<correction grub-efi-amd64-signed "Xen-Binärdateien stehen lassen, damit sie wieder funktionieren; Schriftarten in das Memdisk-Kompilat für EFI-Abbilder einbinden; Fehler im Kern-Dateicode behoben, sodass besser mit Fehlern umgegangen wird; Debian-SBAT-Stufe auf 4 angehoben">
<correction grub-efi-arm64-signed "Xen-Binärdateien stehen lassen, damit sie wieder funktionieren; Schriftarten in das Memdisk-Kompilat für EFI-Abbilder einbinden; Fehler im Kern-Dateicode behoben, sodass besser mit Fehlern umgegangen wird; Debian-SBAT-Stufe auf 4 angehoben">
<correction grub-efi-ia32-signed "Xen-Binärdateien stehen lassen, damit sie wieder funktionieren; Schriftarten in das Memdisk-Kompilat für EFI-Abbilder einbinden; Fehler im Kern-Dateicode behoben, sodass besser mit Fehlern umgegangen wird; Debian-SBAT-Stufe auf 4 angehoben">
<correction grub2 "Xen-Binärdateien stehen lassen, damit sie wieder funktionieren; Schriftarten in das Memdisk-Kompilat für EFI-Abbilder einbinden; Fehler im Kern-Dateicode behoben, sodass besser mit Fehlern umgegangen wird; Debian-SBAT-Stufe auf 4 angehoben">
<correction hydrapaper "Fehlende Abhängigkeit von python3-pil nachgetragen">
<correction isoquery "Testfehlschlag behoben, der durch eine Änderung in der französischen Übersetzung im iso-codes-Paket verursacht wurde">
<correction jtreg6 "Neues Paket, nötig zum Kompilieren neuerer openjdk-11-Versionen">
<correction lemonldap-ng "Übertragung der Beendigung von Sitzungen verbessert [CVE-2022-37186]">
<correction leptonlib "Teilung durch null behoben [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Offene Weiterleitung behoben [CVE-2021-3639]">
<correction libbluray "BD-J-Unterstützung bei neuen Oracle-Java-Aktualisierungen überarbeitet">
<correction libconfuse "Heap-basiertes Puffer-Überauslesen in cfg_tilde_expand behoben [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libtasn1-6 "Lesezugriff außerhalb der Grenzen behoben [CVE-2021-46848]">
<correction libvncserver "Speicherleck behoben [CVE-2020-29260]; größere Bildschirmabmessungen unterstützen">
<correction linux "Neue stabile Veröffentlichung der Originalautoren; ABI auf 20 angehoben; [rt] Aktualisierung auf 5.10.158-rt77">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 20 angehoben; [rt] Aktualisierung auf 5.10.158-rt77">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 20 angehoben; [rt] Aktualisierung auf 5.10.158-rt77">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 20 angehoben; [rt] Aktualisierung auf 5.10.158-rt77">
<correction mariadb-10.5 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "X-Client-IP-Kopfzeile fallen lassen, wenn sie nicht vertrauenswürdig ist [CVE-2022-2255]">
<correction mplayer "Mehrere Sicherheitsprobleme behoben [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "gpgme-Absturz beim Auflisten der Schlüssel in einem Public-Key-Block sowie beim Public-Key-Block-Auflisten für ältere Versionen von gpgme behoben">
<correction nano "Abstürze und mögliche Datenverluste behoben">
<correction nftables "Um-eins-daneben- und Doppel-free-Probleme behoben">
<correction node-hawk "URLs via stdlib verarbeiten [CVE-2022-29167]">
<correction node-loader-utils "Prototyp-Pollution [CVE-2022-37599 CVE-2022-37601], Regulärausdruck-basierende Dienstblockade behoben [CVE-2022-37603]">
<correction node-minimatch "Schutz gegen Regulärausdruck-basierende Dienstblockade verbessert [CVE-2022-3517]; Regression in der Korrektur für CVE-2022-3517 behoben">
<correction node-qs "Prototyp-Pollution behoben [CVE-2022-24999]">
<correction node-xmldom "Prototyp-Pollution behoben [CVE-2022-37616]; Einfügen nicht wohlgeformter Knoten verhindert [CVE-2022-39353]">
<correction nvidia-graphics-drivers "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Fehlende Abhängigkeit des ibjs-jquery vom omnievents-doc-Paket nachgetragen">
<correction onionshare "Dienstblockade unterbunden [CVE-2022-21689], HTML-Injektion abgestellt [CVE-2022-21690]">
<correction openvpn-auth-radius "verify-client-cert-Direktive unterstützen">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren">
<correction postgresql-13 "Neue stabile Veröffentlichung der Originalautoren">
<correction powerline-gitstatus "Befehlsinjektion durch schadhafte Depotkonfiguration behoben [CVE-2022-42906]">
<correction pysubnettree "Modulkompilierung überarbeitet">
<correction speech-dispatcher "espeak-Puffergröße gesenkt, um Synthetisierungsartefakte zu reduzieren">
<correction spf-engine "Startprobleme von pyspf-milter wegen fehlerhaftem Import-Statement gelöst">
<correction tinyexr "Heap-Überläufe beseitigt [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Endlosschleife behoben [CVE-2021-42260]">
<correction tzdata "Daten für Fiji, Mexiko und Palästina aktualisiert; Schaltsekundenliste aktualisiert">
<correction virglrenderer "Schreibzugriff außerhalb der Grenzen abgestellt [CVE-2022-0135]">
<correction x2gothinclient "Dafür gesorgt, dass das x2gothinclient-minidesktop-Paket das virtuelle Paket lightdm-greeter mitbringt">
<correction xfig "Pufferüberlauf behoben [CVE-2021-40241]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für 
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält, 
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern 
Freier Software, die ihre Zeit und Mühen einbringen, um das 
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail 
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das 
Stable-Veröffentlichungs-Team (auf Englisch) 
unter &lt;debian-release@lists.debian.org&gt;.</p>
