#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11 aktualisiert: 11.3 veröffentlicht</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die dritte Aktualisierung seiner Stable-Distribution 
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese 
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung 
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen 
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt. 
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete 
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf 
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele 
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind 
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen 
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apache-log4j1.2 "Sicherheitsprobleme [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] durch Entfernen der Unterstützung für das JMSSink-, das JDBCAppender-, das JMSAppender- und das Apache-Chainsaw-Modul behoben">
<correction apache-log4j2 "Problem mit Codeausführung aus der Ferne behoben [CVE-2021-44832]">
<correction apache2 "Neue Veröffentlichung der Originalautoren; Absturz wegen zufälliger Speicherlesung behoben [CVE-2022-22719]; HTTP-Anfrageschmuggel beseitigt [CVE-2022-22720]; Problem mit Schreibzugriff außerhalb der Grenzen behoben [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Informationsleck abgedichtet [CVE-2021-46671]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 11.3">
<correction bible-kjv "Off-by-one-Error (Um-eins-daneben-Fehler) in der Suche behoben">
<correction chrony "Einlesen der chronyd-Konfigurationsdatei, die timemaster(8) erzeugt, erlauben">
<correction cinnamon "Absturz beim Hinzufügen eines Online-Accounts mit Anmeldung behoben">
<correction clamav "Neue stabile Version der Originalautoren; Problem mit Dienstblockade behoben [CVE-2022-20698]">
<correction cups-filters "Apparmor: Lesen in Debian Edus cups-browsed-Konfigurationsdatei erlauben">
<correction dask.distributed "Unerwünschtes Abhören der öffentlichen Schnittstellen durch Worker unterbunden [CVE-2021-42343]; Kompatibilität mit Python 3.9 verbessert">
<correction debian-installer "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 5.10.0-13 angehoben">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-ports-archive-keyring "<q>Debian Ports Archive Automatic Signing Key (2023)</q> hinzugefügt; 2021er-Signierschlüssel in den <q>removed</q>-Schlüsselbund umgehängt">
<correction django-allauth "OpenID-Unterstützung überarbeitet">
<correction djbdns "Datenbegrenzungen für axfrdns, dnscache und tinydns angehoben">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction e2guardian "Problem mit fehlender SSL-Zertifikatsvalidierung behoben [CVE-2021-44273]">
<correction epiphany-browser "Fehler in GLib umgangen, um einen Absturz des UI-Prozesses loszuwerden">
<correction espeak-ng "Merkwürdige 50ms-Verzögerung beim Verarbeiten von Events beseitigt">
<correction espeakup "debian/espeakup.service: espeakup vor System-Überlastungen beschützen">
<correction fcitx5-chinese-addons "fcitx5-table: fehlende Abhängigkeiten von fcitx5-module-pinyinhelper und fcitx5-module-punctuation hinzugefügt">
<correction flac "Problem mit Schreibzugriff außerhalb der Grenzen behoben [CVE-2021-0561]">
<correction freerdp2 "Zuätzliche Fehlersuchprotokollierung abgeschaltet">
<correction galera-3 "Neue Veröffentlichung der Originalautoren">
<correction galera-4 "Neue Veröffentlichung der Originalautoren">
<correction gbonds "Treasury-API für Auszahlungsdaten verwenden">
<correction glewlwyd "Mögliche Privilegieneskalation behoben">
<correction glibc "Schlechte Umwandlung von ISO-2022-JP-3 mit iconv behoben [CVE-2021-43396]; Probleme mit Pufferüberlauf behoben [CVE-2022-23218 CVE-2022-23219]; Weiternutzung abgegebenen Speichers (use-after-free) abgestellt [CVE-2021-33574]; Austauschen älterer Versionen von /etc/nsswitch.conf unterbunden; Überprüfung der unterstützten Kernel-Versionen vereinfacht, weil 2.x-Kernel nicht länger unterstützt werden; Unterstützung der Installation auf Kerneln mit einer Veröffentlichungsnummer oberhalb von 255 hinzugefügt">
<correction glx-alternatives "Nach dem ersten Einrichten der Umleitungen eine Minimalversion der umgeleiteten Dateien installieren, sodass die Bibliotheken nicht vermisst werden, bis glx-alternative-mesa seine Trigger verarbeitet hat">
<correction gnupg2 "scd: CCID-Treiber für SCM SPR332/SPR532 überarbeitet; Netzwerkinteraktion im Generator vermeiden, weil diese zu Hängern führen kann">
<correction gnuplot "Teilung durch null abgestellt [CVE-2021-44917]">
<correction golang-1.15 "IsOnCurve für big.Int-Werte, die keine gültigen Koordinaten sind, behoben [CVE-2022-23806]; math/big: Speicher-Großverbrauch in Rat.SetString verhindert [CVE-2022-23772]; cmd/go: Zweige davon abhalten, sich als Versionen zu verselbstsändigen [CVE-2022-23773]; Stack-Erschöpfung beim Kompilieren von tief verschachtelten Ausdrücken behoben [CVE-2022-24921]">
<correction golang-github-containers-common "seccomp-Unterstützung aktualisiert, damit auch neuere Kernel-Versionen verwendet werden können">
<correction golang-github-opencontainers-specs "seccomp-Unterstützung aktualisiert, damit auch neuere Kernel-Versionen verwendet werden können">
<correction gtk+3.0 "Fehlende Suchergebnisse bei Verwendung von NFS wiederhergestellt; Waylands Zwischenablagenroutinen davon abgehalten, in gewissen Spezialfällen einzufrieren; Druck auf per mDNS entdeckten Druckern verbessert">
<correction heartbeat "Erstellung von /run/heartbeat auf Systemen mit systemd überarbeitet">
<correction htmldoc "Lesezugriff außerhalb der Grenzen abgestellt [CVE-2022-0534]">
<correction installation-guide "Dokumentation und Übersetzungen aktualisiert">
<correction intel-microcode "Enthaltenen Microcode aktualisiert; einige Sicherheitsprobleme umgangen [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "<q>mktemp</q> anstelle des veralteten <q>tempfile</q> verwenden, um Warnungen zu vermeiden">
<correction lemonldap-ng "Authentifizierungsprozess in Passwort-Test-Plugins überarbeitet [CVE-2021-40874]">
<correction libarchive "Extraktion von Hardlinks zu symbolischen Links überarbeitet; Umgang mit ACLs von symbolischen Links überarbeitet [CVE-2021-23177]; beim Setzen von Datei-Flags niemals symbolischen Links folgen [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libgdal-grass "Neukompilierung gegen grass 7.8.5-1+deb11u1">
<correction libpod "seccomp-Unterstützung aktualisiert, damit auch neuere Kernel-Versionen verwendet werden können">
<correction libxml2 "Weiterverwendung von abgegebenem Speicher (use-after-free) abgestellt [CVE-2022-23308]">
<correction linux "Neue stabile Version der Originalautoren; [rt] Aktualisierung auf 5.10.106-rt64; ABI auf 13 angehoben">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren; [rt] Aktualisierung auf 5.10.106-rt64; ABI auf 13 angehoben">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren; [rt] Aktualisierung auf 5.10.106-rt64; ABI auf 13 angehoben">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren; [rt] Aktualisierung auf 5.10.106-rt64; ABI auf 13 angehoben">
<correction mariadb-10.5 "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "»Beschädigt:« bei älteren Versionen von libmpich1.0-dev hinzugefügt, um einige Upgrade-Probleme zu lösen">
<correction mujs "Problem mit Pufferüberlauf behoben [CVE-2021-45005]">
<correction mutter "Verschiedene Korrekturen aus dem stabilen Zweig der Originalautoren zurückportiert">
<correction node-cached-path-relative "Prototype Pollution behoben [CVE-2021-23518]">
<correction node-fetch "Keine sicheren Kopfzeilen an Domains von Dritten weiterleiten [CVE-2022-0235]">
<correction node-follow-redirects "Die Cookie-Kopfzeile nicht über Domains hinweg weiterleiten [CVE-2022-0155]; keine vertraulichen Kopfzeilen über Schemata hinweg weiterleiten [CVE-2022-0536]">
<correction node-markdown-it "Auf regulären Ausdrücken basierende Dienstblockade verhindert[CVE-2022-21670]">
<correction node-nth-check "Auf regulären Ausdrücken basierende Dienstblockade verhindert [CVE-2021-3803]">
<correction node-prismjs "Markup in Befehlszeilenausgabe maskieren [CVE-2022-23647]; minifizierte Dateien aktualisiert, um sicherzustellen, dass die RegEx-basierte Dienstblockade behoben ist [CVE-2021-3801]">
<correction node-trim-newlines "Auf regulären Ausdrücken basierende Dienstblockade verhindert [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb: Nicht-funktionierende Python-Unterstützung abgestellt, weil sie Speicherzugriffsfehler verursacht; Schnappschuss von openjdk-8-jre verwenden (8u312-b07-1)">
<correction nvidia-graphics-drivers-tesla-450 "Neue Veröffentlichung der Originalautoren; Probleme mit Dienstblockaden behoben [CVE-2022-21813 CVE-2022-21814]; nvidia-kernel-support: /etc/modprobe.d/nvidia-options.conf als Vorlage anbieten">
<correction nvidia-modprobe "Neue Veröffentlichung der Originalautoren">
<correction openboard "Anwendungssymbol korrigiert">
<correction openssl "Neue Veröffentlichung der Originalautoren; armv8-Zeiger-Authentifikation überarbeitet">
<correction openvswitch "Weitverwendung von abgegebenem Speicher (use-after-free) behoben [CVE-2021-36980]; Installationsprozess von libofproto korrigiert">
<correction ostree "Kompatiblität mit eCryptFS verbessert; unendliche Rekursion bei der Wiederherstellung nach gewissen Fehlern verhindert; Commits vor dem Herunterladen als »partial« (teilweise) markieren; Assertionsfehlschlag behoben, der auftritt, wenn eine Rückportierung oder eine lokal kompilierte Version von GLib &gt;= 2.71 verwendet wird; Fähigkeit zum Abholen von OSTree-Inhalten von Pfaden, die Nicht-URI-Zeichen (wie umgekehrten Schrägstrichen) oder Nicht-ASCII-Zeichen enthalten, überarbeitet">
<correction pdb2pqr "Kompatiblität von propka mit Python 3.8 oder höher verbessert">
<correction php-crypt-gpg "Verhindert, dass zusätzliche Optionen an GPG weitergegeben werden [CVE-2022-24953]">
<correction php-laravel-framework "Problem mit seitenübergreifendem Skripting behoben [CVE-2021-43808], fehlende Blockierung für Hochladen von ausführbaren Inhalten nachgereicht [CVE-2021-43617]">
<correction phpliteadmin "Problem mit seitenübergreifendem Skripting behoben [CVE-2021-46709]">
<correction prips "Unendlichen Umbruch, wenn ein Bereich an 255.255.255.255 stößt, behoben; CIDR-Ausgabe mit Adressen, die sich in ihrem ersten Bit unterscheiden, überarbeitet">
<correction pypy3 "Kompilierungsfehlschläge durch Entfernen eines überflüssigen #endif aus import.h behoben">
<correction python-django "Probleme mit Dienstblockade [CVE-2021-45115], Informationsoffenlegung [CVE-2021-45116], Verzeichnisüberschreitung [CVE-2021-45452] behoben; Traceback im Rahmen der Handhabung von RequestSite/get_current_site() wegen eines Kreisimports behoben">
<correction python-pip "Race-Condition bei Verwendung von per ZIP importierten Abhängigkeiten behoben">
<correction rust-cbindgen "Neue stabile Version der Originalautoren, um Kompilierungen neuerer firefox-esr- und thunderbird-Versionen zu ermöglichen">
<correction s390-dasd "Durchreichen der missbilligten Option -f an dasdfmt abgestellt">
<correction schleuder "Bool-Werte nach Integer migrieren, wenn der ActiveRecord-SQLite3-Verbindungsadapter verwendet wird, sodass die Funktionalität wiederhergestellt wird">
<correction sphinx-bootstrap-theme "Suchfunktion überarbeitet">
<correction spip "Mehrere Probleme mit seitenübergreifendem Skripting behoben">
<correction symfony "CVE-Injektion abgestellt [CVE-2021-41270]">
<correction systemd "Unkontrollierte Rekursion in systemd-tmp-Dateien beseitigt [CVE-2021-3997]; systemd-timesyncd von der Abhängigkeit zur Empfehlung degradiert, um einen Abhängigkeitskreis aufzubrechen; Fehlschlag beim bind-Einhängen eines Verzeichnisses in einen Container via machinectl behoben; Regression in udev entfernt, die zu langen Verzögerungen beim Verarbeiten von Partitionen mit der selben Bezeichnung geführt haben; Regression bei der Verwendung von systemd-networkd in einem unprivilegierten LXD-Container entfernt">
<correction sysvinit "Auswertung von <q>shutdown +0</q> korrigiert; klargestellt, dass das Herunterfahren nicht beendet wird, wenn shutdown mit einer <q>time</q> (Zeit) aufgerufen wird">
<correction tasksel "CUPS für alle *-desktop-Aufgaben einrichten, weil task-print-service nicht mehr existiert">
<correction usb.ids "Enthaltene Daten aktualisiert">
<correction weechat "Problem mit Dienstblockade behoben [CVE-2021-40516]">
<correction wolfssl "Mehrere Problem behoben, die sich auf OCSP-Handhabung [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] und TLS1.3-Unterstützung beziehen [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "SIGILL-Absturz auf nicht-SSE2-CPUs behoben">
<correction xterm "Pufferüberlauf behoben [CVE-2022-24130]">
<correction zziplib "Dienstblockade behoben [CVE-2020-18442]">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für 
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb 
unserer Kontrolle liegen::</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction angular-maven-plugin "Nützt nichts mehr">
<correction minify-maven-plugin "Nützt nichts mehr">
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält, 
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>


<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern 
Freier Software, die ihre Zeit und Mühen einbringen, um das 
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail 
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das 
Stable-Veröffentlichungs-Team (auf Englisch) 
unter &lt;debian-release@lists.debian.org&gt;.</p>


