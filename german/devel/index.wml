#use wml::debian::template title="Debian Entwickler-Ecke" MAINPAGE="true"
#use wml::debian::translation-check translation="9a3deb4236c92e083ea1e494362dc1ff242f6a8c"
# Updated: Holger Wansing <hwansing@mailbox.org>, 2023.
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Die Informationen und alle Links auf 
   diesen Seiten sind, wenn auch öffentlich zu erreichen, vor allem für Debian-Entwickler
   von Interesse.
</aside>

<ul class="toc">
<li><a href="#basic">Grundlagen</a></li>
<li><a href="#packaging">Paketerstellung</a></li>
<li><a href="#workinprogress">Laufende Arbeit</a></li>
<li><a href="#projects">Projekte</a></li>
<li><a href="#miscellaneous">Verschiedenes</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Grundlagen</a></h2>
      <p>Eine Liste der derzeitigen Entwickler und Paketbetreuer, wie Sie dem Projekt beitreten,
         sowie Links zu Entwicklerdatenbank, Debian-Satzung, Abstimmungsinformationen, Veröffentlichungen
         und Architekturen.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debians Organisation</a></dt>
        <dd>Über 1000 Freiwillige sind Teil des Debian-Projekts. Diese Seite beschreibt die Organisationsstruktur,
            listet Teams und deren Mitglieder sowie Kontaktadressen auf.</dd>
        <dt><a href="$(HOME)/intro/people">Die Leute hinter Debian</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Debian-Entwickler (DD)</a> (vollwertige Mitglieder
            des Debian-Projekts) und <a href="https://wiki.debian.org/DebianMaintainer">Debian-Betreuer (DM)</a>
            tragen zum Projekt bei. Schauen Sie in die Liste der
            <a href="https://nm.debian.org/public/people/dd_all/">Debian-Entwickler</a> und der
            <a href="https://nm.debian.org/public/people/dm_all/">Debian-Betreuer</a>, um mehr über die
            beteiligten Personen herauszufinden. Wir haben auch eine
            <a href="developers.loc">Weltkarte der Debian-Entwickler</a>.</dd>
        <dt><a href="join/">Wie Sie Debian beitreten</a></dt>
        <dd>Möchten Sie etwas beitragen und dem Projekt beitreten? Wir suchen immer nach neuen Entwicklern
            und Verfechtern freier Software mit technischen und nicht-technischen Kenntnissen.
            Weitere Informationen finden Sie auf dieser Seite.</dd>
        <dt><a href="https://db.debian.org/">Entwicklerdatenbank</a></dt>
        <dd>Einige Informationen in dieser Datenbank sind für jeden einsehbar, andere nur für Entwickler,
            die sich zuvor eingeloggt haben. Es sind Informationen enthalten wie die
            <a href="https://db.debian.org/machines.cgi">Rechner-Datenbank</a> und GnuPG-Schlüssel der Entwickler.
            Um den Schlüssel eines angezeigten Entwicklers zu extrahieren, klicken Sie auf den Link
            <q>PGP/GPG fingerprint</q>.
            Entwickler können <a href="https://db.debian.org/password.html">ihr Passwort ändern</a> und die
            <a href="https://db.debian.org/forward.html">Mail-Weiterleitung</a> für ihren Debian-Account einrichten.
            Wenn Sie vorhaben, einen der Debian-Rechner zu nutzen, lesen Sie bitte unbedingt die
            <a href="dmup">Benutzungsrichtlinien für Debian-Rechner</a>.</dd>
        <dt><a href="constitution">Die Satzung</a></dt>
        <dd>Dieses Dokument beschreibt die Organisationsstruktur für formale Entscheidungsfindungen innerhalb des
            Projekts.</dd>
        <dt><a href="$(HOME)/vote/">Abstimmungsinformationen</a></dt>
        <dd>Wie wir über unseren Projektleiter oder unsere Logos abstimmen, und die Abstimmungen im Allgemeinen.</dd>
        <dt><a href="$(HOME)/releases/">Veröffentlichungen</a></dt>
        <dd>Diese Seite listet aktuelle Veröffentlichungen (<a href="$(HOME)/releases/stable/">stable</a>,
            <a href="$(HOME)/releases/testing/">testing</a> und <a href="$(HOME)/releases/unstable/">unstable</a>)
            auf sowie enthält einen Index älterer Veröffentlichungen und ihrer Codenamen.</dd>
        <dt><a href="$(HOME)/ports/">Verschiedene Architekturen</a></dt>
        <dd>Debian läuft auf vielen verschiedenen Arten von Computern (<q>Architekturen</q>).
            Diese Seite vereint Informationen über diverse Debian-Portierungen, einige basierend auf
            dem Linux-Kernel, andere auf dem FreeBSD-, NetBSD- oder Hurd-Kernel.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Paketerstellung</a></h2>
      <p>Links zu unserem Policy-Handbuch sowie anderen Dokumenten, die mit der Debian-Policy
         zusammenhängen, zu Vorgängen und anderen Ressourcen für Debian-Entwickler, sowie zum
         Leitfaden für Debian-Betreuer.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debian-Policy-Handbuch</a></dt>
        <dd>Dieses Dokument beschreibt die Richtlinien für die Debian-Distribution.
            Das beinhaltet die Struktur und den Inhalt des Debian-Archivs, einige Planungsdetails des
            Betriebssystems sowie die technischen Voraussetzungen, die jedes Paket erfüllen muss, um
            in die Distribution aufgenommen zu werden.

        <p>Kurz: Sie <strong>müssen</strong> es lesen.</p>
        </dd>
      </dl>

      <p>Es gibt mehrere weitere Dokumente, auf die sich die Debian-Policy bezieht, die Sie vielleicht auch
         interessieren:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />Der FHS definiert die Verzeichnisstruktur und -inhalte (Speicherorte von Dateien);
              die Kompatibilität mit Version 3.0 ist vorgeschrieben (siehe
              <a href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">Kapitel 9</a>
              des Debian-Policy-Handbuchs).</li>
        <li>Liste von <a href="$(DOC)/packaging-manuals/build-essential">build-essential-Paketen</a>
        <br />Es wird erwartet, dass sie diese Pakete installiert haben, wenn Sie Software kompilieren
              oder ein oder mehrere Pakete erstellen möchten. Sie müssen diese nicht in der
              <code>Build-Depends</code>-Zeile eintragen, wenn Sie
              <a href="https://www.debian.org/doc/debian-policy/ch-relationships.html">die Abhängigkeiten</a>
              ihrer Pakete definieren.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Menü-System</a>
        <br />Debians Vorgaben für die Struktur von Menüeinträgen; bitte lesen Sie auch die
        <a href="$(DOC)/packaging-manuals/menu.html/">Dokumentation zum Menü-System</a> selbst.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs-Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java-Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl-Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python-Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf-Spezifikation</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Policy für Datenbank-Anwendungen</a> (Entwurf)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk-Policy</a> (Entwurf)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian-Policy für Ada</a></li>
      </ul>

      <p>Werfen Sie bitte auch einen Blick auf die
         <a href="https://bugs.debian.org/debian-policy">vorgeschlagenen Ergänzungen zur
         Debian-Policy</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Entwicklerreferenz</a></dt>

        <dd>Überblick über empfohlene Vorgehensweisen und zur Verfügung stehende Ressourcen
            für Debian-Entwickler -- sollte auch <strong>unbedingt</strong> gelesen werden.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Leitfaden für Debian-Betreuer</a></dt>

        <dd>Wie Sie ein Debian-Paket erstellen (in allgemein verständlicher Sprache verfasst),
            inklusive vieler Beispiele. Wenn Sie Debian-Entwickler oder Paketbetreuer werden möchten,
            ist dieses Dokument ein guter Anfang.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">Laufende Arbeit: Links für aktive Debian-Entwickler und -Betreuer</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian &lsquo;Testing&rsquo;</a></dt>
  <dd>Automatisch generiert aus der &lsquo;unstable&rsquo;-Distribution;
      hier müssen Sie Ihre Pakete haben, damit sie für die nächste Debian-Veröffentlichung
      infrage kommen.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Veröffentlichungskritische Fehler</a></dt>
  <dd>Eine Liste von Fehlern, die dazu führen können, dass ein Paket aus der 
      <q>Testing</q>-Distribution entfernt wird, oder die in einigen Fällen sogar 
      zu einer Verzögerung der Veröffentlichung der Distribution führen können.
      Fehlerberichte mit einem Schweregrad von <q>serious</q> oder höher werden in
      diese Liste aufgenommen &ndash; stellen Sie daher bitte sicher, dass solche
      Fehler in Ihren Paketen so schnell wie möglich behoben werden.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Debians Fehlerdatenbank (BTS)</a></dt>
    <dd>Zum Melden, Diskutieren und Beheben von Fehlern. Das BTS ist sowohl für
        Benutzer als auch für Entwickler nützlich.
    </dd>
        
  <dt>Informationen über Debian-Pakete</dt>
    <dd>Die Webseiten der <a href="https://qa.debian.org/developer.php">Paketübersicht</a> und des
        <a href="https://tracker.debian.org/">Paket-Trackers</a> versammeln wertvolle
        Informationen für Betreuer. Entwickler, die über bestimmte Pakete immer aktuell
        informiert sein wollen, können (per E-Mail) einen Dienst abonnieren, der ihnen
        Kopien von BTS-Mails sowie Benachrichtigungs-Mails über Uploads und Installationen
        sendet. Weitere Details finden Sie im
        <a href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">Handbuch
        des Paket-Trackers</a>.
    </dd>

    <dt><a href="wnpp/">Pakete, die Hilfe benötigen</a></dt>
      <dd>Hilfe-bedürftige und voraussichtliche Pakete, kurz WNPP (<q>Work-Needing and
          Prospective Packages</q>): eine Liste von Debian-Paketen, die einen neuen
          Verantwortlichen benötigen, und solchen, die neu in Debian aufgenommen werden sollen.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Incoming-System</a></dt>
      <dd>Interne Archiv-Server: neue Pakete werden hierhin hochgeladen. Akzeptierte Pakete
          sind beinahe augenblicklich über einen Webbrowser verfügbar und werden viermal
          am Tag an die <a href="$(HOME)/mirror/">Spiegel-Server</a> verteilt.
          <br />
          <strong>Merke</strong>: Aufgrund der Natur von <q>Incoming</q> raten wir davon
          ab, es zu spiegeln!
      </dd>

    <dt><a href="https://lintian.debian.org/">Lintian-Berichte</a></dt>
      <dd><a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
          ist ein Programm, das überprüft, ob ein Paket der Debian-Policy entspricht.
          Sie sollten es immer benutzen, bevor Sie ein Paket auf die Debian-Server hochladen.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian &lsquo;Experimental&rsquo;</a></dt>
      <dd>Die &lsquo;Experimental&rsquo;-Distribution wird als temporärer Entwicklungsbereich
          für hochgradig experimentelle Software genutzt. Verwenden Sie Pakete aus
          <a href="https://packages.debian.org/experimental/">Experimental</a> nur,
          wenn Sie auch bereits mit &lsquo;unstable&rsquo; umgehen können.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Debian Wiki</a></dt>
      <dd>Das Debian Wiki mit Anweisungen für Entwickler und andere Beitragende.
      </dd>
</dl>

<h2><a id="projects">Projekte: interne Gruppen und Projekte</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Debian-Webseiten</a></li>
<li><a href="https://ftp-master.debian.org/">Debian-Archiv</a></li>
<li><a href="$(DOC)/ddp">Debian-Documentation-Projekt (DDP)</a></li>
<li><a href="https://qa.debian.org/">Debians Quality-Assurance-Team (QA)</a></li>
<li><a href="$(HOME)/CD/">CD/DVD-Images</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Keysigning</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Keysigning - Koordinierung</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Debian-IPv6-Projekt</a></li>
<li><a href="buildd/">Autobuilder-Netzwerk</a> und deren <a href="https://buildd.debian.org/">Build-Logs</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Projekt zur Übersetzung von Paketbeschreibungen (Debian Description Translation Project; DDTP)</a></li>
<li><a href="debian-installer/">Der Debian-Installer</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>

</ul>


<h2><a id="miscellaneous">Verschiedene Links</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://peertube.debian.social/home">Aufnahmen</a> von Vorträgen bei unseren Konferenzen auf PeerTube,
     oder unter Verwendung einer
    <a href="https://debconf-video-team.pages.debian.net/videoplayer/">alternativen Benutzeroberfläche</a></li>
<li><a href="passwordlessssh">Einrichten von SSH</a>, so dass es Sie nicht nach einem Passwort fragt</li>
<li>Wie Sie eine <a href="$(HOME)/MailingLists/HOWTO_start_list">Anfrage für eine neue Mailingliste</a> stellen</li>
<li>Informationen zum <a href="$(HOME)/mirror/">Spiegeln von Debian</a></li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">Diagramm über alle Fehlerberichte</a></li>
<li><a href="https://ftp-master.debian.org/new.html">Neue Pakete</a>, die darauf warten, in Debian aufgenommen zu werden (NEW-Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Neue Debian-Pakete</a> der letzten 7 Tage</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Pakete, die aus Debian entfernt wurden</a></li>
</ul>
