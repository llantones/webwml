# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2010, 2012, 2016, 2017, 2018, 2019.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2009, 2011.
# Gerfried Fuchs <rhonda@debian.at>, 2002, 2003, 2004, 2008.
# Jens Seidel <tux-master@web.de>, 2004, 2006.
# Holger Wansing <linux@wansing-online.de>, 2011 - 2015, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2022-05-23 20:55+0200\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "Delegations-Mail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "Berufungs-Mail"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegiert"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegiert"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>er/ihn"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>sie/ihr"

#: ../../english/intro/organization.data:24
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegiert"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>sie/sie"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "aktuell"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "Mitglied"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "Verwalter"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Release-Manager für Stable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "Berater"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "Vorsitzender"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "Assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "Vertreter"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "Funktion"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"In der folgenden Liste wird <q>aktuell</q> verwendet für Positionen, "
"die vorübergehend besetzt werden (gewählt oder ernannt mit einem "
"bestimmten Ablaufdatum)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Direktoren"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:107
msgid "Distribution"
msgstr "Distribution"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Kommunikation und Outreach"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Datenschutz-Team"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Öffentlichkeitsarbeits-Team"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Mitgliedschaft in anderen Organisationen"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:302
msgid "Support and Infrastructure"
msgstr "Unterstützung und Infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Projektleiter"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Technischer Ausschuss"

#: ../../english/intro/organization.data:102
msgid "Secretary"
msgstr "Schriftführer"

#: ../../english/intro/organization.data:110
msgid "Development Projects"
msgstr "Entwicklungs-Projekte"

#: ../../english/intro/organization.data:111
msgid "FTP Archives"
msgstr "FTP-Archive"

#: ../../english/intro/organization.data:113
msgid "FTP Masters"
msgstr "FTP-Master"

#: ../../english/intro/organization.data:119
msgid "FTP Assistants"
msgstr "FTP-Mitarbeiter"

#: ../../english/intro/organization.data:125
msgid "FTP Wizards"
msgstr "FTP-Berater"

#: ../../english/intro/organization.data:129
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:131
msgid "Backports Team"
msgstr "Backports-Team"

#: ../../english/intro/organization.data:135
msgid "Release Management"
msgstr "Release-Verwaltung"

#: ../../english/intro/organization.data:137
msgid "Release Team"
msgstr "Release-Team"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Qualitätssicherung"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Installationssystem-Team"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Debian-Live-Team"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Veröffentlichungshinweise"

#: ../../english/intro/organization.data:152
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB-Images"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Cloud-Team"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Autobuilding-Infrastruktur"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Wanna-build-Team"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Buildd-Administration"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Liste der Arbeit-bedürfenden und voraussichtlichen Pakete"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Webseiten"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Outreach"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Debian-Women-Projekt"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Community"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Um eine private Nachricht an alle Mitglieder des Community-Teams zu schicken, "
"verwenden Sie den GPG-Schlüssel <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Veranstaltungen"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "DebConf-Ausschuss"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Partner-Programm"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Hardware-Spenden-Koordination"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:292
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:295
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:298
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:305
msgid "Bug Tracking System"
msgstr "Fehlerdatenbank"

#: ../../english/intro/organization.data:310
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Mailinglisten-Verwaltung und -Archive"

#: ../../english/intro/organization.data:318
msgid "New Members Front Desk"
msgstr "Empfang (Front desk) für neue Mitglieder"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Debian-Konten-Verwalter"

#: ../../english/intro/organization.data:333
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Um eine private Nachricht an alle DAMs zu schicken, verwenden Sie den GPG-"
"Schlüssel 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:334
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Schlüsselring-Verwalter (PGP und GPG)"

#: ../../english/intro/organization.data:338
msgid "Security Team"
msgstr "Sicherheitsteam"

#: ../../english/intro/organization.data:350
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:353
msgid "System Administration"
msgstr "System-Administration"

#: ../../english/intro/organization.data:354
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Diese Adresse soll benutzt werden, wenn Probleme mit einer von Debians "
"Maschinen aufgetreten sind, auch wenn es sich um Passwort-Probleme handelt "
"oder wenn neue Pakete installiert werden sollen."

#: ../../english/intro/organization.data:363
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Falls Sie Hardware-Probleme mit Debian-Rechnern bemerken, prüfen Sie bitte "
"die Übersicht der <a href=\"https://db.debian.org/machines.cgi\">Debian-"
"Rechner</a>, sie sollte Administrator-Informationen für jede Maschine "
"enthalten."

#: ../../english/intro/organization.data:364
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP-Entwickler-Verzeichnis-Verwalter"

#: ../../english/intro/organization.data:365
msgid "Mirrors"
msgstr "Spiegel"

#: ../../english/intro/organization.data:368
msgid "DNS Maintainer"
msgstr "DNS-Verwalter"

#: ../../english/intro/organization.data:369
msgid "Package Tracking System"
msgstr "Paketverfolgungs-System"

#: ../../english/intro/organization.data:371
msgid "Treasurer"
msgstr "Schatzmeister"

#: ../../english/intro/organization.data:376
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Anfragen zur Verwendung der <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Handelsmarken</a>"

#: ../../english/intro/organization.data:380
msgid "Salsa administrators"
msgstr "Salsa-Administratoren"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-Administratoren"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (nicht aktiv: wurde nicht mit Squeeze veröffentlicht)"

#~ msgid "Anti-harassment"
#~ msgstr "Gegen Mobbing/Belästigung"

#~ msgid "Auditor"
#~ msgstr "Kassenprüfer"

#~ msgid "Bits from Debian"
#~ msgstr "Neuigkeiten von Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "CD-Distributoren-Seite"

#~ msgid "Consultants Page"
#~ msgstr "Beraterseite"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf-Leitung"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Schlüsselring-Betreuer für Debian-Betreuer (DM)"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends (angepasste Debian-Distributionen)"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian für Astronomie"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian für Kinder von 1 bis 99"

#~ msgid "Debian for education"
#~ msgstr "Debian für Bildung"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian für die medizinische Praxis und Forschung"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian für Behinderte"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian für die Wissenschaft und zugehörige Forschung"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian in Rechtsanwaltskanzleien"

#~ msgid "Embedded systems"
#~ msgstr "Eingebettete Systeme"

#~ msgid "Firewalls"
#~ msgstr "Firewalls"

#~ msgid "Handhelds"
#~ msgstr "Handheldcomputer"

#~ msgid "Individual Packages"
#~ msgstr "Individuelle Pakete"

#~ msgid "Key Signing Coordination"
#~ msgstr "Schlüssel-Signierungs-Koordination"

#~ msgid "Laptops"
#~ msgstr "Laptops"

#~ msgid "Live System Team"
#~ msgstr "Live-System-Team"

#~ msgid "Marketing Team"
#~ msgstr "Marketing-Team"

#~ msgid "Ports"
#~ msgstr "Portierungen"

#~ msgid "Publicity"
#~ msgstr "Öffentlichkeitsarbeit"

#~ msgid "SchoolForge"
#~ msgstr "SchoolForge"

#~ msgid "Security Audit Project"
#~ msgstr "Sicherheits-Audit-Projekt"

#~ msgid "Special Configurations"
#~ msgstr "Spezielle Konfigurationen"

#~ msgid "Testing Security Team"
#~ msgstr "Testing-Sicherheitsteam"

#~ msgid "User support"
#~ msgstr "Benutzer-Unterstützung"

#~ msgid "Vendors"
#~ msgstr "Distributoren"

#~ msgid "Volatile Team"
#~ msgstr "Volatile-Team"

#~ msgid "current Debian Project Leader"
#~ msgstr "derzeitiger Debian-Projektleiter"
