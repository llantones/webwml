#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c"
<define-tag pagetitle>Debians officielle kommunikationskanaler</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>Fra tid til anden bliver Debian spurgt om hvad vores officielle 
kommunikationskanaler er og hvem der ejer websteder med enslydende navne.</p>

<p>Debians primære websted <a href="https://www.debian.org">www.debian.org</a> 
er vores primære kommunikationsmedie.  Dem, der søger oplysninger om vores 
aktuelle begivenheder og udviklingsfremdrift i fællesskabet, kan have interesse 
i afsnittet <a href="https://www.debian.org/News/">Debian-nyheder</a> på Debians 
websted.  Hvad angår mindre formelle annonceringer, har vi den officielle 
Debian-blog <a href="https://bits.debian.org">Bits from Debian</a> og tjenesten 
<a href="https://micronews.debian.org">Debian micronews</a>, til mere 
kortfattede nyheder.</p>

<p>Vores officielle nyhedsbrev <a href="https://www.debian.org/News/weekly/">\
Debians projektnyheder</a> og alle officielle annonceringer af nyheder eller 
projektændringer, offentliggøres både på vores websted og udsendes på vores 
officielle postlister 
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> eller 
<a href="https://lists.debian.org/debian-news/">debian-news</a>.  Der er 
begrænset adgang til at sende indlæg til disse postlister.</p>

<p>Vi ønsker også at benytte lejligheden til at offentliggøre hvordan 
Debian-projektet, eller blot Debian, er struktureret.</p>

<p>Debians struktur reguleres af vores 
<a href="https://www.debian.org/devel/constitution">vedtægter</a>.  
Tillidsposter og delegater er opremset på vores 
<a href="https://wiki.debian.org/Teams">Teams</a>-side.</p>

<p>Den komplette liste over officielle Debian-medlemmer, finder man på vores 
<a href="https://nm.debian.org/members">Nye medlemmer</a>-side, hvor vores 
medlemskab administreres.  En bredere liste over Debian-bidragydere, finder man 
på vores <a href="https://contributors.debian.org">Contributors</a>-side.</p>

<p>Hvis du har spørgsmål, kan du på engelsk kontakte presseholdet på 
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>
