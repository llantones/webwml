#use wml::debian::template title="Sites miroirs de Debian à travers le monde" BARETITLE=true
#use wml::debian::translation-check translation="d83882f2dd9b20c81d0cb83b32c028d9765578c7" maintainer="Jean-Paul Guillonneau"

#premier traducteur : Nicolas Bertolissio
#traducteurs : voir journal

<p>
Voici une liste <strong>complète</strong> de miroirs de Debian. Pour chaque
site, les différents types de contenus disponibles sont listés, ainsi que les
méthodes d'accès pour chaque type.
</p>

<p>
Les contenus suivants disposent de miroirs&nbsp;:
</p>

<dl>
  <dt><strong>Packages</strong> (paquets)</dt>
    <dd>L'ensemble des paquets de Debian.</dd>
  <dt><strong>CD Images</strong> (images de CD)</dt>
    <dd>Les images de CD officielles de Debian. Veuillez vous reporter à <url
    "https://www.debian.org/CD/" /> pour de plus amples détails.</dd>
  <dt><strong>Old releases</strong> (anciennes versions)</dt>
    <dd>Les archives des anciennes versions de Debian.
    <br />
    Certaines des anciennes versions incluent l'archive de Debian appelée
    non-US, avec des sections pour les paquets Debian qui ne pouvaient pas être
    distribués aux États-Unis à cause de brevets logiciels ou de l'utilisation
    du chiffrement. Les mises à jour de l'archive non-US de Debian ont été
    abandonnées à partir de la version&nbsp;3.1 de Debian.</dd>

</dl>

<p>
Les méthodes d'accès suivantes sont possibles&nbsp;:
</p>

<dl>
  <dt><strong>HTTP</strong></dt>
    <dd>L'accès standard par la Toile, mais il peut être utilisé pour télécharger
    des fichiers.</dd>
  <dt><strong>rsync</strong></dt>
    <dd>Un moyen efficace de faire un miroir.</dd>
</dl>

<p>Une copie officielle de la liste suivante est toujours disponible sur&nbsp;:
<url "https://www.debian.org/mirror/list-full">.
<br />
Tout ce que vous souhaitez savoir d'autre sur les miroirs Debian se trouve
sur&nbsp;: <url "https://www.debian.org/mirror/" />.
</p>

<hr style="height:1" />

<p>
Passer directement à un pays de la liste&nbsp;:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"
