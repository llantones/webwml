#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans memcached, un système
de gestion d'objets en mémoire de haute performance. Un attaquant distant
pourrait tirer avantage de ces défauts pour provoquer un déni de service
(plantage du démon), ou éventuellement pour exécuter du code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7291">CVE-2013-7291</a>

<p>memcached, lors d'une exécution en mode bavard, peut être planté en
envoyant des requêtes soigneusement contrefaites qui déclenchent un
affichage de clés illimité, avec pour conséquence un plantage du démon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8704">CVE-2016-8704</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8705">CVE-2016-8705</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8706">CVE-2016-8706</a>

<p>Aleksandar Nikolic de Cisco Talos a découvert plusieurs vulnérabilités
dans memcached. Un attaquant distant pourrait provoquer un dépassement
d'entier en envoyant des requêtes soigneusement contrefaites au serveur
memcached, avec pour conséquence un plantage du démon.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.4.13-0.2+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets memcached.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-701.data"
# $Id: $
