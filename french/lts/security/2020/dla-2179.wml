#use wml::debian::translation-check translation="1955ca40a0394dbd1d43a5c9eec63b467b874b51" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés à l’encontre du paquet source
jackson-databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10968">CVE-2020-10968</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
org.aoju.bus.proxy.provider.remoting.RmiProvider (c'est-à-dire, bus-proxy).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10969">CVE-2020-10969</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
javax.swing.JEditorPane.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11111">CVE-2020-11111</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l'interaction entre la séquence de sérialisation et la saisie concernant
org.apache.activemq.* (c'est-à-dire, activemq-jms, activemq-core, activemq-pool
et activemq-pool-jms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11112">CVE-2020-11112</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
org.apache.commons.proxy.provider.remoting.RmiProvider (c'est-à-dire,
apache/commons-proxy).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11113">CVE-2020-11113</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
org.apache.openjpa.ee.WASRegistryManagedRuntime (c'est-à-dire, openjpa).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11619">CVE-2020-11619</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
org.springframework.aop.config.MethodLocatingFactoryBean (c'est-à-dire,
spring-aop).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11620">CVE-2020-11620</a>

<p>FasterXML jackson-databind 2.x avant la version 2.9.10.4 gère incorrectement
l’interaction entre la séquence de sérialisation et la saisie concernant
org.apache.commons.jelly.impl.Embedded (c'est-à-dire, commons-jelly).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.2-2+deb8u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2179.data"
# $Id: $
