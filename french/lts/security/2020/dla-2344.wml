#use wml::debian::translation-check translation="a4f934cd9df38a915753b05d161e2aa1fbc57893" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de déni de service a été découverte dans mongodb, une base
de données orientée documents/objets. Un utilisateur autorisé pourrait 
présenter des requêtes spécialement contrefaites qui enfreindraient un invariant 
dans la prise en charge des requêtes de sous-système pour geoNear.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:3.2.11-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mongodb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mongodb, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mongodb">https://security-tracker.debian.org/tracker/mongodb</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2344.data"
# $Id: $
