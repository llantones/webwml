#use wml::debian::translation-check translation="ef47a64a9f7e1cf56ca4aa5eda63abfe62209f37" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L’API ACME v1 de Let's Encrypt est devenue obsolète et a commencé son
processus de disparition. En commençant par des coupures en janvier 2021 et en
finissant par un arrêt complet en juin 2021, les API de Let's Encrypt ne seront
plus disponibles. Pour éviter aux utilisateurs des perturbations dans le
renouvellement de leurs certificats, cette mise à jour rétroporte le basculement
vers l’API ACME v2.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.28.0-1~deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-certbot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-certbot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-certbot">https://security-tracker.debian.org/tracker/python-certbot</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2484.data"
# $Id: $
