#use wml::debian::translation-check translation="804f608dc176dede73a214b5e75c5761320898be" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été découverts dans libjettison-java, une collection
d’analyseurs et d’éditeurs StAX pour JSON. Une entrée utilisateur contrefaite
pour l'occasion pouvait causer un déni de service à l’aide d’un épuisement de
mémoire ou des erreurs de dépassement de pile.</p>

<p>De plus, un échec de construction relatif à la mise à jour a été corrigé
dans jersey1.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.5.3-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjettison-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libjettison-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libjettison-java">\
https://security-tracker.debian.org/tracker/libjettison-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3259.data"
# $Id: $
