#use wml::debian::translation-check translation="7d93f19134dd7c74d9a2ea20f6de94ae4b135765" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Trois vulnérabilités ont été corrigées qui pouvaient, dans de rares
circonstances, mener à des vulnérabilités de déni de service exploitables à
distance dans du logiciel utilisant exiv2 pour l’extraction de métadonnées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11683">CVE-2017-11683</a>

<p>Plantage dû à une assertion accessible dans une entrée contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19716">CVE-2020-19716</a>

<p>Dépassement de tampon lors du traitement de métadonnées contrefaites d’images
CRW.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 0.25-4+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exiv2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/exiv2">\
https://security-tracker.debian.org/tracker/exiv2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3186.data"
# $Id: $
