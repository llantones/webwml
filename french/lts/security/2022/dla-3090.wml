#use wml::debian::translation-check translation="5dbe226f9a56d7b88b1caa727bb5604c3ad26196" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité de désérialisation d'objet arbitraire dans
php-horde-turba, un composant de carnet d'adresses pour la suite d'outils
de travail en groupe Horde.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30287">CVE-2022-30287</a>

<p><q>Horde Groupware Webmail Edition</q> jusqu'à sa version 5.2.22 permet
une attaque d'injection par réflexion grâce à laquelle un attaquant peut
instancier une classe de pilote. Cela mène alors à la désérialisation
arbitraire d'objets PHP.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 4.2.23-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-horde-turba.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3090.data"
# $Id: $
