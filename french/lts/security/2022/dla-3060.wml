#use wml::debian::translation-check translation="74ae34490fcbc40456bdee2a00c8f60d47a00307" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans blender, un modeleur et
moteur de rendu 3D très rapide et polyvalent.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0546">CVE-2022-0546</a>

<p>Un accès au tas hors limites dû à l'absence de vérification dans le
chargeur d'image pouvait avoir pour conséquences un déni de service, une
corruption de mémoire ou éventuellement l'exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0545">CVE-2022-0545</a>

<p>Un dépassement d'entier lors du traitement d'images 2D pouvait avoir
pour conséquences une vulnérabilité d'écriture d'une valeur arbitraire à un
emplacement arbitraire ou une vulnérabilité de lecture hors limites qui
pouvait divulguer des informations sensibles ou réaliser une exécution de
code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0544">CVE-2022-0544</a>

<p>Des fichiers image DDS contrefaits pouvaient générer un dépassement
d'entier par le bas dans le chargeur DDS qui menait à une lecture hors
limites et pouvait divulguer des informations sensibles.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.79.b+dfsg0-1~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets blender.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de blender, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/blender">\
https://security-tracker.debian.org/tracker/blender</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3060.data"
# $Id: $
