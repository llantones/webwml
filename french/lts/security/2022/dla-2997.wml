#use wml::debian::translation-check translation="ec4d6deb822417698117d625c8d965ad32000347" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans ecdsautils, une collection d'outils de chiffrement sur courbes
elliptiques ECDSA en ligne de commande, une vérification incorrecte de
signatures de chiffrement a été détectée. Une signature composée de zéros
uniquement est toujours considérée comme valable, simplifiant la
contrefaçon de signatures.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24884">CVE-2022-24884</a></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.3.2+git20151018-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ecdsautils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ecdsautils, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ecdsautils">\
https://security-tracker.debian.org/tracker/ecdsautils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2997.data"
# $Id: $
