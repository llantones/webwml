#use wml::debian::translation-check translation="ecaea2b5314ab8a949036d1837008f036037fba5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque Xalan Java XSLT d'Apache est vulnérable à un problème de
troncature d'entier lors du traitement de feuilles de style XSLT
malveillantes. Cela peut être utilisé pour corrompre des fichiers de classe
Java générés par le compilateur XSLT interne et exécuter du bytecode Java.
Dans Debian, le code vulnérable est dans le paquet source bcel.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
6.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bcel.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bcel, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bcel">\
https://security-tracker.debian.org/tracker/bcel</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3155.data"
# $Id: $
