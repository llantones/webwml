#use wml::debian::translation-check translation="3ce9aebe3d36cec8321b25285b8cee430910f10e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Timothee Desurmont a découvert une vulnérabilité de fuite d'informations
dans node-eventsource, un client EventSource conforme aux spécifications du W3C
pour Node.js : le module n’honorait pas la politique de même origine et lors
d’un suivi de redirection pouvait faire fuiter les cookies de l’URL cible.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 0.2.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-eventsource.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-eventsource,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-eventsource">\
https://security-tracker.debian.org/tracker/node-eventsource</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3235.data"
# $Id: $
