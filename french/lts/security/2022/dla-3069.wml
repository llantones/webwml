#use wml::debian::translation-check translation="c15dfd43d7cde67df2a0ea3bc783746eeac62a04" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans les greffons pour
l'environnement multimédia GStreamer. Cela peut avoir pour conséquences un
déni de service ou éventuellement l'exécution de code arbitraire lors de
l'ouverture d'un fichier média mal formé.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 1.14.4-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-good1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gst-plugins-good1.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gst-plugins-good1.0">\
https://security-tracker.debian.org/tracker/gst-plugins-good1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3069.data"
# $Id: $
