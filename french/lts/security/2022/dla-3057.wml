#use wml::debian::translation-check translation="b92c6c2182dc526c8d255ba089065ed49bad1dd2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un problème dans request-tracker4, un système extensible de
gestion de tickets et de problèmes. Des informations sensibles pouvaient
être révélées au moyen d'une attaque temporelle sur le système
d'authentification.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38562">CVE-2021-38562</a>

<p>Les versions 4.2 avant 4.2.17, 4.4 avant 4.4.5 et 5.0 avant 5.0.2 de
Request Tracker (RT) de Best Practical permettent la divulgation
d'informations à l'aide d'une attaque temporelle à l'encontre de
lib/RT/REST2/Middleware/Auth.pm.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.4.1-3+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets request-tracker4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3057.data"
# $Id: $
