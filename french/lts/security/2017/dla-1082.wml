#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13776">CVE-2017-13776</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13777">CVE-2017-13777</a>

<p>Problème de déni de service dans ReadXBMImage().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12935">CVE-2017-12935</a>

<p>La fonction ReadMNGImage dans coders/png.c gèrait incorrectement de grandes
images MNG, conduisant à une lecture de mémoire non valable dans la fonction
SetImageColorCallBack dans magick/image.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12936">CVE-2017-12936</a>

<p>La fonction ReadWMFImage dans coders/wmf.c avait un problème d’utilisation de
mémoire après libération pour les données associées avec le rapport d’exception.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12937">CVE-2017-12937</a>

<p>La fonction ReadSUNImage dans coders/sun.c avait une lecture hors limites de
tampon basé sur le tas de colormap.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13063">CVE-2017-13063</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13064">CVE-2017-13064</a>

<p>Vulnérabilité de dépassement de tampon basé sur le tas dans la fonction
GetStyleTokens dans coders/svg.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13065">CVE-2017-13065</a>

<p>Vulnérabilité de déréférencement de pointeur NULL dans la fonction
SVGStartElement dans coders/svg.c</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1082.data"
# $Id: $
