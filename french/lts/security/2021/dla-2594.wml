#use wml::debian::translation-check translation="35410c484742da27b9d6436dfc8ebda66e54fe80" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Trois problèmes de sécurité ont été détectés dans tomcat8.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-24122">CVE-2021-24122</a>

<p>Lors du service de ressources à partir d’un emplacement réseau utilisant le
système de fichiers NTFS, Apache Tomcat, des versions 8.5.0 à 8.5.59, est
susceptible de divulguer du code source JSP dans certaines configurations. La
cause principale est le comportement inattendu de File.getCanonicalPath() de
l’API JRE, qui à son tour est provoqué par le comportement incohérent de l’API
Windows (FindFirstFileW) dans certaines circonstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25122">CVE-2021-25122</a>

<p>Lors de la réponse à de nouvelles requêtes de connexion h2c, Apache Tomcat
pourrait dupliquer des en-têtes de requête et une partie limitée du corps d’une
requête à une autre, signifiant qu’un utilisateur A et un utilisateur B
pourraient tous deux voir le résultat de la requête de A.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25329">CVE-2021-25329</a>

<p>Le correctif pour 2020-9484 était incomplet. Lors de l’utilisation d’Apache
Tomcat, versions 8.5.0 à 8.5.61, avec un cas extrême de configuration hautement
improbable, l’instance de Tomcat était encore vulnérable au
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9494">CVE-2020-9494</a>.
Remarquez que les prérequis précédemment publiés pour
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>
et les mitigations précédemment publiées pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>
s’appliquent à ce problème.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 8.5.54-0+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat8">\
https://security-tracker.debian.org/tracker/tomcat8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2594.data"
# $Id: $
