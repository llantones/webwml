#use wml::debian::translation-check translation="999c71c6d7ff246653a7f1ce91ea018e1943e80f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans ruby2.3, un interpréteur
de script du langage Ruby orienté objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31799">CVE-2021-31799</a>

<p>Dans RDoc 3.11 jusqu’à la version 6.x avant la version 6.3.1, comme fourni
avec Ruby jusqu’à la version 2.3.3, il est possible d’exécuter du code
arbitraire à l’aide de « | » et de balises dans un nom de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31810">CVE-2021-31810</a>

<p>Un problème a été découvert dans Ruby jusqu’à la version 2.3.3. Un serveur
FTP malveillant peut utiliser la réponse PASV pour amener Net::FTP à se
reconnecter à une adresse IP et un port donnés. Potentiellement, cela conduit
curl à extraire des informations sur des services qui sont privées et non
sujettes à divulgation (par exemple, un attaquant peut réaliser un balayage de
ports et des extractions de bannière de service).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32066">CVE-2021-32066</a>

<p>Un problème a été découvert dans Ruby jusqu’à la version 2.3.3. Net::IMAP
ne déclenche pas d’exception quand StartTLS échoue lors d’une réponse inconnue.
Cela pourrait permettre à des attaquants de type « homme du milieu » de
contourner les protections TLS en exploitant une position de réseau entre le
client et le registre pour bloquer la commande StartTLS, c’est-à-dire une
« StartTLS stripping attack ».</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 2.3.3-1+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby2.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby2.3">\
https://security-tracker.debian.org/tracker/ruby2.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2780.data"
# $Id: $
