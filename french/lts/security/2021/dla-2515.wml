#use wml::debian::translation-check translation="eb05a3fc3ac3bddbb24f6738f040883d4b4ecf55" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert que csync2, un outil pour la synchronisation de
l’informatique dématérialisée, ne vérifiait pas correctement la valeur renvoyée
par les routines de sécurité GnuTLS. Il omettait d’appeler de façon répétitive
cette fonction comme prévu dans la conception de cette API.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15523">CVE-2019-15523</a>

<p>Un problème a été découvert dans LINBIT csync2 jusqu’à la version 2.0. Il ne
vérifiait pas correctement la valeur renvoyée GNUTLS_E_WARNING_ALERT_RECEIVED de
la fonction gnutls_handshake(). Il omettait de rappeler cette fonction, comme
prévu dans la conception de cette API.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0-8-g175a01c-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets csync2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2515.data"
# $Id: $
