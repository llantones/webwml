#use wml::debian::translation-check translation="9fa238e1e692e0e02c33ff312ffa3fcf144996f9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur HTTP Apache.
Un attaquant pourrait envoyer des requêtes à l’aide d’un mandataire à un
serveur arbitraire, corrompre la mémoire dans quelques configurations
impliquant des modules tiers et causer un plantage du serveur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34798">CVE-2021-34798</a>

<p>Des requêtes mal formées pourraient faire que le serveur déréférence un
pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39275">CVE-2021-39275</a>

<p>ap_escape_quotes() peut écrire au-delà de la fin d’un tampon lorsqu’une
entrée malveillante est fournie. Aucun des modules inclus ne peut passer de
données non fiables, mais des modules tiers ou externes le peuvent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40438">CVE-2021-40438</a>

<p>Une requête contrefaite de chemin d’URI peut faire que mod_proxy
retransmette la requête à un serveur d’origine choisi par l’utilisateur
distant.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 2.4.25-3+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2776.data"
# $Id: $
