#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un second problème de régression a été résolu dans poppler, la bibliothèque
partagée de rendu de PDF, cette fois introduit avec la version 0.26.5-2+deb8u6
(consulter DLA 1562-2).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

<p>Dans Poppler 0.68.0, la fonction Parser::getObj() dans Parser.cc peut
provoquer une récursion infinie à l'aide d'un fichier contrefait. Un attaquant
distant peut exploiter cela pour une attaque par déni de service.</p>

<p>Les développeurs amont de poppler ont ajouté en plus deux corrections de
régression au-dessus du correctif original (requête de fusion n° 91 de l’amont).
Ces deux correctifs sont maintenant ajoutés dans le paquet poppler dans Debian
Jessie LTS.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 0.26.5-2+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562-3.data"
# $Id: $
