#use wml::debian::translation-check translation="700b40c8d1dd06f164092015c75f811b6c9c2c6d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>HTML::StripScripts, un module pour supprimer des scripts d’HTML, permettait
un ReDoS _hss_attval_style à cause d’un retour sur trace catastrophique pour du
contenu HTML avec certains attributs de style.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.06-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libhtml-stripscripts-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libhtml-stripscripts-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libhtml-stripscripts-perl">\
https://security-tracker.debian.org/tracker/libhtml-stripscripts-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3296.data"
# $Id: $
