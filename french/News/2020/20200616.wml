#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Ampere donne des serveurs Arm64 à Debian pour renforcer l'écosystème Arm</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

<p>
<a href="https://amperecomputing.com/">Ampere®</a> a établi un partenariat avec
Debian pour renforcer notre infrastructure matérielle par le don de trois
serveurs Arm64 de haute performance d'Ampere. Ces serveurs ThinkSystem HR330A
de Lenovo renferment un processeur eMAG d'Ampere avec un processeur Arm®v8
64 bits spécialement conçu pour les serveurs d’informatique dématérialisée, et
sont munis de 256 Go de RAM, deux disques SSD de 960 Go et une carte réseau
à deux ports 25GbE.
</p>

<p>
Les serveurs donnés ont été déployés à l'Université de Colombie-Britannique,
notre partenaire d'hébergement à Vancouver, Canada. Les administrateurs système
de Debian (DSA) les ont configurés pour qu'ils exécutent les démons de
construction arm64, armhf et armel, remplaçant ceux en cours de fonctionnement
sur des cartes de développement moins puissantes. Sur des machines virtuelles
avec moitié moins de processeurs virtuels, le résultat est que le temps de
construction de paquets Arm* a été réduit de moitié avec le système eMAG
d'Ampere. Un autre bénéfice de ce don généreux est qu'il permet aux DSA de
faire migrer certains services généraux de Debian exploités sur notre
infrastructure actuelle et qu'il fournira des machines virtuelles pour d'autres
équipes Debian (par exemple celles d'intégration continue ou d'assurance
qualité, etc.) qui ont besoin d'accéder à l'architecture Arm64.
</p>

<p>
<q>Notre partenariat avec Debian renforce notre stratégie de développement pour
élargir la communauté de l'informatique libre qui utilise les serveurs Ampere
pour développer encore plus l'écosystème Arm64 et permettre la création de
nouvelles applications,</q> a déclaré Mauri Whalen, vice-président du
développement logiciel chez Ampere. <q>Debian est une communauté bien gérée et
respectée et nous sommes fiers de travailler avec elle.</q>
</p>

<p>
<q>Les administrateurs système de Debian sont reconnaissants à Ampere pour son
don de serveurs Arm64 extrêmement fiables avec des interfaces de gestion
standard intégrées comme l'interface de gestion intelligente de matériel, (ou
IPMI, Intelligent Platform Management Interface), et avec les garanties
matérielles de Lenovo et l'organisation d’assistance  qui l'accompagne, est
précisément ce que les DSA souhaitaient pour l'architecture Arm64. Ces serveurs
sont très puissants et très bien équipés : nous prévoyons de les utiliser pour
des services généraux en plus des démons de construction Arm64. Je pense qu'ils
se révéleront très attrayants pour les opérateurs d’informatique dématérialisée
et je suis ravi du partenariat d'Ampere Computing avec Debian.</q> – Luca
Filipozzi, administrateur système Debian.
</p>

<p>
C'est seulement grâce au don de travail bénévole, d'équipement et de services
en nature et à des soutiens financiers que Debian peut répondre à l'engagement
d'un système d'exploitation libre. Nous sommes très reconnaissants envers Ampere
pour sa générosité.
</p>

<h2>À propos d'Ampere Computing</h2>
<p>
Ampere élabore le futur de l'informatique dématérialisée à très grande échelle
et en périphérie de réseau avec le premier processeur au monde conçu pour
l’infonuagique. Élaboré pour l’infonuagique avec une architecture de serveur
Arm moderne 64 bits, Ampere offre à ses clients la liberté d'accélérer
l'exécution de toutes les applications de l'informatique dématérialisée. Avec
des performances, une efficacité énergétique et une adaptabilité parmi les
meilleures de l'industrie, les processeurs Ampere sont taillés sur mesure pour
la croissance continue de l'informatique dématérialisée et en périphérie de
réseau.
</p>

<h2>À propos de Debian</h2>
<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et prenant en charge un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le <q>système d'exploitation
universel</q>.
</p>


<h2>Plus d'informations</h2>

<p>
Pour plus d'informations, veuillez consulter les pages internet de
Debian à l'adresse
<a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un message
à &lt;press@debian.org&gt;.
</p>
