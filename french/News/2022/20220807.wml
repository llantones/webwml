#use wml::debian::translation-check translation="7dcdd3e690d3084ffd92cb099c4321e35edccfc1" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Propriété du domaine <q>debian.community</q></define-tag>
<define-tag release_date>2022-08-07</define-tag>
#use wml::debian::news

<p>
L'Organisation mondiale de la propriété intellectuelle (OMPI), en suivant les
principes directeurs de règlement des litiges relatifs aux noms de domaine
(Uniform Domain-Name Dispute-Resolution Policy – UDRP), a décidé que la
propriété du domaine <q><a href="https://debian.community">debian.community</a></q>
devait être
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2022-1524">\
transférée au Projet Debian</a>.
</p>

<p>
Le comité nommé a constaté que <q>le nom de domaine litigieux était identique
à une marque déposée sur laquelle le Requérant possède des droits.</q>
</p>

<p>
Dans sa décision, le comité observe que :
<p>

<blockquote>
[...] le nom de domaine litigieux est identique à la marque DEBIAN, ce qui
comporte un risque élevé d'affiliation implicite au Requérant. [...] Étant donné
que le Requérant décrit Debian de manière bien évidente sur sa page d'accueil
comme une « communauté » et pas seulement comme un système d'exploitation, le
suffixe [.community] renforce réellement l'idée que la résolution du nom du
domaine en litige mènera vers un site opéré ou soutenu par [Debian]. Le nom de
domaine en litige ne contient aucune critique ni d'autres termes pour dissiper
ou nuancer cette fausse impression.
</blockquote>

<p>
Le comité a fait observer que :
</p>

<blockquote>
Les preuves soumises par le Requérant montrent que certains messages présentent
la marque DEBIAN associée à un culte du sexe tristement célèbre, à des
délinquants sexuels notoires et à l'asservissement de femmes, et un message
affiche des photographies de marques physiques censément situées sur la peau des
partes génitales des victimes. L'association d'informations sur le Requérant à
ce type d'informations est intentionnelle et l'ampleur de ce type d'informations
n'est pas seulement fortuite sur le site web. De l'avis du Comité, ces messages
visent délibérément à une association trompeuse entre la marque DEBIAN et ces
phénomènes offensants, et donc salissent la marque.
</blockquote>

<p>
et conclut plus loin que :
</p>

<blockquote>
rien dans le Contrat Social de Debian ou ailleurs n'indique que le Requérant ait
jamais consenti à ce type d'association fallacieuse avec sa marque publiée par
le défendeur sur son site web. Le Défendeur signale que la marque DEBIAN est
enregistrée seulement en matière de logiciel. Néanmoins, même si les messages
concernés s'attaquent aux membres du Requérant qui mettent à disposition le
logiciel DEBIAN, plutôt qu'au logiciel lui-même, ces messages utilisent la
marque en association avec le nom de domaine en litige d'une manière qui cherche
intentionnellement à créer des associations mensongères avec la marque
elle-même.
</blockquote>

<p>
Debian s'est engagée à un usage correct de ses marques déposées soumises à la 
<a href="$(HOME)/trademark">Charte de marque déposée</a> et continuera à prendre
des mesures coercitives quand cette charte est violée.
</p>

<p>
Le contenu de <q>debian.community</q> a désormais été remplacé par une 
<a href="$(HOME)/legal/debian-community-site">page</a>
expliquant la situation et répondant à des questions complémentaires.
</p>

<p>
Le texte complet de la décision de l'OMPI est disponible en ligne ici :
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273">https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273</a>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contact Information</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt;.</p>
