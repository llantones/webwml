#use wml::debian::template title="Como usar o mecanismo de busca do Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>
O projeto Debian oferece seu próprio mecanismo de busca em
<a href = "https://search.debian.org/">https://search.debian.org/</a>.
Aqui estão algumas dicas sobre como usá-lo e iniciar pesquisas simples, bem
como pesquisas mais complexas com operadores booleanos.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Pesquisa simples</h3>
      <p>A maneira mais simples de usar o mecanismo é inserir uma única palavra
no campo de pesquisa e pressionar [Enter]. Como alternativa, você pode clicar
no botão <em>Pesquisar</em>. O mecanismo de pesquisa retornará todas as páginas
do nosso site que contenham essa palavra. Para a maioria das pesquisas,
isso deve fornecer bons resultados. </p> 
      <p>Como alternativa, você pode pesquisar por mais de uma palavra.
Novamente, você deve ver todas as páginas do site do Debian que contêm todas as
palavras que você digitou. Para pesquisar frases, coloque-as entre aspas (").
Por favor observe que o mecanismo de pesquisa não diferencia maiúsculas de
minúsculas, portanto, pesquisar por <code>gcc</code> corresponde a "gcc" e
também a "GCC".</p>
       <p>Abaixo do campo de pesquisa, você pode decidir quantos resultados por
página deseja ver. Também é possível escolher um idioma diferente; a pesquisa do
site Debian suporta quase 40 idiomas diferentes.</p> 
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Pesquisa booleana</h3>

<p>Se uma pesquisa simples não for suficiente, você pode usar operadores de
pesquisa booleanos. Você pode escolher entre <em>AND</em>, <em>OR</em> e
<em>NOT</em> ou combinar os três. Por favor certifique-se de usar letras
maiúsculas para todos os operadores, para que o mecanismo de pesquisa os
reconheça.</p> 

      <ul>
        <li><b>AND</b> combina duas expressões e retorna páginas que contêm
ambas as palavras. Por exemplo, <code>gcc AND patch</code> encontra todas as
páginas que contêm "gcc" e "patch". Neste caso, você obterá os mesmos resultados
da pesquisa por <code>gcc patch</code>, mas um <code>AND</code> explícito pode
ser útil em combinação com outros operadores.</li>
        <li><b>OR</b> retorna resultados se alguma das palavras estiver na
página. <code>gcc OR patch</code> encontra qualquer página que contenha "gcc"
ou "patch".</li>
        <li><b>NOT</b> é usado para excluir termos de pesquisa dos resultados.
Por exemplo, <code>gcc NOT patch</code> encontra todos os que contêm "gcc",
mas não "patch". <code>gcc AND NOT patch</code> fornece os mesmos resultados,
mas a pesquisa por <code>NOT patch</code> não é suportada.</li>
        <li><b>(...)</b> pode ser usado para agrupar expressões. Por exemplo,
<code>(gcc OR make) NOT patch</code> encontrará todas as páginas que contêm
"gcc" ou "make", mas não contêm "patch".</li>
      </ul>
    </div>
  </div>
</div>
