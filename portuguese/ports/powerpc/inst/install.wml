#use wml::debian::template title="Porte para PowerPC -- Instalação " NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="70cf45edbaeb4b8fc8f99d683f2f5c5c4435be92"

<h2> Instalação do Debian GNU/Linux em máquinas PowerPC </h2>
<p>
 Verifique as páginas seguintes para informações específicas sobre
 a instalação do Debian/PowerPC em seu sistema:
</p>
<ul>
 <li> <a href="apus">Sistema PowerUP Amiga</a> </li>
 <li> <a href="chrp">CHRP</a> </li>
 <li> <a href="prep">PReP</a> </li>
 <li> <a href="pmac">PowerMac</a> </li>
</ul>
     <p>
Existem quatro grandes tipos de <em>powerpc</em> suportados:
máquinas PMac (Power-Macintosh), Apus, CHRP e PReP. Portes para outras
arquiteturas <em>powerpc</em>, como as arquiteturas Be-Box e MBX,
estão a caminho, mas ainda não são suportadas pelo Debian. Nós poderemos
ter um porte 64bit no futuro.</p>

<p>
Também existem quatro tipos de kernel powerpc no Debian. Eles são
baseados em tipos de CPU específicos (e não devem ser confundidos
com os tipos de arquitetura discutidas acima):</p>

<div><dl>

<dt>powerpc</dt>

<dd><p>
A maior parte dos sistemas utilizam este tipo de kernel, que suporta os
processadores PowerPC 601, 603, 604, 740, 750, e 7400. Todos os sistemas
Power Macintosh da Apple até, e incluindo, o G4, usam processadores
suportados por este kernel.</p></dd>

<dt>power3</dt>

<dd><p>
O processador POWER3 é usado em sistemas de servidor IBM 64-bit mais antigos:
modelos conhecidos incluem o IntelliStation POWER Modelo 265, o pSeries 610 e
640, e o RS/6000 7044-170, 7043-260 e 7044-270.</p></dd>

<dt>power4</dt>

<dd><p>
O processador POWER4 é usado em sistemas de servidor IBM 64-bit mais recentes:
modelos conhecidos incluem os pSeries 615, 630, 650, 655, 670 e 690.
O Apple G5 também é baseado na arquitetura POWER4 e usa este tipo de
kernel.</p></dd>

<dt>apus</dt>

<dd><p>
Este tipo de kernel suporta o sistema Power-UP Amiga.
</p></dd>

</dl></div>

     <p>
A Apple (e, por um breve período, alguns outros e poucos fabricantes - Power
Computing, por exemplo) faz uma série de computadores Macintosh baseados
no processador PowerPC. Para os propósitos de suporte de arquitetura,
eles são categorizados como Nubus, OldWorld PCI e NewWorld.
     <p>
Os sistemas Nubus atualmente não são suportados pelo debian/powerpc. A
arquitetura de kernel monolítica Linux/PPC não tem suporte para
essas máquinas; ao contrário, deve-se usar o microkernel MkLinux Mach,
que o Debian ainda não suporta. Eles incluem a linha 6100/7100/8100
dos Power Macintoshes.
     <p>
Os sistemas OldWorld são, em sua maioria, Power Macintoshes com um drive
de disquete e um bus PCI. A maior parte dos 603, 603e, 604 e 604e baseados
em Power Macintoshes são máquinas OldWorld. Os sistemas G3 de cor bege
também são OldWorld.
     <p>
Os assim chamados NewWorld PowerMacs são qualquer PowerMacs em gabinetes
plásticos coloridos e translúcidos, todos os sistemas iMacs, iBooks, G4
e G5. Os NewWorld PowerMacs também são conhecidos por usarem o sistema
"ROM in RAM" para Mac OS, e foram manufaturados de meados de 1998 em
diante.

<p>Aqui está uma lista de máquinas powerpc que devem funcionar com o Debian.</p>

<table class="reltable">
<colgroup span="2">
<tr>
  <th><strong>Modelo nome/número</strong></th>
  <th><strong>Arquitetura</strong></th>
</tr>
<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Apple</strong></td>
  <td></td>
</tr>
				
<tr class="even"><td>iMac Bondi Blue, 5 tipos, Slot Loading</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iMac Summer 2000, início de 2001</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iMac G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook, iBook SE, iBook Dual USB</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook2</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook G4</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh Blue and White (B&amp;W) G3</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 PCI, AGP, Cube</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 Gigabit Ethernet</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 Digital Audio, Quicksilver</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 FireWire Pismo (2000)</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 Lombard (1999)</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G4 Titanium</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G4 Aluminum</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Mac mini</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Xserve G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td colspan="2"></td></tr>

<tr class="even"><td>Performa 4400, 54xx, 5500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Performa 6300, 6360, 6400, 6500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 4400, 5400</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 7200, 7300, 7500, 7600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 8200, 8500, 8600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 9500, 9600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh (Beige) G3 Minitower</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh (Beige) Desktop, All-in-One</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerBook 2400, 3400, 3500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 Wallstreet (1998)</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Twentieth Anniversary Macintosh</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Workgroup Server 7250, 8550, 9650, G3</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Power Computing</strong></td>
  <td></td>
</tr>

<tr class="even"><td>PowerBase, PowerTower / Pro, PowerWave</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerCenter / Pro, PowerCurve</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>UMAX</strong></td>
  <td></td>
</tr>

<tr class="even"><td>C500, C600, J700, S900</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>APS</strong></td>
  <td></td>
</tr>

<tr class="even"><td>APS Tech M*Power 604e/2000</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Motorola</strong></td>
  <td></td>
</tr>

<tr class="even"><td>Starmax 3000, 4000, 5000, 5500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Firepower, PowerStack Series E, PowerStack II</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MPC 7xx, 8xx</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MTX, MTX+</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MVME2300(SC)/24xx/26xx/27xx/36xx/46xx</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MCP(N)750</td>  <td><a href="prep">PReP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>IBM RS/6000</strong></td>
  <td></td>
</tr>

<tr class="even"><td>40P, 43P</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Power 830/850/860 (6070, 6050)</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>6015, 6030, 7025, 7043</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>p640</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>B50, 43-P150, 44P</td>  <td><a href="chrp">CHRP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Genesi</strong></td>
  <td></td>
</tr>

<tr class="even"><td>Pegasos I, Pegasos II</td>  <td><a href="chrp">CHRP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Amiga Power-UP Systems (APUS)</strong></td>
  <td></td>
</tr>

<tr class="even"><td>A1200, A3000, A4000</td>  <td><a href="apus">APUS</a></td></tr>
</table>
<p>
Aqui está uma lista de máquinas que não sabemos se funcionam com
o Debian. Elas bem que podem funcionar, e nós gostaríamos de saber
se você testar alguma e tiver algum sucesso.
</p>
<table class="reltable">
<colgroup span="2">
<tr>
  <th><strong>Modelo nome/número</strong></th>
  <th><strong>Arquitetura</strong></th>
</tr>
<tr class="even"><td>IBM Longtrail II, o primeiro 'free' ou 'open' PowerPC Board</td>   <td><a href="chrp">CHRP</a></td></tr>
<tr class="even"><td>7248-100,7248-120,7248-132 </td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Notebook Thinkpad 820: 7247-821/822/823 </td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Notebook Thinkpad 850: 7247-851/860 </td>  <td><a href="prep">PReP</a></td></tr>
</table>
