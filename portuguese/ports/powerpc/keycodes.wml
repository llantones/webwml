#use wml::debian::template title="Alteração de códigos de tecla do PowerPC" NOHEADER="yes"
#use wml::fmt::verbatim
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::translation-check translation="0cfff43768945b514bb734757927bbbd8b043626"


<h2>Alerta de códigos de tecla (keycodes) do PowerPC Linux!</h2>
<p>
Se você está rodando um kernel construído antes de abril de 2001 e
está considerando uma atualização para o woody, um atualização de
kernel ou uma alteração do mapa de teclado (keymap), é ESSENCIAL que
você esteja ciente da alteração de códigos de tecla (keycodes) no Debian para o
PowerPC no woody.</p>
<p>
Isto foi o que aconteceu: para enviar o powerpc para a linha principal do
linux e eliminar conflitos presentes e futuros, o pacote console-data do
woody foi alterado para apresentar para instalação os mapas de códigos de
teclas do linux em vez dos mapas de códigos de teclas ADB (que são usados
como a norma para kernels powerpc). Os mapas de códigos de teclas ADB não
são mais oficialmente suportados no Debian.</p>
<p>
O kernel da instalação do sistema foi modificado para acompanhar esse
caminho, então o kernel da nova instalação usa códigos de tecla linux, não
códigos de tecla ADB. Esta é uma alteração permanente; um kernel compilado sem
suporte aos códigos de tecla ADB é incapaz de usar mapas de teclado ADB.
Os códigos de tecla Linux são uma funcionalidade da "nova camada de entrada"
que é definida para se tornar a padrão para todos os dispositivos de entrada
em todas as arquiteturas durante o ciclo de versão do kernel pós-2.4.</p>

<p>
Se você preferir ficar com os códigos de tecla ADB por enquanto, e o
kernel que queira usar foi compilado com
CONFIG_MAC_ADBKEYCODES=n, você pode recompilá-lo com
CONFIG_MAC_ADBKEYCODES=y e continuar a usar um mapa de teclado ADB.</p>
<p>
Os mapas de teclado mac/apple para códigos de tecla ADB em comparação com os
códigos de tecla linux são bem diferentes. É por isso que você deve estar
preparado(a). Se você não coordenar suas instruções de boot, seu kernel e
seu mapa de teclado, algum dia você vai digitar "root" no prompt de
login e vai ver "sswj". Uma situação bem desconcertante.</p>


<h2>Planejamento futuro</h2>
<p>
Uma situação muito comum nesse sentido acontece ao se atualizar do
potato para o woody. Neste caso, uma caixa de diálogo especial foi incorporada
quando console-data é atualizado. Uma verificação é feita do kernel que
você está executando quando a atualização acontece, e se você estiver rodando
um kernel ADB, você é avisado(a) sobre a situação e solicitado(a) a fazer
uma escolha.</p>
<p>
Já que você já sabe tudo sobre o problema, será uma escolha fácil.
Entre com um novo mapa de teclado durante o boot, e após a atualização
estiver completa (ANTES da reinicialização), modifique sua configuração de
boot (quik.conf ou yaboot.conf) para adicionar uma linha para a seção
de imagem do kernel que você está fazendo o boot, dessa forma:</p>
<pre>
append="keyboard_sends_linux_keycodes=1"
</pre>
<p>
Se você já tiver uma linha append=, adicione o novo termo dentro das
aspas, assim:</p>
<pre>
append="video=ofonly keyboard_sends_linux_keycodes=1"
</pre>
<p>
Não esqueça de executar quik ou ybin após sua edição ter terminado, para
salvar as modificações de configuração para os arquivos do bootloader real.</p>
<p>
Outra instância em que este problema pode aparecer é quando se atualiza o X
de 4.0.x para 4.1.x, com um kernel de códigos de tecla ADB. Seu XF86Config-4
provavelmente tem XkbModel definido para "macintosh", mas o significado disso
foi alterado, então esse teclado macintosh é presumido que use os novos
códigos de tecla Linux. Se você vai ficar com os códigos de tecla ADB, você
precisará alterar XkbModel para "macintosh_old".</p>

<h2>Novas instalações do Woody</h2>
<p>
Para uma nova instalação do woody, você terá instalado um mapa de
teclado com códigos de tecla linux e o kernel com códigos de tecla linux.
Assim, eles estarão compatíveis e você não terá qualquer problema. A menos
que...</p>
<p>
A menos que você altere o kernel após a instalação e ele acabe sendo
um kernel ADB. Então você terá o mesmo problema ao inverso. Ou
a menos que...</p>
<p>
A menos que você altere o mapa de teclado manualmente, selecionando em
/usr/share/keymaps/mac. Esses são todos mapas de teclado ADB e eles não
corresponderão ao seu kernel de códigos de tecla linux!</p>


<h2>Isso não vai acontecer comigo -- mas quando acontece</h2>
<p>
Então, como se virar quando você digita root e vê sswj? Ou no
caso inverso, digita (tab)ssw e vê root?</p>
<p>
Entusiastas Linux odeiam desligar seus computadores. E
sempre existe alguma corrupção do sistema de arquivos quando você faz isso,
que pode ou não ser reparada corretamente. Então aqui estão algumas
sugestões para fazer o sistema desligar graciosamente se isso acontecer.</p>
<p>
Se você tem o ssh instalado no seu sistema e pode conectar de outro
computador, você pode temporariamente corrigir o problema remotamente.
Faça o login na conta root e execute o seguinte:</p>

<verbatim>
cd /proc/sys/dev/mac_hid/
echo 0 > keyboard_lock_keycodes
echo 1 > keyboard_sends_linux_keycodes
</verbatim>

<p>
Seu teclado vai assim responder normalmente até a reinicialização. Utilize
a oportunidade para sincronizar seu mapa de teclado, kernel e bootloader!</p>
<p>
Se seu sistema tem uma combinação de teclas usada como comando de
reinicialização ou desligamento, você pode tentar usar as tabelas abaixo
para descobrir qual a combinação e aplicá-la. No PowerPC, uma combinação
de teclas comum é Control-Shift-Delete. Com o mapa de teclado ADB carregado,
interpretado como código de teclas linux, isto seria Control-F6-F12. Com o
mapa de códigos de tecla linux carregado, interpretado por um kernel mapeado
por ADB, você precisaria de Shift-AltGr-Equals. Boa sorte.</p>

<p>
Meu teclado não tem a tecla Delete, então quando eu tinha um mapa
de códigos de tecla linux carregado em um kernel configurado com ADB, eu
descobri, usando a tabela, como fazer o login como root ( 2==3 seguido por F5).
Para minha senha de root, eu usei a tabela abaixo. Para reinicialização, eu
digitei ( 21 tecla-tab ==3 seguido por F5 ). Você também pode usar halt
( p]j3 seguido por F5 ).</p>
<p>
Alguns caracteres não podem ser digitados no mapa de teclado errado. Eles
estão vazios ou ausentes nas tabelas.</p>

<h2>Mapa de teclado com códigos de tecla Linux carregado, kernel configurado para ADB</h2>

<p>&nbsp;</p>

<pre>
Se você quer: a b   c d e f g h i j      k l m n     o p q r s t u v w
então digite: ] TAB m u 1 [ i p 5 Return l j ` Space = 9 y 2 o 3 6 . t

               x y z 0 1 2 3 4 5 6 7 8 9 *   /  [ ] ,         = - ` \ ;
               n 4 / b d f h g z x c v   Alt F7 7 - Backspace w q ; , '

              Control Shift     Enter Tab Backspace Fwd-Del Space
              F6      Ctrl or \ F5, 8 r   e         F12     CapsLock

              Home   NumLock   Clear  AltGr =(numkpd) Escape F11 F12
              Clear  +(numkpd) F6-6   Shift Fwd-Del   s      kp-5 kp-6

              F1         F2          F3         F4       F7   F9  
              Left-Arrow Right-Arrow Down-Arrow Up-Arrow kp-. kp-*

              Left-Arrow Right-Arrow Up-Arrow Down-Arrow
              F13                    F11      
</pre>

<h2>Kernel configurado para códigos de tecla Linux, mapa de teclado ADB carregado</h2>

<pre>
Se você quer: a b c d e         f g h i j k l m n o p q r   s   t u v w
então digite:   0 7 1 Backspace 2 4 3 g l ' k c x s h - Tab Esc w d 8 = 

              x y z 0       1 2 3 4 5 6 7 8      9 * /  [ ] , = - ` ; 
              6 q 5 Control e r t y i u [ Return p   z  f a \ o ] m ` 

              Control Shift Return Tab Backspace Fwd-Del   \     Space
              Shift   AltGr j      b   ,         =(numkpd) Shift n

              Clear  AltGr    =(numkpd) CapsLock Escape Alt
              Home   CapsLock Fwd-Del   Space    /       kp-*

              F1 F2 F3 F4 F5    F6   F7 F8 F9 F0 F11 F12
                          Enter Ctrl /               Fwd-Del

              Left-Arrow Right-Arrow Up-Arrow Down-Arrow
              F1         F2          F4       F3
</pre>


<h2>Como descobrir sua situação atual</h2>
<p>
Atualmente os mapas de teclado não têm comentários dentro, então se você
está se perguntando que tipo de mapa está ativo, você pode descobrir olhando
na linha keycode 1 com</p>
<pre>
zgrep 'keycode *1 =' /etc/console/boottime.kmap.gz
</pre>
Se keycode 1 = Escape, é o mapa de códigos de tecla linux (na verdade, i386).
Se keycode 1 = s, é o ADB (exceto pelo ADB dvorak, keycode 1 = o).
<p>
Os arquivos config-XXXXX em /boot revelarão se o kernel que você está
iniciando está compilado com ou sem suporte para códigos de tecla ADB.
Para descobrir, use</p>
<pre>
grep MAC_ADB /boot/*
</pre>
<p>
Você deve conseguir uma lista de arquivos de configuração para os kernels
que você pode iniciar. Se houver uma entrada do tipo</p>
<pre>
/boot/config-2.4.12-powerpc:CONFIG_MAC_ADBKEYCODES=y
</pre>
<p>então seu kernel está compilado com suporte aos códigos ADB. Se a
última letra é n, é um kernel com códigos de tecla linux.</p>

<h2>Como consertar</h2>
<p>
Uma vez que você tenha conseguido desligar seu sistema errante, você ainda
precisa consertá-lo. Como você pode saber o que é necessário? Você pode ter
que usar seu disco de recuperação ou fazer boot em outra partição de modo
a corrigir os problemas.</p>

<p>
Se o seu problema é um kernel compilado com ADB tentando usar um mapa de
teclado de códigos linux, somente adicione</p>
<pre>
keyboard_sends_linux_keycodes=1
</pre>
<p>no prompt do boot: após digitar o rótulo da imagem de kernel. Isto é somente
uma correção para este boot; você precisa fazer a alteração permanente pela
edição do seu arquivo de configuração de boot e salvá-lo para o bootloader.</p>
<p>
Se o seu problema é o inverso (kernel com códigos de tecla linux tentando usar
um mapa de teclado ADB), você precisará se livrar do mapa de teclado ADB. Você
pode copiar qualquer mapa de teclado do diretório /usr/share/keymaps/i386,
há vários para se escolher. Por exemplo</p>
<pre>
cd /usr/share/keymaps/i386/qwerty/
cp mac-usb-us.kmap.gz /etc/console/boottime.kmap.gz
</pre>
