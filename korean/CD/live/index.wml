#use wml::debian::cdimage title="라이브 설치 이미지"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7" maintainer="Sebul"

<p><q>라이브 설치</q> 이미지에는 하드디스크 안의 어떤 파일도 바꾸지 않고 부팅할 수 있고, 이미지 내용에서 데비안 설치도 허용하는 데비안 시스템이 들어있습니다.
</p>

<p><a name="choose_live"><strong>라이브 이미지가 적절한가?</strong></a> 
여러분의 결정을 도울 고려 사항 몇 가지가 있습니다.
<ul>
<li><b>플레이버:</b> 라이브 이미지는 데스크탑 환경 (GNOME, KDE, LXDE, Xfce, Cinnamon 및 MATE)을 선택할 수 있는 여러 가지 "플레이버"를 제공합니다.
많은 사용자는 이러한 초기 패키지 선택이 적합함을 알고 나중에 네트워크에서 필요한 추가 패키지를 설치합니다. 
<li><b>아키텍처:</b> 가장 인기있는 2개 아키텍처
32비트 PC (i386) 및 64비트 PC (amd64)만 현재 제공됩니다.
<li><b>크기:</b> 각 이미지는 DVD 이미지 전체 세트보다 작지만, 네트워크 설치 미디어보다 큽니다.
<li><b>언어:</b> 이미지에는 패키지를 지원하는 모든 언어 세트가 들어 있는 건 아닙니다.
입력기, 폰트, 언어 패키지가 필요하면 이것을 설치할 필요가 있을 겁니다.
</ul>

<p>다운로드 가능한 라이브 설치 이미지:</p>

<ul>

  <li>공식 <q>라이브 설치</q> 이미지 <q>안정</q> 릴리스 &mdash; <a
  href="#live-install-stable">아래를 보세요</a></li>

</ul>


<h2 id="live-install-stable">공식 라이브 설치 이미지 <q>안정</q> 릴리스</h2>

<p>위에서 설명한 것처럼 크기가 다른 여러 가지 플레이버로 제공되는 이 이미지는 
선택된 기본 패키지 세트로 구성된 데비안 시스템을 써보고 동일한 매체에서 설치하는 데 적합합니다.
</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>하이브리드</q> ISO 이미지 파일은 DVD-R(W) 매체 및 적절한 크기의 USB 메모리에 쓰기 적합합니다.
BitTorrent를 사용할 수 있다면 그렇게 하세요.서버의 부하가 줄어듭니다.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>DVD/USB</strong></p>
<p><q>하이브리드</q> ISO 이미지 파일은 DVD-R(W) 매체 및 USB 메모리 쓰기 적당한 크기입니다.</p>
       <stable-live-install-iso-cd-images /> 
</div> </div>

<p>이 파일들이 무엇이며 그것을 어떻게 쓰는지에 대한 정보는 <a href="../faq/">FAQ</a>를 보세요.</p>

<p>다운로드한 라이브 이미지에서 데비안을 설치하려면
<a href="$(HOME)/releases/stable/installmanual">설치 절차 자세한 정보</a>를 보세요.</p>

<p>이 이미지에서 제공하는 데비안 라이브 시스템에 대한 더 많은 정보는 <a href="$(HOME)/devel/debian-live">데비안 라이브 프로젝트 페이지</a>에
있습니다.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">비공식 라이브 CD/DVD 이미지, 비자유 펌웨어 포함 </a></h2>

<div id="firmware_nonfree" class="important">
<p>
시스템에 <strong>비자유 펌웨어가 장치 드라이버와 함께 로드</strong>되어야 하는 하드웨어가 있으면, 
일반 펌웨어 패키지의 <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">Tarball</a> 
중 하나를 사용하거나 이러한 <strong>non-free</strong> 펌웨어를 포함한 <strong>비공식</strong> 이미지를 다운로드할 수 있습니다. 
Tarball 사용 방법 및 설치 중 펌웨어 로드에 대한 일반적인 정보는 <a href="../../releases/stable/amd64/ch06s04">설치 안내서</a>에서 확인할 수 있습니다.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/">비공식
라이브 이미지 <q>안정</q> 펌웨어 포함</a>
</p>
</div>
