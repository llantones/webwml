msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:51+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Feliratkozás Levelező Listára"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Információkért lásd a <a href=\"./#subunsub\">levelező listák</a> oldalt, "
"hogy hogyan tudsz e-mail-ben feliratkozni. A leiratkozáshoz <a href="
"\"subscribe\">leiratkozó web form</a> is elérhető."

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Figyelem! A legtöbb Debian levelező lista nyilvános fórum. Bármely, a "
"listáraküldött levél nyilvános archívumban kerül megjelenítésre és "
"indexelésrekeresőmotorok által. Csak olyan e-mail címmel iratkozzál fel "
"Debian levelezőlistára, amelynél nem probléma, ha nyilvánosságra kerül."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Kérlek, válaszd ki a listát, amelyre fel szeretnél iratkozni (a "
"feliratkozásokszáma limitált - ha a kérésed nem teljesül, kérlek, használd a "
"<a href=\"./#subunsub\">másik módszert</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Nincs leírás"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Moderálja:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Csak feliratkozottak küldhetnek üzeneteket."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr "Csak Debian fejlesztők által aláírt üzeneteket fogad ez a lista."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Feliratkozás:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "csak olvasható, kivonatolt verzió."

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "A Te e-mail címed:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Feliratkozás"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Tiszta"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Kérlek, tartsd tiszteletben a <a href=\"./#ads\">hirdetés a Debian "
"levelezőlistákon irányelveket</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Leiratkozás a levelező listáról"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Információkért lásd a <a href=\"./#subunsub\">levelező listák</a> oldalt, "
"hogy hogyan tudsz e-mail-ben leiratkozni. A feliratkozáshoz <a href="
"\"subscribe\">feliratkozó web form</a> is elérhető."

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Kérlek válaszd ki, melyik listáról szeretnél leiratkozni (a leiratkozások "
"száma limitált, ha a kérésed nem teljesült, kérlek használd a <a href=\"./"
"#subunsub\">másik módszert</a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Leiratkozás"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "nyitott"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "zárt"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Válaszd ki, melyik listára szeretnél feliratkozni:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Válaszd ki, melyik listáról szeretnél leiratkozni:"
