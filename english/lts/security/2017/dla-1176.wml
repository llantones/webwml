<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9988">CVE-2017-9988</a>

    <p>NULL pointer dereference in the readEncUInt30 function (util/read.c)
    in Ming <= 0.4.8, which allows attackers to cause a denial of service
    via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9989">CVE-2017-9989</a>

    <p>NULL pointer dereference in the outputABC_STRING_INFO function
    (util/outputtxt.c) in Ming <= 0.4.8, which allows attackers to cause
    a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11733">CVE-2017-11733</a>

    <p>NULL pointer dereference in the stackswap function (util/decompile.c)
    in Ming <= 0.4.8, which allows attackers to cause a denial of service
    via a crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.4.4-1.1+deb7u5.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1176.data"
# $Id: $
