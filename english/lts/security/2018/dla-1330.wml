<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that constructed ASN.1 types with a recursive
definition could exceed the stack, potentially leading to a denial of
service.</p>

<p>Details can be found in the upstream advisory: <a href="https://www.openssl.org/news/secadv/20180327.txt">https://www.openssl.org/news/secadv/20180327.txt</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.1t-1+deb7u4.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1330.data"
# $Id: $
