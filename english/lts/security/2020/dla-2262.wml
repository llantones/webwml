<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were fixed in qemu, a fast processor emulator.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1983">CVE-2020-1983</a>

    <p>slirp: Fix use-after-free in ip_reass().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13361">CVE-2020-13361</a>

    <p>es1370_transfer_audio in hw/audio/es1370.c
    allowed guest OS users to trigger an out-of-bounds access
    during an es1370_write() operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13362">CVE-2020-13362</a>

    <p>megasas_lookup_frame in hw/scsi/megasas.c had
     an out-of-bounds read via a crafted reply_queue_head field from
     a guest OS user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13765">CVE-2020-13765</a>

    <p>hw/core/loader: Fix possible crash in rom_copy().</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u15.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2262.data"
# $Id: $
