<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in otrs2 (Open source Ticket Request System)</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1770">CVE-2020-1770</a>

    <p>Support bundle generated files could contain sensitive information
    that might be unwanted to be disclosed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>

    <p>It’s possible to craft Lost Password requests with wildcards in the
    Token value, which allows attacker to retrieve valid Token(s),
    generated by users which already requested new passwords.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1774">CVE-2020-1774</a>

    <p>When user downloads PGP or S/MIME keys/certificates, exported file
    has same name for private and public keys. Therefore it’s possible
    to mix them and to send private key to the third-party instead of
    public key.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u15.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2198.data"
# $Id: $
