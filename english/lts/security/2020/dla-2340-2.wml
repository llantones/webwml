<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of sqlite3 released as DLA-2340-1 contained an incomplete fix
for <a href="https://security-tracker.debian.org/tracker/CVE-2019-20218">CVE-2019-20218</a>.  Updated sqlite3 packages are now available to
correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.16.2-5+deb9u3.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>For the detailed security status of sqlite3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sqlite3">https://security-tracker.debian.org/tracker/sqlite3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2340-2.data"
# $Id: $
