<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue where incorrect default
permissions on a HTTP cookie store could have allowed local attackers to read
private credentials in <tt>libzypp</tt>, a package management library that
powers applications like YaST, zypper and the openSUSE/SLE implementation of
PackageKit</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18900">CVE-2019-18900</a>

    <p>Incorrect Default Permissions vulnerability in libzypp of SUSE CaaS Platform 3.0, SUSE Linux Enterprise Server 12, SUSE Linux Enterprise Server 15 allowed local attackers to read a cookie store used by libzypp, exposing private cookies. This issue affects: SUSE CaaS Platform 3.0 libzypp versions prior to 16.21.2-27.68.1. SUSE Linux Enterprise Server 12 libzypp versions prior to 16.21.2-2.45.1. SUSE Linux Enterprise Server 15 17.19.0-3.34.1.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.29.1-2+deb8u1.</p>

<p>We recommend that you upgrade your libzypp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2132.data"
# $Id: $
