<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were fixed in nss,
the Network Security Service libraries.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12399">CVE-2020-12399</a>

    <p>Force a fixed length for DSA exponentiation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12402">CVE-2020-12402</a>

    <p>Side channel vulnerabilities during RSA key generation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:3.26-1+debu8u11.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2266.data"
# $Id: $
