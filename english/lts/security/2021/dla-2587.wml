<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilites were discovered in privoxy, a web proxy with
advanced filtering capabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20272">CVE-2021-20272</a>

    <p>An assertion failure could be triggered with a crafted CGI
    request leading to server crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20273">CVE-2021-20273</a>

    <p>A crash can occur via a crafted CGI request if Privoxy is toggled
    off.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20275">CVE-2021-20275</a>

     <p>An invalid read of size two may occur in
     chunked_body_is_complete() leading to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20276">CVE-2021-20276</a>

    <p>Invalid memory access with an invalid pattern passed to
    pcre_compile() may lead to denial of service.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.0.26-3+deb9u2.</p>

<p>We recommend that you upgrade your privoxy packages.</p>

<p>For the detailed security status of privoxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/privoxy">https://security-tracker.debian.org/tracker/privoxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2587.data"
# $Id: $
