<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>lxml is a library for processing XML and HTML in the Python language.
Prior to version 4.6.5, the HTML Cleaner in lxml.html lets certain
crafted script content pass through, as well as script content in
SVG files embedded using data URIs.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.7.1-1+deb9u5.</p>

<p>We recommend that you upgrade your lxml packages.</p>

<p>For the detailed security status of lxml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2871.data"
# $Id: $
