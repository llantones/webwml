<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in atftp, an advanced TFTP client.
Both are related to sending crafted requests to the server and triggering
a denial-of-service due to for example a buffer overflow.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.7.git20120829-3.1~deb9u2.</p>

<p>We recommend that you upgrade your atftp packages.</p>

<p>For the detailed security status of atftp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/atftp">https://security-tracker.debian.org/tracker/atftp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2820.data"
# $Id: $
