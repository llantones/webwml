<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two SQL injection vulnerabilities were discovered in SQLAlchemy, a SQL
toolkit and Object Relational Mapper for Python, when the order_by or
group_by parameters can be controlled by an attacker.</p>

<p>Warning: The text coercion feature of SQLAlchemy is rarely used but the
warning that has been previously emitted is now an ArgumentError or in case
of the order_by() and group_by() parameters a CompileError.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.15+ds1-1+deb9u1.</p>

<p>We recommend that you upgrade your sqlalchemy packages.</p>

<p>For the detailed security status of sqlalchemy please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/sqlalchemy">https://security-tracker.debian.org/tracker/sqlalchemy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2811.data"
# $Id: $
