<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Gson, a Java library that can be used to convert Java
Objects into their JSON representations and vice versa, was vulnerable to a deserialization flaw. An application would de-serialize untrusted data without
sufficiently verifying that the resulting data will be valid, letting the
attacker to control the state or the flow of the execution. This can lead to a
denial of service or even the execution of arbitrary code.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.8.5-3+deb10u1.</p>

<p>We recommend that you upgrade your libgoogle-gson-java packages.</p>

<p>For the detailed security status of libgoogle-gson-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libgoogle-gson-java">https://security-tracker.debian.org/tracker/libgoogle-gson-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3100.data"
# $Id: $
