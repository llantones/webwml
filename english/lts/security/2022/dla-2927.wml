<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Twisted, a Python event-based framework for
internet applications, is affected by HTTP request splitting
vulnerabilities, and may expose sensitive data when following
redirects. An attacker may bypass validation checks and retrieve
credentials.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10108">CVE-2020-10108</a>

    <p>HTTP request splitting vulnerability. When presented with two
    content-length headers, it ignored the first header. When the
    second content-length value was set to zero, the request body was
    interpreted as a pipelined request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10109">CVE-2020-10109</a>

    <p>HTTP request splitting vulnerability. When presented with a
    content-length and a chunked encoding header, the content-length
    took precedence and the remainder of the request body was
    interpreted as a pipelined request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21712">CVE-2022-21712</a>

    <p>Twisted exposes cookies and authorization headers when following
    cross-origin redirects. This issue is present in the
    `twisted.web.RedirectAgent` and
    `twisted.web.BrowserLikeRedirectAgent` functions.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
16.6.0-2+deb9u1.</p>

<p>We recommend that you upgrade your twisted packages.</p>

<p>For the detailed security status of twisted please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/twisted">https://security-tracker.debian.org/tracker/twisted</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2927.data"
# $Id: $
