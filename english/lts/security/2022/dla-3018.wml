<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that libpgjava, the official PostgreSQL JDBC Driver, would be
vulnerable if an attacker controlled jdbc url or properties. The JDBC driver
did not verify if certain classes implemented the expected interface before
instantiating the class. This can lead to code execution loaded via arbitrary
classes.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
9.4.1212-1+deb9u1.</p>

<p>We recommend that you upgrade your libpgjava packages.</p>

<p>For the detailed security status of libpgjava please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libpgjava">https://security-tracker.debian.org/tracker/libpgjava</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3018.data"
# $Id: $
