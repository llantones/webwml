<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>When parsing files containing Nef polygon data, several memory access
violations may happen. Many of these allow code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28601">CVE-2020-28601</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionality of CGAL. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser::read_vertex()</code> <code>Face_of[]</code> OOB read.
    An attacker can provide malicious input to trigger this
    vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28602">CVE-2020-28602</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionality of CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_vertex()</code>
    <code>Halfedge_of[]</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28603">CVE-2020-28603</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionality of CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code> <code>e-&gt;set_prev()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28604">CVE-2020-28604</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionality of CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code> <code>e-&gt;set_next()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28605">CVE-2020-28605</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionality of CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code>
    <code>e-&gt;set_vertex()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28606">CVE-2020-28606</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code> <code>e-&gt;set_face()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28607">CVE-2020-28607</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution.  An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> set_halfedge().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28608">CVE-2020-28608</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> store_fc().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28609">CVE-2020-28609</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> store_iv().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28610">CVE-2020-28610</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SM_io_parser.h</code> <code>SM_io_parser&lt;Decorator_&gt;::read_vertex()</code>
    set_face().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28611">CVE-2020-28611</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SM_io_parser.h</code> <code>SM_io_parser&lt;Decorator_&gt;::read_vertex()</code>
    set_first_out_edge().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28612">CVE-2020-28612</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;svertices_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28613">CVE-2020-28613</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;svertices_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28614">CVE-2020-28614</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;shalfedges_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28615">CVE-2020-28615</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;shalfedges_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28616">CVE-2020-28616</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;sfaces_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28617">CVE-2020-28617</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;sfaces_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28618">CVE-2020-28618</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
    <code>vh-&gt;shalfloop()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28619">CVE-2020-28619</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_edge()</code> <code>eh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28620">CVE-2020-28620</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
    <code>eh-&gt;center_vertex()</code>:.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28621">CVE-2020-28621</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
    <code>eh-&gt;out_sedge()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28622">CVE-2020-28622</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
    <code>eh-&gt;incident_sface()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28623">CVE-2020-28623</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_facet()</code> <code>fh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28624">CVE-2020-28624</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
    <code>fh-&gt;boundary_entry_objects</code> SEdge_of.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28625">CVE-2020-28625</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
    <code>fh-&gt;boundary_entry_objects</code> SLoop_of.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28626">CVE-2020-28626</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
    <code>fh-&gt;incident_volume()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28627">CVE-2020-28627</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_volume()</code>
    <code>ch-&gt;shell_entry_objects()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28628">CVE-2020-28628</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_volume()</code> <code>seh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28629">CVE-2020-28629</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;sprev()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28630">CVE-2020-28630</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;snext()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28631">CVE-2020-28631</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;source()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28632">CVE-2020-28632</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code>
    <code>seh-&gt;incident_sface()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28633">CVE-2020-28633</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;prev()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28634">CVE-2020-28634</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;next()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28635">CVE-2020-28635</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;facet()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28636">CVE-2020-28636</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionalityof CGAL. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser::read_sloop()</code> <code>slh-&gt;twin()</code> An
    attacker can provide malicious input to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35628">CVE-2020-35628</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionalityof CGAL. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser::read_sloop()</code>
    <code>slh-&gt;incident_sface</code>. An attacker can provide malicious input to
    trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35629">CVE-2020-35629</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sloop()</code> <code>slh-&gt;facet()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35630">CVE-2020-35630</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
    <code>sfh-&gt;center_vertex()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35631">CVE-2020-35631</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
    <code>SD.link_as_face_cycle()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35632">CVE-2020-35632</a>

    <p>Multiple code execution vulnerabilities exists in the Nef polygon    parsing functionalityof CGAL. A specially crafted malformed file can
    lead to an out-of-bounds read and type confusion, which could lead to
    code execution. An attacker can provide malicious input to trigger
    any of these vulnerabilities. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
    <code>sfh-&gt;boundary_entry_objects</code> <code>Edge_of</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35633">CVE-2020-35633</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionalityof CGAL. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
    <code>store_sm_boundary_item()</code> <code>Edge_of</code>. A specially crafted malformed file
    can lead to an out-of-bounds read and type confusion, which could
    lead to code execution. An attacker can provide malicious input to
    trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35634">CVE-2020-35634</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionalityof CGAL. An oob read vulnerability exists in
    <code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
    <code>sfh-&gt;boundary_entry_objects</code> <code>Sloop_of</code>. A specially crafted malformed
    file can lead to an out-of-bounds read and type confusion, which
    could lead to code execution. An attacker can provide malicious input
    to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35635">CVE-2020-35635</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionality of CGAL libcgal CGAL-5.1.1 in <code>Nef_S2/SNC_io_parser.h</code>
    <code>SNC_io_parser::read_sface()</code> <code>store_sm_boundary_item()</code> <code>Sloop_of</code> OOB
    read. A specially crafted malformed file can lead to an out-of-bounds
    read and type confusion, which could lead to code execution. An
    attacker can provide malicious input to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35636">CVE-2020-35636</a>

    <p>A code execution vulnerability exists in the Nef polygon-parsing
    functionality of CGAL libcgal CGAL-5.1.1 in <code>Nef_S2/SNC_io_parser.h</code>
    <code>SNC_io_parser::read_sface()</code> <code>sfh-&gt;volume()</code> OOB read. A specially
    crafted malformed file can lead to an out-of-bounds read and type
    confusion, which could lead to code execution. An attacker can
    provide malicious input to trigger this vulnerability.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.13-1+deb10u1.</p>

<p>We recommend that you upgrade your cgal packages.</p>

<p>For the detailed security status of cgal please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cgal">https://security-tracker.debian.org/tracker/cgal</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3226.data"
# $Id: $
