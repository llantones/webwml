<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in cacti, a web
interface for graphing of monitoring systems, which may result in
information disclosure, authentication bypass, or remote code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8813">CVE-2020-8813</a>

    <p>Askar discovered that an authenticated guest user with the graph
    real-time privilege could execute arbitrary code on a server running
    Cacti, via shell meta-characters in a cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23226">CVE-2020-23226</a>

    <p>Jing Chen discovered multiple Cross Site Scripting (XSS)
    vulnerabilities in several pages, which can lead to information
    disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25706">CVE-2020-25706</a>

    <p>joelister discovered an Cross Site Scripting (XSS) vulnerability in
    templates_import.php, which can lead to information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0730">CVE-2022-0730</a>

    <p>It has been discovered that Cacti authentication can be bypassed
    when LDAP anonymous binding is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46169">CVE-2022-46169</a>

    <p>Stefan Schiller discovered a command injection vulnerability,
    allowing an unauthenticated user to execute arbitrary code on a
    server running Cacti, if a specific data source was selected (which
    is likely the case on a production instance) for any monitored
    device.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.2.2+ds1-2+deb10u5.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>For the detailed security status of cacti please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cacti">https://security-tracker.debian.org/tracker/cacti</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3252.data"
# $Id: $
