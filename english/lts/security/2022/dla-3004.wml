<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an integer overflow vulnerabiliity in
htmldoc, a HTML processor that generates indexed HTML, PS and PDF files. This
was caused by a programming error in image_load_jpeg function due to a
conflation or confusion of declared/expected/observed image dimensions.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27114">CVE-2022-27114</a>

       <p>CVE-2022-27114: There is a vulnerability in htmldoc 1.9.16. In
       image_load_jpeg function image.cxx when it calls malloc,'img-&gt;width'
       and 'img-&gt;height' they are large enough to cause an integer
       overflow. So, the malloc function may return a heap block smaller than the
       expected size, and it will cause a buffer overflow/Address boundary
       error in the jpeg_read_scanlines function.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.8.27-8+deb9u3.</p>

<p>We recommend that you upgrade your htmldoc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3004.data"
# $Id: $
