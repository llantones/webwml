<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Panorama Tools, a toolkit to generate, edit and
transform many kinds of panoramic images, contained an out-of-bounds read
vulnerability which could lead to a denial of service (application crash)
when a malformed image file is processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.9.19+dfsg-2+deb9u2.</p>

<p>We recommend that you upgrade your libpano13 packages.</p>

<p>For the detailed security status of libpano13 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libpano13">https://security-tracker.debian.org/tracker/libpano13</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2957.data"
# $Id: $
