<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a information disclosure utility in sudo, a
tool used to provide limited superuser privileges to specific users. A local
unprivileged user may have been able to perform arbitrary directory-existence
tests by exploiting a race condition in sudoedit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23239">CVE-2021-23239</a>

    <p>The sudoedit personality of Sudo before 1.9.5 may allow a local
    unprivileged user to perform arbitrary directory-existence tests by winning
    a sudo_edit.c race condition in replacing a user-controlled directory by a
    symlink to an arbitrary path.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.8.27-1+deb10u4.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3181.data"
# $Id: $
