<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that in Exim, a mail transport agent, handling an
e-mail can cause a heap-based buffer overflow in some situations. An
attacker can cause a denial-of-service (DoS) and possibly execute
arbitrary code.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.92-8+deb10u7.</p>

<p>We recommend that you upgrade your exim4 packages.</p>

<p>For the detailed security status of exim4 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/exim4">https://security-tracker.debian.org/tracker/exim4</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3082.data"
# $Id: $
