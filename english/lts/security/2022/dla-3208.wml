<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Martin van Kervel Smedshammer discovered a request forgery attack can be
performed on Varnish Cache servers that have the HTTP/2 protocol turned on. An
attacker may introduce characters through the HTTP/2 pseudo-headers that are
invalid in the context of an HTTP/1 request line, causing the Varnish server to
produce invalid HTTP/1 requests to the backend. This may in turn be used to
successfully exploit vulnerabilities in a server behind the Varnish server.</p>

<p>For Debian 10 buster, these problems have been fixed in version
6.1.1-1+deb10u4.</p>

<p>We recommend that you upgrade your varnish packages.</p>

<p>For the detailed security status of varnish please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/varnish">https://security-tracker.debian.org/tracker/varnish</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3208.data"
# $Id: $
