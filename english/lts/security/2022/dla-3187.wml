<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An issue was discovered in Dropbear, a relatively small SSH server and
client. Due to a non-RFC-compliant check of the available authentication
methods in the client-side SSH code, it was possible for an SSH server
to change the login process in its favor. This attack can bypass
additional security measures such as FIDO2 tokens or SSH-Askpass. Thus,
it allows an attacker to abuse a forwarded agent for logging on to
another server unnoticed.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2018.76-5+deb10u2.</p>

<p>We recommend that you upgrade your dropbear packages.</p>

<p>For the detailed security status of dropbear please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dropbear">https://security-tracker.debian.org/tracker/dropbear</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3187.data"
# $Id: $
