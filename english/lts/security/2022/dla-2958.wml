<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A use-after-free vulnerability was found in Usbredirparser, a parser
for the usbredir protocol, which could result in denial of service or
potentially arbitrary code execution.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.7.1-1+deb9u1.</p>

<p>We recommend that you upgrade your usbredir packages.</p>

<p>For the detailed security status of usbredir please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/usbredir">https://security-tracker.debian.org/tracker/usbredir</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2958.data"
# $Id: $
