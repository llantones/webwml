<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Apache Log4j 1.2, a
Java logging framework, when it is configured to use JMSSink, JDBCAppender,
JMSAppender or Apache Chainsaw which could be exploited for remote code
execution.</p>

<p>Note that a possible attacker requires write access to the Log4j configuration
and the aforementioned features are not enabled by default. In order to
completely mitigate against these type of vulnerabilities the related classes
have been removed from the resulting jar file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.2.17-7+deb9u2.</p>

<p>We recommend that you upgrade your apache-log4j1.2 packages.</p>

<p>For the detailed security status of apache-log4j1.2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j1.2">https://security-tracker.debian.org/tracker/apache-log4j1.2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2905.data"
# $Id: $
