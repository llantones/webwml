<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Evgeny Vereshchagin discovered multiple vulnerabilities in D-Bus, a
simple interprocess messaging system, which may result in denial of
service by an authenticated user.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.12.24-0+deb10u1.</p>

<p>We recommend that you upgrade your dbus packages.</p>

<p>For the detailed security status of dbus please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dbus">https://security-tracker.debian.org/tracker/dbus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3142.data"
# $Id: $
