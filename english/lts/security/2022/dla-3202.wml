<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Three issues have been found in libarchive, a multi-format archive and
compression library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19221">CVE-2019-19221</a>

    <p>out-of-bounds read because of an incorrect mbrtowc or mbtowc call</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23177">CVE-2021-23177</a>

    <p>extracting a symlink with ACLs modifies ACLs of target</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31566">CVE-2021-31566</a>

    <p>symbolic links incorrectly followed when changing modes, times,
    ACL and flags of a file while extracting an archive</p>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.3.3-4+deb10u2.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>For the detailed security status of libarchive please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libarchive">https://security-tracker.debian.org/tracker/libarchive</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3202.data"
# $Id: $
