<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been found in nss.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4000">CVE-2015-4000</a>

 <p>With TLS protocol 1.2 and earlier, when a DHE_EXPORT ciphersuite is
 enabled on a server but not on a client, does not properly convey
 a DHE_EXPORT choice, which allows man-in-the-middle attackers to
 conduct cipher-downgrade attacks by rewriting a ClientHello with
 DHE replaced by DHE_EXPORT and then rewriting a ServerHello with
 DHE_EXPORT replaced by DHE, aka the <q>Logjam</q> issue.</p>

<p>The solution in nss was to not accept bit lengths less than 1024.
This may potentially be a backwards incompatibility issue but such
low bit lengths should not be in use so it was deemed acceptable.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.14.5-1+deb7u7.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-507.data"
# $Id: $
