<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several flaws were corrected in SQLite, an SQL database engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2518">CVE-2017-2518</a>

    <p>A use-after-free bug in the query optimizer may cause a
    buffer overflow and application crash via a crafted SQL statement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2519">CVE-2017-2519</a>

    <p>Insufficient size of the reference count on Table objects
    could lead to a denial-of-service or arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2520">CVE-2017-2520</a>

    <p>The sqlite3_value_text() interface returned a buffer that was not
    large enough to hold the complete string plus zero terminator when
    the input was a zeroblob. This could lead to arbitrary code
    execution or a denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10989">CVE-2017-10989</a>

    <p>SQLite mishandles undersized RTree blobs in a crafted database
    leading to a heap-based buffer over-read or possibly unspecified
    other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

    <p>Databases whose schema is corrupted using a CREATE TABLE AS
    statement could cause a NULL pointer dereference.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.8.7.1-1+deb8u4.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1633.data"
# $Id: $
