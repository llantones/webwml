<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of apache2 released as DLA-1900-1 contained an incomplete
fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>, a limited cross-site scripting issue affecting
the mod_proxy error page. The old patch rather introduced a new CSRF
protection which also caused a regression, an inability to dynamically
change the status of members in the balancer via the balancer-manager.
This update reverts the change and provides the correct upstream patch
to address <a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.4.10-10+deb8u16.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1900-2.data"
# $Id: $
