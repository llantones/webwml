<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in the Open Ticket
Request System that could lead to information disclosure or privilege
escalation. New configuration options were added to resolve those
problems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>

    <p>An attacker could send a malicious email to an OTRS system. If a
    logged in agent user quotes it, the email could cause the browser to
    load external image resources.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12497">CVE-2019-12497</a>

    <p>In the customer or external frontend, personal information of agents
    can be disclosed like Name and mail address in external notes.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u10.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1816.data"
# $Id: $
