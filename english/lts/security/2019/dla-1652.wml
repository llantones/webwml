<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found by Kaspersky Lab in libvncserver, a C library
to implement VNC server/client functionalities. In addition, some of the
vulnerabilities addressed in DLA 1617-1 were found to have incomplete
fixes, and have been addressed in this update.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15126">CVE-2018-15126</a>

    <p>An attacker can cause denial of service or remote code execution via
    a heap use-after-free issue in the tightvnc-filetransfer extension.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-20749">CVE-2018-20749</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-20750">CVE-2018-20750</a>

    <p>Some of the out of bound heap write fixes for <a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a> and
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a> were incomplete. These CVEs address those issues.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.9.9+dfsg2-6.1+deb8u5.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1652.data"
# $Id: $
