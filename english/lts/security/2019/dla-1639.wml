<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the journald component of
systemd which can lead to a crash or code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>

    <p>An allocation of memory without limits, that could result in the
    stack clashing with another memory region, was discovered in
    systemd-journald when many entries are sent to the journal
    socket. A local attacker, or a remote one if
    systemd-journal-remote is used, may use this flaw to crash
    systemd-journald or execute code with journald privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16865">CVE-2018-16865</a>

    <p>An allocation of memory without limits, that could result in the
    stack clashing with another memory region, was discovered in
    systemd-journald when a program with long command line arguments
    calls syslog. A local attacker may use this flaw to crash
    systemd-journald or escalate his privileges. Versions through v240
    are vulnerable.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
215-17+deb8u9.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1639.data"
# $Id: $
