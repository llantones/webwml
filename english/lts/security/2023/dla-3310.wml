<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn, working with Trend Micro Zero Day Initiative, discovered
a vulnerability in the X.Org X server.
A potential use after free mighty result in local privilege escalation if
the X server is running privileged or remote code execution during ssh X
forwarding sessions.</p>


<p>For Debian 10 buster, this problem has been fixed in version
2:1.20.4-1+deb10u8.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>For the detailed security status of xorg-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xorg-server">https://security-tracker.debian.org/tracker/xorg-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3310.data"
# $Id: $
