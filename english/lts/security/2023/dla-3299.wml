<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Nathanael Braun and Johan Brissaud discovered a prototype poisoning
vulnerability in node-qs, a Node.js module to parse and stringify query
strings.  node-qs 6.5.x before 6.5.3 allows for instance the creation of
array-like objects by setting an Array in the <code>__ proto__</code>
property; the resulting Objects inherit the <code>Array</code> prototype,
thereby exposing native Array functions.</p>

<p>For Debian 10 buster, this problem has been fixed in version
6.5.2-1+deb10u1.</p>

<p>We recommend that you upgrade your node-qs packages.</p>

<p>For the detailed security status of node-qs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-qs">https://security-tracker.debian.org/tracker/node-qs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3299.data"
# $Id: $
