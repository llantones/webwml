<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Curl, an easy-to-use client-side
URL transfer library, which could result in denial of service or
information disclosure.</p>

<p>This update also revises the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2022-27782">CVE-2022-27782</a> released in DLA-3085-1.</p>

<p>For Debian 10 buster, these problems have been fixed in version
7.64.0-4+deb10u4.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3288.data"
# $Id: $
