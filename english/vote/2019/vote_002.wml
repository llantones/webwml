<define-tag pagetitle>General Resolution: Init systems and systemd</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in 
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms, 
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse, 
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss, 
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Proposal and amendment</th>
        <td>2019-11-16</td>
		<td></td>
      </tr>
      <tr>
        <th>Discussion Period:</th>
		<td>2019-11-22</td>
		<td></td>
      </tr>
      <tr>
        <th>Voting Period:</th>
            <td>Saturday 2019-12-07 00:00:00 UTC</td>
            <td>Friday 2019-12-27 23:59:59 UTC</td>
      </tr>
    </table>

    The Project Leader has varied discussion period so that the minimum
discussion period ends on November 30. [<a
href='https://lists.debian.org/debian-vote/2019/11/msg00189.html'>mail</a>]

    <vproposerf />
    <p>Martin Michlmayr [<email tbm@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00330.html'>text of proposal</a>]
    </p>
    <vsecondsf />
    <ol>
        <li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00331.html'>mail</a>] </li>
        <li>Ansgar Burchardt [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00332.html'>mail</a>] </li>
        <li>Julien Cristau [<email jcristau@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00333.html'>mail</a>] </li>
        <li>Enrico Zini [<email enrico@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00334.html'>mail</a>] </li>
        <li>Matthias Klumpp [<email mak@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00335.html'>mail</a>] </li>
        <li>Ana Beatriz Guerrero López [<email ana@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00337.html'>mail</a>] </li>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00338.html'>mail</a>] </li>
        <li>Intrigeri [<email intrigeri@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00341.html'>mail</a>] </li>
        <li>Luca Filipozzi [<email lfilipoz@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00343.html'>mail</a>] </li>
        <li>Moritz Mühlenhoff [<email jmm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00346.html'>mail</a>] </li>
        <li>Paul Tagliamonte [<email paultag@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00347.html'>mail</a>] </li>
        <li>Jordi Mallach [<email jordi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00348.html'>mail</a>] </li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00350.html'>mail</a>] </li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00351.html'>mail</a>] </li>
        <li>Jeremy Bicha [<email jbicha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00000.html'>mail</a>] </li>
    </ol>
    <vtextf />
	<h3>Choice 1: F: Focus on systemd</h3>

<p>This resolution is a position statement under section 4.1 (5) of the
Debian constitution:</p>

<p>Cross-distribution standards and cooperation are important factors in
the choice of core Debian technologies.  It is important to recognize
that the Linux ecosystem has widely adopted systemd and that the level
of integration of systemd technologies in Linux systems will increase
with time.</p>

<p>Debian is proud to support and integrate many different technologies.
With everything we do, the costs and benefits need to be considered,
both for users and in terms of the effects on our development community.
An init system is not an isolated component, but is deeply integrated in
the core layer of the system and affects many packages.  We believe that
the benefits of supporting multiple init systems do not outweigh the
costs.</p>

<p>Debian can continue to provide and explore other init systems, but
systemd is the only officially supported init system.  Wishlist bug
reports with patches can be submitted, which package maintainers should
review like other bug reports with patches.  As with systemd, work
should be done upstream and in cooperation with other Linux and FOSS
distributions where possible.  The priority is on standardization
without the reliance on complicated compatibility layers.</p>

<p>Integrating systemd more deeply into Debian will lead to a more
integrated and tested system, improve standardization of Linux systems,
and bring many new technologies to our users.  Packages can rely upon,
and are encouraged to make full use of, functionality provided by
systemd.  Solutions based on systemd technologies will allow for more
cross-distribution cooperation.  The project will work on proposals and
coordinate transitions from Debian-specific solutions where appropriate.</p>


    <vproposerb />
    <p>Sam Hartman [<email hartmans@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00117.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00293.html'>amendment</a>]
    </p>
    <vsecondsb />
	This amendment has been submitted by the current Project Leader, and thus does not require seconding
#    <ol>
#    </ol>
    <vtextb />
	<h3>Choice 2: B: Systemd but we support exploring alternatives</h3>

<p>Using its power under Constitution section 4.1 (5), the project issues
the following statement describing our current position on Init
systems, multiple init systems, and the use of systemd facilities.  This
statement describes the position of the project at the time it is
adopted.  That position may evolve as time passes without the need to
resort to future general resolutions.  The GR process remains
available if the project needs a decision and cannot come to a
consensus.
</p>

<p>The Debian project recognizes that systemd service units are the
preferred configuration for describing how to start a daemon/service.
However, Debian remains an environment where developers and users can
explore and develop alternate init systems and alternatives to systemd
features.  Those interested in exploring such alternatives need to
provide the necessary development and packaging resources to do that
work.  Technologies such as elogind that facilitate exploring
alternatives while running software that depends on some systemd
interfaces remain important to Debian.  It is important that the
project support the efforts of developers working on such technologies
where there is overlap between these technologies and the rest of the
project, for example by reviewing patches and participating in
discussions in a timely manner.
</p>

<p>Packages should include service units or init scripts to start daemons
and services.  Packages may use any systemd facility at the
package maintainer's discretion, provided that this is consistent with
other Policy requirements and the normal expectation that packages
shouldn't depend on experimental or unsupported (in Debian) features
of other packages.  Packages may include support for alternate init
systems besides systemd and may include alternatives for any
systemd-specific interfaces they use.  Maintainers use their normal
procedures for deciding which patches to include.
</p>

<p>Debian is committed to working with derivatives that make different
choices about init systems.  As with all our interactions with
downstreams, the relevant maintainers will work with the downstreams to
figure out which changes it makes sense to fold into Debian and which
changes remain purely in the derivative.
</p>

    <vproposera />
    <p>Sam Hartman [<email hartmans@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00257.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00258.html'>amendment</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00260.html'>amendment</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00293.html'>amendment</a>]
    </p>

    <vsecondsa />
	This amendment has been submitted by the current Project Leader, and thus does not require seconding
#    <ol>
#    </ol>
    <vtexta />
	<h3>Choice 3: A: Support for multiple init systems is Important</h3>

<p>The project issues the following statement describing our current
position on Init systems, multiple init systems, and the use of
systemd facilities.  This statement describes the position of the
project at the time it is adopted.  That position may evolve as time
passes without the need to resort to future general resolutions.  The
GR process remains available if the project needs a decision and
cannot come to a consensus.
</p>

<p>Being able to run Debian systems with init systems other than systemd
continues to be something that the project values.  Every package
should work with pid1 != systemd, unless it was designed by upstream
to work exclusively with systemd and no support for running without
systemd is available.  It is an important bug (although not a serious
one) when packages should work without systemd but they do not.
According to the NMU guidelines, developers may perform non-maintainer
uploads to fix these bugs.
</p>

<p>Software is not to be considered to be designed by upstream to work
exclusively with systemd merely because upstream does not provide,
and/or will not accept, an init script.
</p>

<p>Modification of Policy to adopt systemd facilities instead of
existing approaches is discouraged unless an equivalent implementation
of that facility is available for other init systems.
</p>

    <vproposerd />
    <p>Ian Jackson [<email iwj@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00163.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00206.html'>text of amended</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00283.html'>text of amended</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00306.html'>text of amended</a>]
    </p>
    <vsecondsd />
    <ol>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00128.html'>mail</a>] </li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00133.html'>mail</a>] </li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00138.html'>mail</a>] </li>
        <li>Kyle Robbertze [<email paddatrapper@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00140.html'>mail</a>] </li>
        <li>Dmitry Bogatov [<email kaction@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00156.html'>mail</a>] </li>
        <li>Jonathan McDowell [<email noodles@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00168.html'>mail</a>] </li>
        <li>Matthew Vernon [<email matthew@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00174.html'>mail</a>] </li>
        <li>James Clarke [<email jrtc27@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00205.html'>mail</a>] </li>
        <li>Thomas Goirand [<email zigo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00247.html'>mail</a>] </li>
        <li>Jonathan Dowland [<email jmtd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00289.html'>mail</a>] </li>
    </ol>
    <vtextd />
	<h3>Choice 4: D: Support non-systemd systems, without blocking progress</h3>

<h4>PRINCIPLES</h4>

<p>1. We wish to continue to support multiple init systems for the
foreseeable future.  And we want to improve our systemd support.
</p>

<p>2. It is primarily for the communities in each software ecosystem to
maintain and develop their respective software - but with the
active support of other maintainers and gatekeepers where needed.
</p>

<h4>SYSTEMD DEPENDENCIES</h4>

<p>3. Ideally, packages should be fully functional with all init
systems.  This means (for example) that daemons should ship
traditional init scripts, or use other mechanisms to ensure that
they are started without systemd.  It also means that desktop
software should be installable, and ideally fully functional,
without systemd.
</p>

<p>4. So failing to support non-systemd systems, where no such support is
available, is a bug.  But it is <b>not</b> a release-critical bug.
Whether the requirement for systemd is recorded as a formal bug in
the Debian bug system, when no patches are available, is up to the
maintainer.
</p>

<p>5. When a package has reduced functionality without systemd, this
should not generally be documented as a (direct or indirect)
Depends or Recommends on systemd-sysv.  This is because with such
dependencies, installing such a package can attempt to switch the
init system, which is not what the user wanted.  For example, a
daemon with only a systemd unit file script should still be
installable on a non-systemd system, since it could be started
manually.
</p>

<p>One consequence of this is that on non-systemd systems it may be
possible to install software which will not work, or not work
properly, because of an undeclared dependency on systemd.  This is
unfortunate but trying to switch the user's init system is worse.
We hope that better technical approaches can be developed to
address this.
</p>

<p>6. We recognise that some maintainers find init scripts a burden and
we hope that the community is able to find ways to make it easier
to add support for non-default init systems.  Discussions about the
design of such systems should be friendly and cooperative, and if
suitable arrangements are developed they should be supported in the
usual ways within Debian.
</p>

<h4>CONTRIBUTIONS OF NON-SYSTEMD SUPPORT WILL BE ACCEPTED</h4>

<p>7. Failing to support non-systemd systems when such support is
available, or offered in the form of patches (or packages),
<b>should</b> be treated as a release critical bug.  For example: init
scripts <b>must not</b> be deleted merely because a systemd unit is
provided instead; patches which contribute support for other init
systems (with no substantial effect on systemd installations)
should be filed as bugs with severity &ldquo;serious&rdquo;.
</p>

<p>This is intended to provide a lightweight but effective path to
ensuring that reasonable support can be provided to Debian users,
even where the maintainer's priorities lie elsewhere.  (Invoking
the Technical Committee about individual patches is not sensible.)
</p>

<p>If the patches are themselves RC-buggy (in the opinion of,
initially, the maintainer, and ultimately the Release Team) then of
course the bug report should be downgraded or closed.
</p>

<p>8. Maintainers of systemd components, or other gatekeepers (including
other maintainers and the release team) sometimes have to evaluate
technical contributions intended to support non-systemd users.  The
acceptability to users of non-default init systems, of quality
risks of such contributions, is a matter for the maintainers of
non-default init systems and the surrounding community.  But such
contributions should not impose nontrivial risks on users of the
default configuration (systemd with Recommends installed).
</p>

<h4>NON-INIT-RELATED DECLARATIVE SYSTEMD FACILITIES</h4>

<p>9. systemd provides a variety of facilities besides daemon startup.
For example, creating system users or temporary directories.
Current Debian approaches are often based on debhelper scripts.
</p>

<p>
In general more declarative approaches are better.  Where
<ul>
  <li>systemd provides such facility
  <li>a specification of the facility (or suitable subset) exists
  <li>the facility is better than the other approaches available
    in Debian, for example by being more declarative
  <li>it is reasonable to expect developers of non-systemd
    systems including non-Linux systems to implement it
  <li>including consideration of the amount of work involved
 </ul>
the facility should be documented in Debian Policy (by textual
incorporation, not by reference to an external document).  The
transition should be smooth for all users.  The non-systemd
community should be given at least 6 months, preferably at least 12
months, to develop their implementation.  (The same goes for any
future enhancements.)

<p>
If policy consensus cannot be reached on such a facility, the
Technical Committee should decide based on the project's wishes as
expressed in this GR.
</p>

<h4>BEING EXCELLENT TO EACH OTHER</h4>

<p>10. In general, maintainers of competing software, including
maintainers of the various competing init systems, should be
accommodating to each others' needs.  This includes the needs and
convenience of users of reasonable non-default configurations.
</p>

<p>11. Negative general comments about software and their communities,
including both about systemd itself and about non-systemd init
systems, are strongly discouraged.  Neither messages expressing
general dislike of systemd, nor predictions of the demise of
non-systemd systems, are appropriate for Debian communication fora;
likewise references to bugs which are not relevant to the topic at
hand.
</p>

<p>Communications on Debian fora on these matters should all be
encouraging and pleasant, even when discussing technical problems.
We ask that communication fora owners strictly enforce this.
</p>

<p>12. We respectfully ask all Debian contributors including maintainers,
Policy Editors, the Release Team, the Technical Committee, and the
Project Leader, to pursue these goals and principles in their work,
and embed them into documents etc. as appropriate.
(This resolution is a position statement under s4.1(5).)
</p>


    <vproposerh />
        <p>Ian Jackson [<email iwj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00142.html'>text of proposal</a>]
    </p>
    <vsecondsh />
    <ol>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00129.html'>mail</a>] </li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00131.html'>mail</a>] </li>
        <li>Michael Lustfield [<email mtecknology@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00132.html'>mail</a>] </li>
        <li>Guilhem Moulin [<email guilhem@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00134.html'>mail</a>] </li>
        <li>Matthew Vernon [<email matthew@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00137.html'>mail</a>] </li>
        <li>Kyle Robbertze [<email paddatrapper@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00139.html'>mail</a>] </li>
        <li>gregor herrmann [<email gregoa@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00156.html'>mail</a>] </li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00163.html'>mail</a>] </li>
    </ol>
    <vtexth />
	<h3>Choice 5: H: Support portability, without blocking progress</h3>

<h4>PRINCIPLES</h4>

<p>1. The Debian project reaffirms its commitment to be the glue that binds
and integrates different software that provides similar or equivalent
functionality, with their various users, be them humans or other software,
which is one of the core defining traits of a distribution.</p>

<p>2. We consider portability to different hardware platforms and software
stacks an important aspect of the work we do as a distribution, which
makes software architecturally better, more robust and more future-proof.</p>

<p>3. We acknowledge that different upstream projects have different views on
software development, use cases, portability and technology in general.
And that users of these projects weight tradeoffs differently, and have
at the same time different and valid requirements and/or needs fulfilled
by these diverse views.</p>

<p>4. Following our historic tradition, we will welcome the integration of
these diverse technologies which might sometimes have conflicting
world-views, to allow them to coexist in harmony within our distribution,
by reconciling these conflicts via technical means, as long as there
are people willing to put in the effort.</p>

<p>5. This enables us to keep serving a wide range of usages of our distribution
(some of which might be even unforeseen by us). From servers, to desktops
or deeply embedded; from general purpose to very specifically tailored
usages. Be those projects hardware related or software based, libraries,
daemons, entire desktop environments, or other parts of the software
stack.</p>

<h4>SYSTEMD DEPENDENCIES</h4>

<p>6. Ideally, packages should be fully functional with all init
systems.  This means (for example) that daemons should ship
traditional init scripts, or use other mechanisms to ensure that
they are started without systemd.  It also means that desktop
software should be installable, and ideally fully functional,
without systemd.
</p>

<p>7. So failing to support non-systemd systems, where no such support is
available, is a bug.  But it is <b>not</b> a release-critical bug.
Whether the requirement for systemd is recorded as a formal bug in
the Debian bug system, when no patches are available, is up to the
maintainer.
</p>

<p>8. When a package has reduced functionality without systemd, this
should not generally be documented as a (direct or indirect)
Depends or Recommends on systemd-sysv.  This is because with such
dependencies, installing such a package can attempt to switch the
init system, which is not what the user wanted.  For example, a
daemon with only a systemd unit file script should still be
installable on a non-systemd system, since it could be started
manually.
</p>

<p>One consequence of this is that on non-systemd systems it may be
possible to install software which will not work, or not work
properly, because of an undeclared dependency on systemd.  This is
unfortunate but trying to switch the user's init system is worse.
We hope that better technical approaches can be developed to
address this.
</p>

<p>9. We recognise that some maintainers find init scripts a burden and
we hope that the community is able to find ways to make it easier
to add support for non-default init systems.  Discussions about the
design of such systems should be friendly and cooperative, and if
suitable arrangements are developed they should be supported in the
usual ways within Debian.
</p>

<h4>CONTRIBUTIONS OF NON-SYSTEMD SUPPORT WILL BE ACCEPTED</h4>

<p>10. Failing to support non-systemd systems when such support is
available, or offered in the form of patches (or packages),
<b>should</b> be treated as a release critical bug.  For example: init
scripts <b>must not</b> be deleted merely because a systemd unit is
provided instead; patches which contribute support for other init
systems (with no substantial effect on systemd installations)
should be filed as bugs with severity &ldquo;serious&rdquo;.
</p>

<p>This is intended to provide a lightweight but effective path to
ensuring that reasonable support can be provided to Debian users,
even where the maintainer's priorities lie elsewhere.  (Invoking
the Technical Committee about individual patches is not sensible.)
</p>

<p>If the patches are themselves RC-buggy (in the opinion of,
initially, the maintainer, and ultimately the Release Team) then of
course the bug report should be downgraded or closed.
</p>

<p>11. Maintainers of systemd components, or other gatekeepers (including
other maintainers and the release team) sometimes have to evaluate
technical contributions intended to support non-systemd users.  The
acceptability to users of non-default init systems, of quality
risks of such contributions, is a matter for the maintainers of
non-default init systems and the surrounding community.  But such
contributions should not impose nontrivial risks on users of the
default configuration (systemd with Recommends installed).
</p>

<h4>NON-INIT-RELATED DECLARATIVE SYSTEMD FACILITIES</h4>

<p>12. systemd provides a variety of facilities besides daemon startup.
For example, creating system users or temporary directories.
Current Debian approaches are often based on debhelper scripts.
</p>

<p>
In general more declarative approaches are better.  Where
<ul>
  <li>systemd provides such facility
  <li>a specification of the facility (or suitable subset) exists
  <li>the facility is better than the other approaches available
    in Debian, for example by being more declarative
  <li>it is reasonable to expect developers of non-systemd
    systems including non-Linux systems to implement it
  <li>including consideration of the amount of work involved
 </ul>
the facility should be documented in Debian Policy (by textual
incorporation, not by reference to an external document).  The
transition should be smooth for all users.  The non-systemd
community should be given at least 6 months, preferably at least 12
months, to develop their implementation.  (The same goes for any
future enhancements.)

<p>
If policy consensus cannot be reached on such a facility, the
Technical Committee should decide based on the project's wishes as
expressed in this GR.
</p>

<h4>BEING EXCELLENT TO EACH OTHER</h4>

<p>13. In general, maintainers of competing software, including
maintainers of the various competing init systems, should be
accommodating to each others' needs.  This includes the needs and
convenience of users of reasonable non-default configurations.
</p>

<p>14. Negative general comments about software and their communities,
including both about systemd itself and about non-systemd init
systems, are strongly discouraged.  Neither messages expressing
general dislike of systemd, nor predictions of the demise of
non-systemd systems, are appropriate for Debian communication fora;
likewise references to bugs which are not relevant to the topic at
hand.
</p>

<p>Communications on Debian fora on these matters should all be
encouraging and pleasant, even when discussing technical problems.
We ask that communication fora owners strictly enforce this.
</p>

<p>15. We respectfully ask all Debian contributors including maintainers,
Policy Editors, the Release Team, the Technical Committee, and the
Project Leader, to pursue these goals and principles in their work,
and embed them into documents etc. as appropriate.
(This resolution is a position statement under s4.1(5).)
</p>

    <vproposere />
    <p>Dmitry Bogatov [<email kaction@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00202.html'>text of latest proposal</a>]
    </p>
    <vsecondse />
    <ol>
        <li>Ian Jackson [<email iwj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00193.html'>mail</a>] </li>
        <li>Matthew Vernon [<email matthew@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00194.html'>mail</a>] </li>
        <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00195.html'>mail</a>] </li>
        <li>Kyle Robbertze [<email paddatrapper@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00196.html'>mail</a>] </li>
        <li>Axel Beckert [<email abe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00197.html'>mail</a>] </li>
        <li>Brian Gupta [<email bgupta@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00234.html'>mail</a>] </li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00238.html'>mail</a>] </li>
    </ol>
    <vtexte />
	<h3>Choice 6: E: Support for multiple init systems is Required</h3>

<p>Being able to run Debian systems with init systems other than systemd
continues to be of value to the project. Every package MUST work with
pid1 != systemd, unless it was designed by upstream to work exclusively
with systemd and no support for running without systemd is available.
</p>

<p>Software is not to be considered to be designed by upstream to work
exclusively with systemd merely because upstream does not provide,
and/or will not accept, an init script.
</p>

    <vproposerg />
    <p> Guillem Jover [<email guillem@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/12/msg00176.html'>text of proposal</a>]
    </p>
    <vsecondsg />
    <ol>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00362.html'>mail</a>] </li>
        <li>gregor herrmann [<email gregoa@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00363.html'>mail</a>] </li>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00364.html'>mail</a>] </li>
        <li>Guilhem Moulin [<email guilhem@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00365.html'>mail</a>] </li>
        <li>Ricardo Mones [<email mones@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00366.html'>mail</a>] </li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00367.html'>mail</a>] </li>
        <li>Steve Kostecke [<email steve@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00368.html'>mail</a>] </li>
        <li>Alberto Gonzalez Iniesta [<email agi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00375.html'>mail</a>] </li>
        <li>Kyle Robbertze [<email paddatrapper@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00179.html'>mail</a>] </li>
        <li>Adam Borowski [<email kilobyte@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00180.html'>mail</a>] </li>
        <li>Donald Scott Kitterman [<email kitterman@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/12/msg00181.html'>mail</a>] </li>
    </ol>
    <vtextg />
	<h3>Choice 7: G: Support portability and multiple implementations</h3>

<h4>Principles</h4>

<p>The Debian project reaffirms its commitment to be the glue that binds
and integrates different software that provides similar or equivalent
functionality, with their various users, be them humans or other software,
which is one of the core defining traits of a distribution.</p>

<p>We consider portability to different hardware platforms and software
stacks an important aspect of the work we do as a distribution, which
makes software architecturally better, more robust and more future-proof.</p>

<p>We acknowledge that different upstream projects have different views on
software development, use cases, portability and technology in general.
And that users of these projects weight trade-offs differently, and have
at the same time different and valid requirements and/or needs fulfilled
by these diverse views.</p>

<p>Following our historic tradition, we will welcome the integration of
these diverse technologies which might sometimes have conflicting
world-views, to allow them to coexist in harmony within our distribution,
by reconciling these conflicts via technical means, as long as there
are people willing to put in the effort.</p>

<p>This enables us to keep serving a wide range of usages of our distribution
(some of which might be even unforeseen by us). From servers, to desktops
or deeply embedded; from general purpose to very specifically tailored
usages. Be those projects hardware related or software based, libraries,
daemons, entire desktop environments, or other parts of the software
stack.</p>

<h4>Guidance</h4>

<p>In the same way Debian maintainers are somewhat constrained by the
decisions and direction emerging from their respective upstreams,
the Debian distribution is also somewhat constrained by its volunteer
based nature, which has as another of its core defining traits, not
willing to impose work obligations to its members, while at the same
time being limited by its members bounded interests, motivation, energy
and time.</p>

<p>Because of these previous constraints, trying to provide guidance in
a very detailed way to apply the aforementioned principles, is never
going to be satisfactory, as it will end up being inexorably a rigid
and non-exhaustive list of directives that cannot possibly ever cover
most scenarios, which can perpetuate possible current tensions.</p>

<p>These will always keep involving case by case trade-offs between what
changes or requests upstreams might or might not accept, or the upkeep
on the imposed deltas or implementations the Debian members might need
to carry on. Which can never be quantified and listed in a generic and
universal way.</p>

<p>We will also keep in mind that what might be considered important for
someone, might at the same time be considered niche or an uninteresting
diversion of time for someone else, but that we might end up being on
either side of the fence when sending or receiving these requests.</p>

<p>We will be guided, as we have been in many other Debian contexts in the
past, by taking all the above into account, and discussing and evaluating
each situation, and respecting and valuing that we all have different
interests and motivations. That is in our general interest to try to
work things out with others, to compromise, to reach solutions or find
alternatives that might be satisfactory enough for the various parties
involved, to create an environment where we will collectively try to
reciprocate. And in the end and in most cases it's perhaps a matter of
just being willing to try.</p>


    <vproposerc />
    <p>Sam Hartman [<email hartmans@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00117.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00293.html'>amendment</a>]
	[<a href='https://lists.debian.org/debian-vote/2019/11/msg00381.html'>withdrawal</a>]
    </p>
    <vsecondsc />
	This amendment has been submitted by the current Project Leader, and thus does not require seconding
    <ol>
        <li>Ansgar Burchardt [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/11/msg00253.html'>mail</a>] </li>
    </ol>
    <vtextc />
	<h3>Focus on systemd for init system and other facilities (withdrawn)</h3>



    <vquorum />
     <p>
        With the current list of <a href="vote_002_quorum.log">voting
          developers</a>, we have:
     </p>
    <pre>
#include 'vote_002_quorum.txt'
    </pre>
#include 'vote_002_quorum.src'



    <vstatistics />
    <p>
	For this GR, like always,
#                <a href="https://vote.debian.org/~secretary/gr_initsystems/">statistics</a>
               <a href="suppl_002_stats">statistics</a>
             will be gathered about ballots received and
             acknowledgements sent periodically during the voting
             period.
#                Additionally, the list of voters will be
#             recorded. Also, the tally
#             sheet will also be made available to be viewed.
               Additionally, the list of <a
             href="vote_002_voters.txt">voters</a> will be
             recorded. Also, the <a href="vote_002_tally.txt">tally
             sheet</a> will also be made available to be viewed.
         </p>

    <vmajorityreq />
    <p>
      The proposals need a simple majority
    </p>
#include 'vote_002_majority.src'

    <voutcome />
#include 'vote_002_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

