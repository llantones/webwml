<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Sympa, a mailing list
manager, which could result in local privilege escalation, denial of
service or unauthorized access via the SOAP API.</p>

<p>Additionally to mitigate <a href="https://security-tracker.debian.org/tracker/CVE-2020-26880">\
CVE-2020-26880</a> the sympa_newaliases-wrapper is no longer installed
setuid root by default. A new Debconf question is introduced to allow
setuid installations in setups where it is needed.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 6.2.40~dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your sympa packages.</p>

<p>For the detailed security status of sympa please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/sympa">\
https://security-tracker.debian.org/tracker/sympa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4818.data"
# $Id: $
