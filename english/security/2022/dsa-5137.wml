<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Jakub Wilk discovered a local privilege escalation in needrestart, a
utility to check which daemons need to be restarted after library
upgrades. Regular expressions to detect the Perl, Python, and Ruby
interpreters are not anchored, allowing a local user to escalate
privileges when needrestart tries to detect if interpreters are using
old source files.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 3.4-5+deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 3.5-4+deb11u1.</p>

<p>We recommend that you upgrade your needrestart packages.</p>

<p>For the detailed security status of needrestart please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/needrestart">https://security-tracker.debian.org/tracker/needrestart</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5137.data"
# $Id: $
