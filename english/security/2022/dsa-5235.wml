<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

    <p>Yehuda Afek, Anat Bremler-Barr and Shani Stajnrod discovered that a
    flaw in the resolver code can cause named to spend excessive amounts
    of time on processing large delegations, significantly degrade
    resolver performance and result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3080">CVE-2022-3080</a>

    <p>Maksym Odinintsev discovered that the resolver can crash when stale
    cache and stale answers are enabled with a zero
    stale-answer-timeout. A remote attacker can take advantage of this
    flaw to cause a denial of service (daemon crash) via specially
    crafted queries to the resolver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

    <p>It was discovered that the DNSSEC verification code for the ECDSA
    algorithm is susceptible to a memory leak flaw. A remote attacker
    can take advantage of this flaw to cause BIND to consume resources,
    resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

    <p>It was discovered that the DNSSEC verification code for the EdDSA
    algorithm is susceptible to a memory leak flaw. A remote attacker
    can take advantage of this flaw to cause BIND to consume resources,
    resulting in a denial of service.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1:9.16.33-1~deb11u1.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5235.data"
# $Id: $
