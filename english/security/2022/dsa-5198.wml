<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in Jetty, a Java servlet engine
and webserver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2047">CVE-2022-2047</a>

    <p>In Eclipse Jetty the parsing of the authority segment of an http scheme
    URI, the Jetty HttpURI class improperly detects an invalid input as a
    hostname. This can lead to failures in a Proxy scenario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2048">CVE-2022-2048</a>

    <p>In Eclipse Jetty HTTP/2 server implementation, when encountering an invalid
    HTTP/2 request, the error handling has a bug that can wind up not properly
    cleaning up the active connections and associated resources. This can lead
    to a Denial of Service scenario where there are no enough resources left to
    process good requests.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 9.4.39-3+deb11u1.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5198.data"
# $Id: $
