msgid ""
msgstr ""
"Project-Id-Version: debian-id\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2011-02-15 17:23+0000\n"
"PO-Revision-Date: 2011-02-22 10:08+0700\n"
"Last-Translator: Mahyuddin Susanto <udienz@ubuntu.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/join/nm-steps.inc:7
#, fuzzy
msgid "New Members Corner"
msgstr "Pojok Pengelola Baru"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Langkah 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Langkah 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Langkah 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Langkah 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Langkah 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Langkah 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Langkah 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Daftar pembanding pendaftar"

#: ../../english/devel/website/tc.data:11
#, fuzzy
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Lihat <a href=\"m4_HOME/intl/french/index.fr.html\">https://www.debian.org/"
"intl/french/index.fr.html</a> (hanya tersedia dalam bahasa perancis) untuk "
"informasi lebih lanjut."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Informasi lebih lanjut"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Lihat <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/"
"</a> (hanya tersedia dalam bahasa spanyol) untuk informasi lebih lanjut."

#: ../../english/distrib/pre-installed.defs:18
#, fuzzy
msgid "Phone"
msgstr "Telepon:"

#: ../../english/distrib/pre-installed.defs:19
#, fuzzy
msgid "Fax"
msgstr "Faks:"

#: ../../english/distrib/pre-installed.defs:21
#, fuzzy
msgid "Address"
msgstr "Alamat :"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produk"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Kaos"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "topi"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "stiker"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "gelas"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "baju lainnya"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "baju polo"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbees"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "landasan tetikus"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "badge"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "basketball goals"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "earrings"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "suitcases"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr ""

#: ../../english/events/merchandise.def:56
#, fuzzy
msgid "pillowcases"
msgstr "suitcases"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Dengan&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Tanpa&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Digerakkan dengan Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Digerakkan dengan Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Penggerak Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (tombol mini)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "sama dengan atas"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

#~ msgid "Working"
#~ msgstr "Sedang bekerja"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (rusak)"

#~ msgid "Booting"
#~ msgstr "Booting"

#~ msgid "Building"
#~ msgstr "Membangun"

#~ msgid "Not yet"
#~ msgstr "Belum ada"

#~ msgid "No kernel"
#~ msgstr "Tidak ada kernel"

#~ msgid "No images"
#~ msgstr "Tidak ada citra"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "Tidak diketahui"

#~ msgid "Unavailable"
#~ msgstr "Tidak tersedia"

#~ msgid "Download"
#~ msgstr "Unduh"

#~ msgid "Old banner ads"
#~ msgstr "Ads banner lama"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "BAD"
#~ msgstr "Jelek"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD?"
#~ msgstr "JELEK?"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "Unknown"
#~ msgstr "Tidak Diketahui"

#~ msgid "ALL"
#~ msgstr "SEMUA"

#~ msgid "Package"
#~ msgstr "Paket"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Version"
#~ msgstr "Versi"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Wanted:"
#~ msgstr "Dicari:"

#~ msgid "Who:"
#~ msgstr "Siapa?"

#~ msgid "Architecture:"
#~ msgstr "Arsitektur:"

#~ msgid "Specifications:"
#~ msgstr "Spesifikasi"

#~ msgid "Where:"
#~ msgstr "Dimana:"
