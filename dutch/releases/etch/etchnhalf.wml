#use wml::debian::template title="Debian GNU/Linux 4.0 -- Etch en een half Notities bij de release" BARETITLE=true
#include "$(ENGLISHDIR)/releases/etch/release.data"
#use wml::debian::translation-check translation="5011f532637dc7820b79b151eecfda4ab65aa22f"

<if-etchnhalf-released released="no">
<h1>Plaatsbeklederpagina</h1>

<p>
De inhoud zal worden onthuld wanneer Debian GNU/Linux <q>etch en een half</q>
uitgebracht werd.
</p>
</if-etchnhalf-released>

<if-etchnhalf-released released="yes">

<h2>Overzicht</h2>

<p>
Er werden extra pakketten toegevoegd in de tussenrelease Debian 4.0r4 om het
geheel van hardware dat wordt ondersteund door Debian 4.0 (<q>etch</q>) uit te
breiden. Dit omvat pakketten die gebaseerd zijn op de Linux 2.6.24-kernel en
extra stuurprogramma's voor het X-windowsysteem. Installatie van deze
aanvullende pakketten is niet vereist en zal niet standaard plaatsvinden. Deze
update betekent geen verandering in de ondersteuning van eerder beschikbare
pakketten.
</p>

<p>
De bestaande op 2.6.18 gebaseerde kernel zal de standaardkernel blijven voor de
release van etch.
</p>

<h2>Nieuwe beschikbare pakketten</h2>

<ul>
  <li><a href="https://packages.debian.org/src:linux-2.6.24">linux-2.6.24</a><br />
  Een nieuw linux-image en ondersteunende pakketten zijn toegevoegd om te
  kunnen genieten van veel nieuwe en bijgewerkte apparaatstuurprogramma's. Zie
  het gedeelte <q>Beperkingen</q> van dit document voor meer informatie.</li>
  <li><a href="https://packages.debian.org/etch/xserver-xorg-video-intel">xserver-xorg-video-intel</a><br />
  Een nieuw X-stuurprogramma werd toegevoegd om ondersteuning te bieden voor de
  apparaten 965GM, 965GME, G33, Q35, Q33 en GM45 (PCI-id's 0x2A02, 0x2A12,
  0x29B2, 0x29C2, 0x29D2 en 0x2A42). Sommige kaarten die vroeger ondersteund
  werden door het stuurprogramma xserver-xorg-video-i810 zullen mogelijk beter
  presteren met het stuurprogramma xserver-xorg-video-intel. Dit betreft onder
  meer de apparaten 945GM, 946GZ, 965G en 965Q.</li>
</ul>

<h2>Opgewaardeerde <q>etch</q>-pakketten</h2>

<p>
Sommige bestaande etch-pakketten zijn bijgewerkt om ondersteuning voor nieuwe
hardware toe te voegen en voor compatibiliteit met de 2.6.24-kernel:
</p>

<ul>
 <li><a href="https://packages.debian.org/etch/aboot">aboot</a><br />
 Ondersteuning voor het opstarten van recente kernels.</li>
 <li><a href="https://packages.debian.org/etch/sysvinit">sysvinit</a><br />
 Bijgewerkt commando shutdown om schijven correct af te sluiten.</li>
 <li><a href="https://packages.debian.org/etch/wireless-tools">wireless-tools</a><br />
 Onnauwkeurige incompatibiliteitswaarschuwing verwijderd.</li>
 <li><a href="https://packages.debian.org/source/etch/firmware-nonfree">firmware-nonfree</a><br />
 Firmware-blobs voor gebruik met de 2.6.24-kernel toegevoegd.</li>
  <li><a href="https://packages.debian.org/etch/xserver-xorg-video-nv">xserver-xorg-video-nv</a><br />
  Een bijgewerkt X-stuurprogramma is toegevoegd om ondersteuning toe te voegen
  voor voorheen niet-ondersteunde NVIDIA grafische kaarten (met name de serie
  GeFORCE 8). Het bijgewerkte xserver-xorg-video-nv pakket biedt ondersteuning
  voor de kaarten NVIDIA Riva, TNT, GeFORCE en Quadro.</li>
</ul>

<h2>De release <q>Etch en een half</q> installeren</h2>

<p>
Vanaf deze release zijn er nu twee methoden voor het installeren van de release
4.0 (<q>etch</q>) van Debian.</p>

<ol>
  <li><strong>Installeren met het installatiesysteem van Debian 4.0 (<q>etch</q>)</strong><br />
  Als uw hardware voldoende wordt ondersteund door het standaard
  installatiesysteem van etch, kunt u ervoor kiezen om standaard
  installatiemedia voor etch te gebruiken om uw systeem in eerste instantie te
  installeren en later over te schakelen op de kernel van
  <q>etch en een half</q>. Deze optie is het beste voor gebruikers die al een
  systeem hebben geïnstalleerd met etch of die geen nieuwe installatiemedia
  willen aanschaffen, maar extra hardware willen toevoegen die alleen wordt
  ondersteund door de kernel van <q>etch en een half</q>.<br />
  Informatie over het verkrijgen en het gebruik van het installatiesysteem van
  Debian 4.0 (<q>etch</q>) is <a href="debian-installer/">hier</a> te vinden.
 </li>
  <li><strong>Installeren met het huidige installatiesysteem van Debian <q>lenny</q></strong><br />
  Vanaf Beta 2 bevat het installatiesysteem van Lenny een installatiekernel die
  erg lijkt op de kernel van <q>etch en een half</q> en een
  compatibiliteitsmodus heeft voor het installeren van de etch-release. Deze
  optie is het beste voor gebruikers met hardware die wordt ondersteund door de
  kernel van <q>etch en een half</q>, maar niet wordt ondersteund door het
  standaard installatiesysteem van etch. Informatie over het verkrijgen en het
  gebruik van het op <q>lenny</q> gebaseerde installatiesysteem om
  <q>etch en een half</q> te installeren is
  <a href="debian-installer/etchnhalf">hier</a> te vinden.</li>
</ol>

<h2>Beperkingen</h2>

<ul>
  <li>Debian garandeert niet dat alle hardware die wordt ondersteund door de
  standaard kernel 2.6.18 in etch ook wordt ondersteund door de 2.6.24-kernel,
  noch dat alle software in etch correct zal werken met de nieuwere kernel.</li>
  <li>Overstappen van de kernel 2.6.18 van etch naar de kernel 2.6.24 van
  <q>etch en een half</q> zal in veel gevallen werken, maar er is geen garantie
  voor succes. Opwaarderingen vanaf de kernels 2.6.18 en 2.6.24 naar de kernel
  die met de volgende stabiele release (<q>lenny</q>) geleverd wordt, zullen
  beide ondersteund worden.</li>
  <li>Niet alle functionaliteit van de kernel 2.6.18 van etch is beschikbaar in
  de images van 2.6.24, waaronder de Xen en de linux virtuele servervarianten.</li>
  <li>De kernelmodulepakketten die in etch buiten de broncodeboom werden
  geleverd, werken niet gegarandeerd correct met de 2.6.24-kernel.</li>
</ul>

<h2>Hardware-specifieke aantekeningen</h2>
<ul>
  <li><strong>Broadcom NetXtreme II netwerkkaarten</strong><br />
  Het bnx2-stuurprogramma voor de Broadcom NetXtreme II netwerkkaarten werd
  aangepast om firmwarebestanden te laden vanuit het bestandssysteem. Deze
  firmwarebestanden worden verstrekt in het pakket
  <a href="https://packages.debian.org/etch/firmware-bnx2">firmware-bnx2</a>
  in de sectie non-free van het archief. Systemen die het
  bnx2-stuurprogramma nodig hebben, moeten het pakket firmware-bnx2 installeren
  om te functioneren met kernel 2.6.24 van <q>etch en een half</q>. Deze
  apparaten zullen niet beschikbaar zijn op het moment van installatie wanneer
  het op Debian <q>lenny</q> gebaseerde installatiesysteem gebruikt wordt.</li>
  <li><strong>Intel 3945/4965 draadloze netwerkkaarten</strong><br />
  Systemen die een op Intel 3945 of 4965 gebaseerde draadloze netwerkadapter
  gebruiken moeten de overeenkomstige firmwarebestanden voor deze adapters
  installeren om te functioneren met kernel 2.6.24 van <q>etch en een half</q>.
  Deze firmwarebestanden worden verstrekt in het pakket
  <a href="https://packages.debian.org/etch/firmware-iwlwifi">firmware-iwlwifi</a>
  in de sectie non-free van het archief. Voor informatie over het overschakelen
  van het ipw3945-stuurprogramma naar het iwl3945-stuurprogramma moet u de
  <a href="https://wiki.debian.org/iwlwifi">omschakelingsinformatie</a> erop
  nalezen.</li>
  <li><strong>De <q>Sound Fusion</q> geluidskaarten van Cirrus Logic</strong><br />
  Het stuurprogramma snd-cs46xx is niet langer beschikbaar in de kernel van
  <q>etch en een half</q> als gevolg van juridische kwesties.</li>
  <li><strong>ARM ip32x (Thecus N2100, IO-Data GLAN Tank)</strong>
  <ul>
    <li>Een probleem in het netwerkstuurprogramma r8169, dat NFS-problemen zou
    veroorzaken op de Thecus N2100 (zie bug
    <a href="https://bugs.debian.org/452069">#452069</a>), werd opgelost.</li>
    <li>De Thecus N2100 sluit nu correct af.</li>
    <li>Ondersteuning voor de sensorchip voor het controleren van de ventilator
    is nu dankzij Riku Voipio toegevoegd.</li>
  </ul>
  </li>
  <li><strong>ARM ixp4xx (Linksys NSLU2)</strong>
    <ul>
      <li>Een nieuw Ethernet-stuurprogramma, geschreven door Krzysztof Halasa,
      werd toegevoegd. Dit stuurprogramma is opgenomen in de hoofdversie van de
      kernel, terwijl het stuurprogramma dat in de oude kernel voor etch
      (2.6.18) werd gebruikt niet langer wordt onderhouden.</li>
      <li>Een crash van de kernel met gebitmapte md-apparatuur werd gerepareerd
      (zie bug
      <a href="https://bugs.debian.org/443373">#443373</a>).</li>
      <li>Meer modules werden geactiveerd, bijvoorbeeld meer
      netfilter-modules.</li>
    </ul>
  </li>
  <li><strong>Op SRM gebaseerde Alpha-systemen</strong><br />
  aboot, het opstartprogramma voor alpha-machines die SRM-firmware gebruiken,
  werd bijgewerkt om het opstarten van recente linux kernel-images te
  ondersteunen. Indien u op uw systeem een nieuwe installatie van etch uitvoert
  met het op Debian <q>lenny</q> gebaseerde installatiesysteem, zal automatisch
  een bijgewerkt aboot-pakket gebruikt worden. Om een bestaand etch-systeem om
  te schakelen naar kernel 2.6.24 van <q>etch en een half</q>, moet u in etch
  opwaarderen naar het recentste aboot-pakket en handmatig het opstartblok op
  uw schijf bijwerken met het commando swriteboot. Zie swriteboot(8) voor meer
  informatie.</li>
</ul>

</if-etchnhalf-released>
