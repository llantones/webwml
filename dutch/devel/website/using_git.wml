#use wml::debian::template title="Git gebruiken om aan de website van Debian te werken" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="68ac2b55da5ec0c0222f324e928679e27e5de908"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Werken op de Git-opslagplaats</a></li>
<li><a href="#write-access">Schrijftoegang tot de Git-opslagplaats</a></li>
<li><a href="#notifications">Meldingen ontvangen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a> is een
<a href="https://en.wikipedia.org/wiki/Version_control">versiebeheersysteem</a>
dat helpt het werk van meerdere ontwikkelaars te coördineren. Elke gebruiker
kan een lokale kopie bewaren van een hoofdopslagplaats. De lokale kopieën kunnen
op dezelfde machine staan of aan de andere kant van de wereld. Ontwikkelaars
kunnen dan de lokale kopie aanpassen en hun wijzigingen terug laten opnemen in
de hoofdopslagplaats als ze klaar zijn.</p>
</aside>

<h2><a id="work-on-repository">Werken op de Git-opslagplaats</a></h2>

<p>
Laten we er meteen aan beginnen — in dit deel leert u hoe u een lokale kopie van
de hoofdopslagplaats kunt maken, hoe u die kopie up-to-date kunt houden en hoe
u uw werk kunt indienen.
We zullen ook uitleggen hoe u aan vertalingen kunt werken.
</p>

<h3><a name="get-local-repo-copy">Een lokale kopie ophalen</a></h3>

<p>
Eerst moet u Git installeren. Daarna moet u Git configureren en uw naam en
e-mailadres opgeven. Indien u een nieuwe gebruiker van Git bent, is het
wellicht een goed idee om eerst de algemene documentatie over Git te lezen.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Documentatie over Git</a></button></p>

<p>
Uw volgende stap is de opslagplaats klonen (met andere woorden: er een
lokale kopie van maken).
Er zijn twee manieren om dit te doen:
</p>

<ul>
  <li>Een account aanmaken op <url https://salsa.debian.org/> en SSH-toegang
  mogelijk maken door een openbare SSH-sleutel naar uw Salsa-account te
  uploaden. Raadpleeg de <a
  href="https://salsa.debian.org/help/user/ssh.md">Hulppagina's over Salsa</a>
  voor meer details. Daarna kunt u de
  <code>webwml</code>-opslagplaats klonen met het volgende commando:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>Een andere mogelijkheid is de opslagplaats te klonen met het
  HTTPS-protocol. Houd er rekening mee dat dit de lokale kopie van de
  opslagplaats wel zal aanmaken, maar u zult dan lokale wijzigingen
  niet rechtstreeks kunnen laten opnemen in de hoofdopslagplaats:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Tip:</strong> Om de volledige <code>webwml</code>-opslagplaats te
klonen, moet u ongeveer 1,3 GB data downloaden, wat te veel zou kunnen zijn
als u een langzame, onstabiele internetverbinding gebruiktt. Daarom is het
mogelijk een minimale diepte te specificeren voor een kleinere initiële
download:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Nadat een bruikbare (ondiepe) kopie van de opslagplaats is verkregen,
kunt u de diepte ervan vergroten en dit uiteindelijk omzetten naar een
volledige lokale opslagplaats:</p>

<pre>
  git fetch --deepen=1000 # de opslagplaats verdiepen met nog eens 1.000 vastleggingen
  git fetch --unshallow   # alle ontbrekende vastleggingen ophalen en de opslagplaats daarmee vervolledigen
</pre>

<p>U kunt ook een kopie maken van slechts een gedeelte van de pagina's:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Maak het bestand <code>.git/info/sparse-checkout</code> aan binnen de map
  <code>webwml</code> om te definiëren van welke inhoud u een kopie wilt maken.
  Bijvoorbeeld, indien u enkel de basisbestanden en de Engelse, Franse en
  Nederlandse vertaling wilt ophalen, zal het bestand er zo uitzien:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /french/
      /dutch/
    </pre></li>
  <li>Tenslotte kunt u nu de opslagplaats kopiëren: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Lokale wijzigingen indienen</a></h3>

<h4><a name="keep-local-repo-up-to-date">Uw lokale kopie van de opslagplaats up-to-date houden</a></h4>

<p>Om de paar dagen (en zeker voordat u met bewerken begint!) moet u het
volgende commando uitvoeren:</p>

<pre>
  git pull
</pre>

<p>om alle gewijzigde bestanden op te halen uit de opslagplaats.</p>

<p>
Het wordt sterk aanbevolen om uw lokale Git-werkmap schoon te houden voordat u
het commando <code>git pull</code> uitvoert en begint met het bewerken van
bestanden. Als u nog niet-vastgelegde wijzigingen heeft of lokale vastleggingen
die niet aanwezig zijn in de huidige tak in de externe opslagplaats, zal het
uitvoeren van het commando <code>git pull</code> automatisch
samenvoegingsconflicten creëren of zelfs mislukken ten gevolge van conflicten.
Denk eraan om uw onvoltooide werk in een aparte tak te houden of commando's te
gebruiken zoals <code>git stash</code>.
</p>

<p>Opmerking: Git is een gedistribueerd (en geen gecentraliseerd)
versiebeheersysteem. Dit betekent dat wanneer u wijzigingen vastlegt, deze
enkel in uw lokale opslagplaats opgeslagen worden. Om deze met
anderen te delen, zult u uw wijzigingen ook moeten laten opnemen in de centrale
opslagplaats op Salsa.</p>

<h4><a name="example-edit-english-file">Voorbeeld: enkele bestanden bewerken</a></h4>

<p>
Laten we eens kijken naar een meer praktisch voorbeeld en een typische
bewerkingssessie. We gaan ervan uit dat u een
<a href="#get-local-repo-copy">lokale kopie</a> van de opslagplaats verkregen
heeft met het commando <code>git clone</code>. De volgende stappen zijn dan:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Nu kunt u met bewerken beginnen en de bestanden aanpassen.</li>
  <li>Als u klaar bent, leg dan uw wijzigingen vast in uw lokale
  opslagplaats:
    <pre>
    git add pad/naar/bestand(en)
    git commit -m "Uw vastleggingsbericht"
    </pre></li>
  <li>Als u <a href="#write-access">onbeperkte schrijftoegang</a> heeft tot de
  externe <code>webwml</code>-opslagplaats, kunt u uw wijzigingen nu
  rechtstreeks laten opnemen in de Salsa-opslagplaats: <code>git push</code></li>
  <li>Als u geen directe schrijftoegang heeft tot de
  <code>webwml</code>-opslagplaats, dien dan uw wijzigingen in met een
  <a href="#write-access-via-merge-request">samenvoegingsverzoek</a>
  (merge request of vraag andere ontwikkelaars om
  hulp.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Git-documentatie</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Bugs in Debian sluiten met Git-vastleggingen</a></h4>

<p>
Als u bij uw vastlegging in het logboekbericht
<code>Closes: #</code><var>nnnnnn</var> opneemt, dan zal bugnummer
<code>#</code><var>nnnnnn</var> automatisch gesloten worden wanneer u uw
wijzigingen laat opnemen. De precieze vorm ervan is dezelfde als
die welke vermeld wordt <a href="$(DOC)/debian-policy/ch-source.html#id24">in
de Debian beleidsrichtlijnen</a>.</p>

<h4><a name="links-using-http-https">Linken met gebruik van HTTP/HTTPS</a></h4>

<p>Veel Debian-websites ondersteunen SSL/TLS, gebruik dus HTTPS-links waar dat
mogelijk en verstandig is. <strong>Echter</strong>, sommige
Debian, DebConf, SPI, enz. websites hebben ofwel geen ondersteuning voor HTTPS,
of maken alleen gebruik van de CA van SPI (en geen SSL-CA die door alle browsers
wordt vertrouwd). Om te vermijden dat niet-Debian-gebruikers foutmeldingen
krijgen, gelieve niet naar die sites te linken met gebruik van HTTPS.</p>

<p>De git-opslagplaats weigert vastleggingen die gewone HTTP-links bevatten naar
Debian-websites die HTTPS ondersteunen of die HTTPS-links bevatten naar de
websites van Debian, DebConf en SPI waarvan bekend is dat ze geen HTTPS
ondersteunen, of certificaten gebruiken die alleen zijn ondertekend door SPI.</p>

<h3><a name="translation-work">Aan vertalingen werken</a></h3>

<p>Een vertaling moet altijd up-to-date gehouden worden met het
overeenkomstige Engelse bestand. De kopregel <code>translation-check</code> in
vertaalde bestanden wordt gebruikt om bij te houden op welke versie van het
Engelse bestand de huidige vertaling gebaseerd is. Indien u een vertaald bestand
wijzigt, moet u de kopregel <code>translation-check</code> bijwerken, zodat die
overeenkomt met de Git-vastleggingsfrommel van de overeenkomstige aanpassing van
het Engelse bestand. U kunt deze frommel terugvinden met:</p>

<pre>
  git log pad/naar/het/Engelse/bestand
</pre>

<p>Indien u een nieuwe vertaling gaat maken van een bestand, moet u het script
<code>copypage.pl</code> gebruiken. Dit maakt een sjabloon aan voor uw taal, met
inbegrip van de correcte vertalingskopregel.</p>

<h4><a name="translation-smart-change">Vertalingen aanpassen met smart_change.pl</a></h4>

<p><code>smart_change.pl</code> is een script dat ontworpen is om het
gezamenlijk updaten van originele bestanden en hun vertalingen makkelijker te
maken. U kunt het op twee manieren gebruiken, afhankelijk van welke wijzigingen
u aanbrengt.</p>

<p>
Wanneer u handmatig aan de bestanden werkt, is dit de manier waarop u
<code>smart_change.pl</code> kunt gebruiken en de wijze waarop u de kopregels
over <code>translation-check</code> gewoon kunt updaten:
</p>

<ol>
  <li>Breng de wijzigingen aan in het/de origine(e)l(e) bestand(en), en leg
  deze vast.</li>
  <li>Werk de vertalingen bij.</li>
  <li>Voer het commando <code>smart_change.pl -c VASTLEGGINGS-HASH</code> uit
  (gebruik de vastleggingshash van de wijzigingen aan het/de origine(e)l(e)
  bestand(en)).
  Dit zal de aanpassingen oppikken en in de vertaalde bestanden de kopregel
  updaten.</li>
  <li>Controleer de wijzigingen (bijv. met <code>git diff</code>).</li>
  <li>Leg de wijzigingen in de vertalingen vast.</li>
</ol>

<p>
Een andere mogelijkheid is het toepassen van een reguliere expressie om in één
keer een wijziging in meerdere bestanden door te voeren:
</p>

<ol>
  <li>Geef het commando <code>smart_change.pl -s s/FOO/BAR/ origbestand1
    origbestand2 ...</code></li>
  <li>Controleer de wijzigingen (bijv. met <code>git diff</code>).</li>
  <li>Leg het/de origine(e)l(e) bestand(en) vast.</li>
  <li>Geef het commando <code>smart_change.pl origbestand1 origbestand2</code>
    (d.w.z. ditmaal <strong>zonder de reguliere expressie</strong>). Dit zal nu
    enkel de kopregels in de vertaalde bestanden updaten.</li>
  <li>Leg tenslotte de gewijzigde vertalingen vast.</li>
</ol>

<p>
Toegegeven, dit heeft wat meer voeten in de aarde dan het eerste voorbeeld,
omdat er twee vastleggingen nodig zijn, maar dit is onvermijdelijk door de
manier waarop de Git-vastleggingshashes werken.
</p>

<h2><a id="write-access">Schrijftoegang tot de Git-opslagplaats</a></h2>

<p>
De broncode van de website van Debian wordt beheerd met Git en ze bevindt
zich op <url https://salsa.debian.org/webmaster-team/webwml/>. Standaard mogen
gasten geen vastleggingen toepassen in de broncode-opslagplaats. Als u
wilt bijdragen aan de website van Debian, zult u schrijftoegang tot de
opslagplaats moeten verkrijgen.
</p>

<h3><a name="write-access-unlimited">Onbeperkte schrijftoegang</a></h3>

<p>
Als u onbeperkte schrijftoegang tot de opslagplaats nodig heeft, bijv. als
u van plan bent frequente bijdragen te gaan leveren, vraag dan schrijftoegang
aan via de <url https://salsa.debian.org/webmaster-team/webwml/> webinterface
nadat u inlogde op het Salsa-platform van Debian.
</p>

<p>
Als u een nieuw bent bij de ontwikkeling van de Debian-website
en geen eerdere ervaring heeft, wilt u dan ook een e-mail zenden naar
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
om uzelf voor te stellen, voordat u onbeperkte schrijftoegang vraagt.
Wilt u ons dan ook wat meer over uzelf vertellen, zoals aan welk
gedeelte van de website u wilt gaan werken, welke talen u spreekt, en
ook of een ander teamlid van Debian voor u in kan staan.
</p>

<h3><a name="write-access-via-merge-request">Samenvoegingsverzoeken (Merge Requests)</a></h3>

<p>
Het is niet noodzakelijk onbeperkte schrijftoegang tot de opslagplaats te
verkrijgen — u kunt altijd een samenvoegingsverzoek (Engels: merge request) indienen en
andere ontwikkelaars uw werk laten nakijken en aanvaarden. Volg daarvoor de
standaardprocedure voor samenvoegingsverzoeken die geboden wordt door de
webinterface van het Salsa GitLab-platform en lees de volgende
twee documenten:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a> (Werkwijze bij het maken van een kopie (een zogenaamde fork) van een project</li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a> (Wanneer u in een kopie werkt)</li>
</ul>

<p>
Gelieve er nota van te nemen dat niet alle website-ontwikkelaars op de
uitkijk staan voor samenvoegingsverzoeken. Daarom kan
het enige tijd duren voordat u wat terug hoort. Als u zich afvraagt of uw
bijdrage wel toegepast zal worden, zend dan een e-mail naar de mailinglijst
<a href="https://lists.debian.org/debian-www/">debian-www</a>
en vraag om een beoordeling.
</p>

<h2><a id="notifications">Meldingen ontvangen</a></h2>

<p>
Als u werkt aan de website van Debian, zult u waarschijnlijk willen weten wat
er in de opslagplaats <code>webwml</code> gaande is. Er zijn twee manieren om
daarbij betrokken te blijven: vastleggingsmeldingen en meldingen van
samenvoegingsverzoeken.
</p>

<h3><a name="commit-notifications">Vastleggingsmeldingen ontvangen</a></h3>

<p>We hebben het <code>webwml</code>-project in Salsa zo geconfigureerd dat
vastleggingen worden getoond in het IRC-kanaal #debian-www.</p>

<p>
Als u via e-mail meldingen wilt ontvangen over vastleggingen
in de opslagplaats van <code>webwml</code>, abonneer u dan op het pseudopakket
<code>www.debian.org</code> via <a href="https://tracker.debian.org/">tracker.debian.org</a> en activeer daar het
trefwoord <code>vcs</code> door eenmalig de volgende stappen te doorlopen:
</p>

<ol>
  <li>Open een webbrowser en ga naar <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Teken in op het pseudopakket <code>www.debian.org</code>. (U kunt
  authenticeren via SSO of een e-mail en wachtwoord registreren, indien u niet
  reeds gebruik maakte van tracker.debian.org voor andere doeleinden).</li>
  <li>Ga naar <url https://tracker.debian.org/accounts/subscriptions/>, en dan
  naar <code>modify keywords</code>, vink <code>vcs</code> aan (als het nog
  niet aangevinkt is) en bewaar.</li>
  <li>Vanaf dan zult u een e-mail ontvangen wanneer iemand een vastlegging in
   de <code>webwml</code>-opslagplaats doet.</li>
</ol>

<h3><a name="merge-request-notifications">Meldingen over samenvoegingsverzoeken ontvangen</a></h3>

<p>
Als u een melding via e-mail wilt ontvangen wanneer er een nieuw
samenvoegingsverzoek ingediend werd op de <code>webwml</code>-opslagplaats
in Salsa, kunt u uw instellingen voor meldingen in de
webinterface configureren door deze stappen te volgen:
</p>

<ol>
  <li>Log in op uw Salsa-account en ga naar de projectpagina.</li>
  <li>Klik op het bel-icoontje bovenaan de hoofdpagina van het project.</li>
  <li>Selecteer het meldingsniveau waaraan u de voorkeur geeft.</li>
</ol>

